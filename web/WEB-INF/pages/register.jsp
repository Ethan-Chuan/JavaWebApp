<%-- 
    Document   : register
    Created on : Jun 25, 2018, 2:34:23 PM
    Author     : Ethan
--%>

<%@page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>用戶註冊</title>
    </head>
    <body>
        <form action="${pageContext.request.contextPath}/register-servlet" method="post">
            <table style="border: 1px black solid">
                <tr>
                    <td style="text-align: center;">用戶名</td>
                    <%-- 使用EL表達${}提取存儲在request對象中的formbean對象中封裝的表單數據
(formbean.userName)以及錯誤提示消息(formbean.errors.userName) --%>
                    <td>
                        <input type="text" name="userName" value="${formbean.userName}" /> 
                        ${formbean.errors.userName}
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;">密碼</td>
                    <td>
                        <input type="password" name="userPwd" value="${formbean.userPwd}" /> 
                        ${formbean.errors.userPwd}
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;">確認密碼</td>
                    <td>
                        <input type="password" name="confirmPwd" value="${formbean.confirmPwd}" /> 
                        ${formbean.errors.confirmPwd}
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;">郵箱</td>
                    <td>
                        <input type="text" name="email" value="${formbean.email}" /> 
                        ${formbean.errors.email}
                    </td>
                </tr>
                <tr>
                    <!-- 目前已知英文系統的時間格式為：02/19/1900 -->
                    <td style="text-align: center;">生日</td>
                    <td>
                        <input type="text" name="birthday" value="${formbean.birthday}" /> 
                        ${formbean.errors.birthday}
                    </td>
                </tr>
                <tr>
                    <td><input type="reset" value="清空" /></td>
                    <td><input type="submit" value="註冊" /></td>
                </tr>
            </table>
        </form>
    </body>
</html>

<%--
    凡是位於WEB-INF目錄下的JSP頁面是無法直接通過URL地址直接訪問的
    在開發中如果項目中有一些敏感的網絡資源不想被外界直接訪問，那麼可以考慮將這些
敏感的網絡資源放到WEB-INF目錄下，這樣就可以禁止外界直接通過URL來訪問了。
----------------------------------------------------------------------------------------------------------------
register.jsp中的<form action="${pageContext.request.contextPath}/register-servlet" method="post">
指明表單提交後，交給 tw/ethan/cnblogs/xdp_gacl/mvc/web/controller/RegisterServlet.java進行處理
----------------------------------------------------------------------------------------------------------------
    用戶註冊時如果填寫的表單數據校驗不通過，那麼服務器端就將一個存儲了錯誤提示
消息和表單數據的formbean的對象存儲到請求對象中，然後發送回register.jsp頁面，因此
我們需要在寄存器中。JSP頁面中取出的請求對象中的FormBean對象，然後將用戶填寫的
表單數據重新回顯到對應的表單項上面，將出錯時的提示消息也顯示到表格表單上面，讓
用戶知道是哪些數據填寫不合法！
--%>
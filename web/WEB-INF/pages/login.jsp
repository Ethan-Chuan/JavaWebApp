<%-- 
    Document   : login
    Created on : Jun 28, 2018, 1:35:21 PM
    Author     : Ethan
--%>

<%@page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>用戶登錄</title>
    </head>
    <body>
        <form action="${pageContext.request.contextPath}/login-servlet" method="post">
            用戶名：<input type="text" name="username" /><br />
            密碼：<input type="password" name="password" /><br />
            <input type="submit" value="登錄" />
        </form>
    </body>
</html>

<%--
login.jsp中的
<form action="${pageContext.request.contextPath}/servlet/LoginServlet" method="post">
指明表單提交後，交給LoginServlet進行處理。
--%>
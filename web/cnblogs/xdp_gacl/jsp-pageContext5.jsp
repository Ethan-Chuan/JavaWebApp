<%-- 
    Document   : jsp-pageContext5
    Created on : Jun 22, 2018, 2:19:48 PM
    Author     : Ethan
--%>

<%@page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>使用pageContext的include方法引入資源</title>
    </head>
    <body>
        
        <%
            pageContext.include("jspfragments/head.jsp");
        %>
            使用pageContext的include方法引入資源
        <%
            pageContext.include("jspfragments/foot.jsp");
        %>
        
        <hr />
        
        <jsp:include page="/cnblogs/xdp_gacl/jspfragments/head.jsp" />
        使用jsp:include標籤引入資源
        <jsp:include page="/cnblogs/xdp_gacl/jspfragments/foot.jsp" />
    </body>
</html>

<%--
PageContext類中定義了兩個include方法(用來引入頁面)來簡化和替代<%@ include file="relativeUrlPath" %>方法。
pageContext.include("relativeUrlPath") and <jsp:include page="relativeUrlPath" />只能使用 jsp 擴展名
<%@ include file="relativeUrlPath" %>可以使用 jsp 或 jspf 擴展名

方法接收的資源如果以“/”開頭， “/”代表當前web應用。

----------------------------------------------------------------------------------------------------------------
在實際開發中，使用pageContext的include方法引入頁面這種做法也很少用，一般都使用
jsp:include標籤引入資源，因此這種做法了解一下即可。
--%>
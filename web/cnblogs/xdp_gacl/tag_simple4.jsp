<%-- 
    Document   : tag_simple4
    Created on : 2018/7/12, 上午 11:02:21
    Author     : Ethan
--%>

<%@page language="java" pageEncoding="UTF-8"%>

<%-- 在jsp頁面中導入自定義標籤庫 --%>
<%@taglib  uri="/WEB-INF/tlds/tag_simple.tld" prefix="tst" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>用簡單標籤控制標籤餘下的Jsp不執行</title>
    </head>
    <body>
        <h1>Hello </h1>
        <%-- 在jsp頁面中使用自定義標籤 --%>
        <tst:simple4/>
        <!-- 這裡的內容位於 tst:simple4 標籤後面，因此不會輸出到頁面上 -->
        <h1>SimpleTag!</h1>
    </body>
</html>

<%-- 
    Document   : tag-custom4
    Created on : 2018/7/9, 下午 04:43:41
    Author     : Ethan
--%>

<%@page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<%-- 在jsp頁面中導入自定義標籤庫 --%>
<%@taglib  uri="/WEB-INF/tlds/tag_custom.tld" prefix="tct" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>控制頁面內容重複執行5次</title>
    </head>
    <body>
        <%-- 在jsp頁面中使用自定義標籤 custom4標籤 --%>
        <tct:custom4>
            <h3>jsp頁面的內容</h3>
        </tct:custom4>
    </body>
</html>

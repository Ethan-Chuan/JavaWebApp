<%-- 
    Document   : jstl-remove-tag
    Created on : 2018/7/16, 上午 10:03:25
    Author     : Ethan
--%>

<%@page language="java" pageEncoding="UTF-8"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSTL： --表達式控制標籤"remove"標籤的使用</title>
    </head>
    <body>
        <ul>
            <c:set var="name" scope="session">白虎神皇</c:set>
            <c:set var="age" scope="session">25</c:set>
            <li><c:out value="${sessionScope.name}"></c:out></li>
            <li><c:out value="${sessionScope.age}"></c:out></li>
            <%-- 使用remove標籤移除age變量 --%>
            <c:remove var="age" />
            <li><c:out value="${sessionScope.name}"></c:out></li>
            <li><c:out value="${sessionScope.age}"></c:out></li>
        </ul>
    </body>
</html>

<%--
表達式控制標籤——remove標籤的使用
<c:remove>標籤的功能
　　<c:remove>標籤主要用來從指定的JSP範圍內移除指定的變量。

<c:remove>標籤的語法
　　<c:remove var=”變量名” [scope=”page|request|session|application”]/>
　　其中var屬性是必須的，scope可以以省略。
--%>
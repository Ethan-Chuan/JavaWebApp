<%-- 
    Document   : jsp-calculator
    Created on : Jun 25, 2018, 10:27:04 AM
    Author     : Ethan
    JSP+ JavaBean 的開發模式編寫計算器
--%>

<%@page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<%-- 使用tw.ethan.cnblogs.xdp_gacl.javabean.CalculatorBean --%>
<jsp:useBean id="calcBean" class="tw.ethan.cnblogs.xdp_gacl.javabean.CalculatorBean" />
<%-- 接收用戶輸入的參數 --%>
<jsp:setProperty name="calcBean" property="*" />
<%
    // 使用CalculatorBean進行計算
    calcBean.calculate();
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>使用【jsp+javabean開發模式】開發的簡單計算器</title>
    </head>
    <body>
        <br />
        計算的結果是：
        <jsp:getProperty name="calcBean" property="firstNum" />
        <jsp:getProperty name="calcBean" property="operator" />
        <jsp:getProperty name="calcBean" property="secondNum" />
        =
        <jsp:getProperty name="calcBean" property="result" />
        <br /><hr /><br />
        <form action="${pageContext.request.contextPath}/cnblogs/xdp_gacl/jsp-calculator.jsp" method="post">
            <table style="border: 1px">
                <tr>
                    <td colspan="2">簡單的計算機</td>
                </tr>
                <tr>
                    <td>第一個參數</td>
                    <td><input type="text" name="firstNum" /></td>
                </tr>
                <tr>
                    <td>運算符</td>
                    <td>
                        <select name="operator">
                            <option value="+">+</option>
                            <option value="-">-</option>
                            <option value="*">*</option>
                            <option value="/">/</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>第二個參數</td>
                    <td><input type="text" name="secondNum" /></td>
                </tr>
                <tr>
                    <td colspan="2"><input type="submit" value="計算" /></td>
                </tr>
            </table>
        </form>
    </body>
</html>

<%-- 
    Document   : tag_simple5
    Created on : 2018/7/12, 下午 12:25:21
    Author     : Ethan
--%>

<%@page language="java" pageEncoding="UTF-8"%>

<%-- 在jsp頁面中導入自定義標籤庫 --%>
<%@taglib  uri="/WEB-INF/tlds/tag_simple.tld" prefix="tst" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>通過標籤的屬性控制標籤體的執行次數</title>
    </head>
    <body>
        <%-- 在 jsp 頁面中使用自定義標籤，標籤有一個 count 屬性 --%>
        <tst:simple5 count="2">
            <h1>白虎神皇</h1>
        </tst:simple5>
    </body>
</html>

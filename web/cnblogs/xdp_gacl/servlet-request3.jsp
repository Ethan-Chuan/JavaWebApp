<%-- 
    Document   : servlet-request3
    Created on : Jun 14, 2018, 2:37:25 PM
    Author     : Ethan
--%>

<%@page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Html 的 Form 表單元素</title>
    </head>
    <body>
        <fieldset style="width:500px;">
            <legend>Heml 的 Form 表單元素</legend>
            
            <!-- 
            form 表單的 action 屬性規定當提交表單時，向何處發送表單數據，
            method 屬性指表單的提交方式，分為 get 和 post，默認為 get
            -->
            <form action="${pageContext.request.contextPath}/servlet-request3" method="post">
                <!-- 輸入文本框，SIZE表示顯示長度，maxlength表示最多輸入長度 -->
                編&nbsp;&nbsp;&nbsp;&nbsp;號(文本框)：
                <input type="text" name="userid" value="NO." size="2" maxlength="2"/><br />
                <!-- 輸入文本框，通過value指定其顯示的默認值 -->
                用戶名(文本框)：<input type="text" name="username" value="請輸入用戶名"/><br />
                <!-- 密碼框，其中所有輸入的內容都以密文的形式顯示 -->
                密&nbsp;&nbsp;&nbsp;&nbsp;碼(密碼框)：<input type="password" name="userpass" value="請輸入密碼"/><br />
                <!-- 單選按鈕，通過checked指定默認選中，名稱必須一樣，其中value為真正需要的內容 -->
                性&nbsp;&nbsp;&nbsp;&nbsp;別(單選框)：
                <input type="radio" name="sex" value="男" checked="true"/>男
                <input type="radio" name="sex" value="女"/>女<br />
                <!-- 下拉式表框，通過<option>元素指定下拉的選項 -->
                部&nbsp;&nbsp;&nbsp;&nbsp;門(下拉框)：
                <select name="dept">
                    <option value="技術部">技術部</option>
                    <option value="銷售部">銷售部</option>
                    <option value="財務部">財務部</option>
                </select><br />
                <!-- 複選框，可以同時選擇多個選項，名稱必須一樣，其中value為真正需要的內容 -->
                興&nbsp;&nbsp;&nbsp;&nbsp;趣(複選框)：
                <input type="checkbox" name="inst" value="唱歌"/>唱歌
                <input type="checkbox" name="inst" value="游泳"/>游泳
                <input type="checkbox" name="inst" value="跳舞"/>跳舞
                <input type="checkbox" name="inst" value="編程" checked="true"/>編程
                <input type="checkbox" name="inst" value="上網"/>上網
                <br />
                <!-- 大文本輸入框，寬度為34列，高度為5行 -->
                說&nbsp;&nbsp;&nbsp;&nbsp;明(文本域)：
                <textarea name="note" cols="34" rows="5"></textarea>
                <br />
                <!-- 隱藏域，在頁面上無法看到，專門用來傳遞參數或者保存參數 -->
                <input type="hidden" name="hiddenField" value="hiddenvalue"/>
                <!-- 提交表單按鈕，當點擊提交後，所有填寫的表單內容都會被傳輸到服務器端 -->
                <input type="submit" value="提交(提交按鈕)"/>
                <!-- 重置表單按鈕，當點擊重置後，所有表單恢復原始顯示內容 -->
                <input type="reset" value="重置(重置按鈕)"/>
            </form>
        </fieldset>
    </body>
</html>

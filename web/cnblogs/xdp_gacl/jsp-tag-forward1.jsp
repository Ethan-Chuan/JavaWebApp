<%-- 
    Document   : jsp-tag-forward1
    Created on : Jun 22, 2018, 6:25:19 PM
    Author     : Ethan
--%>

<%@page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>jsp:forward page</title>
    </head>
    <body>
        <%--使用<jsp:forward>標籤跳轉到jsp-tag-forward2.jsp--%>
        <jsp:forward page="jsp-tag-forward2.jsp"/>
    </body>
</html>

<%--
    從頁面的顯示效果來看，頁面已經完成了跳轉，但地址欄沒有變化，地址欄中顯示的
地址還是jsp-tag-forward1.jsp，但頁面顯示的內容卻是jsp-tag-forward2.jsp中的內容。

    因為此跳轉屬於服務器端跳轉。只要是服務器端跳轉，則地址欄永遠沒有變化。
--%>
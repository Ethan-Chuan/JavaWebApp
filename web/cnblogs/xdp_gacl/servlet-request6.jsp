<%-- 
    Document   : servlet-request6
    Created on : Jun 20, 2018, 2:23:01 PM
    Author     : Ethan
--%>

<%@page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Request 對象實現請求轉發</title>
    </head>
    <body>
        使用普通方式取出存儲在request對象中的數據：
        <h3 style="color:red;"><%=(String)request.getAttribute("data")%> </h3>
        使用EL表達式取出存儲在request對象中的數據：
        <h3 style="color:red;">${data}</h3>
    </body>
</html>

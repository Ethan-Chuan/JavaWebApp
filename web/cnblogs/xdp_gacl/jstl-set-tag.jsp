<%-- 
    Document   : jstl-set-tag
    Created on : 2018/7/14, 下午 04:22:41
    Author     : Ethan
--%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page language="java" pageEncoding="UTF-8"%>

<%-- 引入JSTL核心標籤庫 --%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%-- 使用JSP的指令元素指定要使用的JavaBean --%>
<%-- 負責實例化Bean，id指定實例化後的對象名，可以通過 ${person} 得到person在內存中的值 (或者使用 person.toString()方法) --%>
<jsp:useBean id="person" class="tw.ethan.cnblogs.xdp_gacl.BeanPerson" />

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSTL: --表達式控制標籤"set"標籤的使用</title>
    </head>
    <body>
        <h3>代碼給出了給指定scope範圍賦值的示例</h3>
        <ul>
            <%-- 通過<c:set>標籤將data1的值放入page範圍中。 --%>
            <li>把一個值放入page域中：<c:set var="data1" value="www" scope="page" /></li>
            <%-- 使用EL表達式從pageScope得到data1的值 --%>
            <li>從page域中得到的值：${pageScope.data1}</li>
            
            <%-- 通過<c:set>標籤將data2的值放入request範圍中。 --%>
            <li>把一個值放入request域中：<c:set var="data2" value="google" scope="request" /></li>
            <%-- 使用EL表達式從requestScope得到data2的值 --%>
            <li>從request域中得到的值：${requestScope.data2}</li>
            
            <%-- 通過<c:set>標籤將name1的值放入session範圍中。 --%>
            <li>把一個值放入session域中：<c:set var="name1" value="天天開心天天開心" scope="session"></c:set></li>
            <%-- 使用EL表達式從sessionScope得到name1的值 --%>
            <li>從session域中得到的值：${sessionScope.name1}</li>
            
            <%-- 通過<c:set>標籤將name2的值放入application範圍中。 --%>
            <li>把一個值放入application域中：<c:set var="name2" scope="application" >天天笑到整下午</c:set></li>
            <%-- 使用EL表達式從application範圍中取值，用<c:out>標籤輸出使得頁面規範化 --%>
            <li>使用out標籤和EL表達式嵌套從application域中得到的值：
                <c:out value="${applicationScope.name2}">未得到name的值</c:out>
            </li>
            
            <%-- 不指定範圍使用EL自動查找得到的值 --%>
            <li>未指定scope的範圍，會從不同的範圍內查找相對應的值：${data1},${data2},${name1},${name2}</li>
        </ul>
        
        <hr />
        <h3>操作JavaBean，設置JavaBean的屬性值</h3>
        <%--
        設置JavaBean的屬性值，等同與setter方法，Target指向實例化後的對象，property指向要插入值的參數名。
              注意：使用target時一定要指向實例化後的JavaBean對象，也就是要跟<jsp:useBean>配套使用，
              也可以java腳本實例化，但這就失去了是用標籤的本質意義。
              使用Java腳本實例化：
              <%@page import="javabean.Person"%
              <% Person person=new Person(); %>
        --%>
        <c:set target="${person}" property="name">Ethan</c:set>
        <c:set target="${person}" property="age">3x</c:set>
        <c:set target="${person}" property="sex">男</c:set>
        <c:set target="${person}" property="home">地球</c:set>
        <ul>
            <li>使用的目標對象為：${person}</li>
            <li>從bean中獲得的name值為：<c:out value="${person.name}"></c:out></li>
            <li>從bean中獲得的age值為：<c:out value="${person.age}"></c:out></li>
            <li>從bean中獲得的sex值為：<c:out value="${person.sex}"></c:out></li>
            <li>從bean中獲得的home值為：<c:out value="${person.home}"></c:out></li>
        </ul>
        
        <hr />
        <h3>操作Map</h3>
        <%
            Map map = new HashMap();
            request.setAttribute("myMap", map);
        %>
        <%-- 將data對象的值存儲在map集合中 --%>
        <c:set property="data" value="google" target="${myMap}" />
        <c:set property="data1" value="yahoo" target="${myMap}" />
        <c:set property="data2" value="hinet" target="${myMap}" />
        <ul>
            <li><c:out value="${myMap.data}">未得到data的值</c:out></li>
            <li><c:out value="${myMap.data1}">未得到data1的值</c:out></li>
            <li><c:out value="${myMap.data2}">未得到data2的值</c:out></li>
        </ul>
    </body>
</html>

<%--
表達式控制標籤——set標籤的使用

<c:set>標籤的功能
　　<c:set>標籤用於把某一個對象存在指定的域範圍內，或者將某一個對象存儲到Map或者JavaBean對像中。

<c:set>標籤的語法
　　<c:set>標籤的編寫共有4種語法格式。
　　語法1：存值，把一個值放在指定的域範圍內。
　　　　<c:set value=”值1” var=”name1” [scope=”page|request|session|application”]/>
　　　　含義：把一個變量名為name1值為“值1”的變量存儲在指定的scope範圍內。
　　語法2：
　　　　<c:set var=”name2” [scope=”page|request|session|application”]>
　　　　　　值2
　　　　</c:set>
　　　　含義：把一個變量名為name2，值為值2的變量存儲在指定的scope範圍內。
　　語法3：
　　　　<c:set value=”值3” target=”JavaBean對象” property=”屬性名”/>
　　　　含義：把一個值為“值3”賦值給指定的JavaBean的屬性名。相當與setter()方法。
　　語法4：
　　　　<c:set target=”JavaBean對象” property=”屬性名”>
　　　　　　值4
　　　　</c:set>
　　　　含義：把一個值4賦值給指定的JavaBean的屬性名。
從功能上分語法1和語法2、語法3和語法4的效果是一樣的，只是把value值放置的位置不同，
至於使用那個根據個人的喜愛，語法1和語法2是向scope範圍內存儲一個值，語法3和語法4是給指定的JavaBean賦值。

----------------------------------------------------------------------------------------------------------------
<c:set>標籤屬性
  
屬性名                     是否支持EL                 屬性類型                                     屬性描述

  value                         true                             Object                                   用於指定屬性值            

  var                            false                           String                              用於指定要設置的Web域屬性的名稱

  scope                         false                           String                                  用於指定屬性所在的Web域

  target                         true                            Object                             用於指定要設置屬性的對象，這個對象
|                                                                                                          必須是JavaBean對象或java.util.Map對象
    
  property                     true                             String                           用於指定當前要為對象設置的屬性名稱 
--%>
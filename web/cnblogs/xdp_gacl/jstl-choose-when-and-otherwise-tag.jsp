<%-- 
    Document   : jstl-choose-when-and-otherwise-tag
    Created on : 2018/7/16, 上午 11:42:20
    Author     : Ethan
--%>

<%@page language="java" pageEncoding="UTF-8"%>

<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSTL： -- choose及其嵌套標籤示例</title>
    </head>
    <body>
        <h4>choose及其嵌套標籤示例</h4>
        <hr />
        <%-- 通過 set 標籤設定 score 的值為85 --%>
        <c:set var="score" value="85" />
        <c:choose>
            <%--使用<c:when>進行條件判斷。
            如果大於等於90，輸出"您的成績為優秀";
            如果大於等於70小於90，輸出"您的成績為良好";
            大於等於60小於70，輸出"您的成績為及格";
            其他(otherwise)輸出"對不起，您沒能通過考試"。
            --%>
            <c:when test="${score>=90}">
                您的成績為優秀！
            </c:when>
            <c:when test="${score>70 && score<90}">
                您的成績為良好！
            </c:when>
            <c:when test="${score>=60 && score<70}">
                您的成績為及格
            </c:when>
            <c:otherwise>
                對不起，您沒有通過考試！
            </c:otherwise>
        </c:choose>
    </body>
</html>

<%--
流程控制標籤——choose標籤、when標籤、otherwise標籤配合的使用
<c:choose>、<c:when>和<c:otherwise>標籤的功能
<c:choose>、<c:when>和<c:otherwise>這3個標籤通常情況下是一起使用的，<c:choose>標籤作為<c:when>和<c:otherwise>標籤的父標籤來使用。

　　使用<c:choose>，<c:when>和<c:otherwise>三個標籤，可以構造類似 “if-else if-else” 的複雜條件判斷結構。

語法
　<c:choose>
                 <c:when test="條件1">
　　　　　　//業務邏輯1
                 </c:when>
　　 <c:when test="條件2">
　　　　　　//業務邏輯2
                 </c:when>
　　 <c:when test="條件n">
　　　　　　//業務邏輯n
                 </c:when>
                 <c:otherwise>
　　　　　　//業務邏輯
        </c:otherwise>
    </c:choose>
      
----------------------------------------------------------------------------------------------------------------
<c:when>標籤的屬性
  
屬性名                     屬性描述                   是否需要                        Default
  
  test                    Condition to evaluate            Yes                             None
--%>
<%-- 
    Document   : jstl-forToken-tag
    Created on : 2018/7/16, 下午 12:43:59
    Author     : Ethan
--%>

<%@page language="java" pageEncoding="UTF-8"%>

<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSTL： -- forTokens標籤實例</title>
    </head>
    <body>
        <h4><c:out value="forToken實例"/></h4>
        <hr />
        <%--提示：分隔符的作用是根據標識，截取字符串。
        如果未設定分隔符或在字符串中沒有找到分隔符，將把整個元素作為一個元素截取。
        在實際應用中用於在除去某些符號在頁面中顯示--%>
        <c:forTokens var="str" items="世、界、包、容、您、！" delims="、">
            <c:out value="${str}"></c:out><br />
        </c:forTokens>
        <br />
        <c:forTokens items="123-4567-8854" delims="-" var="t">
            <c:out value="${t}"></c:out><br />
        </c:forTokens>
        <br />
        <c:forTokens items="1*2*3*4*5*6*7"
                     delims="*"
                     begin="1"
                     end="3"
                     var="n"
                     varStatus="s">
            &nbsp;<c:out value="${n}" />的四種屬性：<br />
            &nbsp;&nbsp;&nbsp;&nbsp;所在位置，即索引：<c:out value="${s.index}" /><br />
            &nbsp;&nbsp;&nbsp;&nbsp;總共疊代的次數：<c:out value="${s.count}" /><br />
            &nbsp;&nbsp;&nbsp;&nbsp;是否為第一個位置：<c:out value="${s.first}" /><br />
            &nbsp;&nbsp;&nbsp;&nbsp;是否為最後一個位置：<c:out value="${s.last}" /><br />
        </c:forTokens>
    </body>
</html>

<%--
循環標籤——forTokens標籤的使用
<c:forTokens>標籤的功能
　　該標籤用於瀏覽字符串，並根據指定的字符將字符串截取。

<c:forTokens>標籤的語法
語法：
<c:forTokens items=”strigOfTokens”
            delims=”delimiters”
            [var=”name”
            begin=”begin”
            end=”end”
            step=”len”
            varStatus=”statusName”] >
    本體內容
</c:forTokens>
【參數說明】
　　（1）items指定被迭代的字符串。
　　（2）delims指定使用的分隔符。
　　（3）var指定用來存放遍歷到的成員。
　　（4）begin指定遍歷的開始位置（int型從取值0開始）。
　　（5）end指定遍歷結束的位置（int型，默認集合中最後一個元素）。
　　（6）step遍歷的步長（大於0的整型）。
　　（7）varStatus存放遍歷到的成員的狀態信息。
--%>
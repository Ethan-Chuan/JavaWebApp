<%-- 
    Document   : jsp-javabean5
    Created on : 2018/6/24, 下午 03:03:52
    Author     : Ethan
--%>

<%@page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<%--
在jsp中使用jsp:useBean標籤來實例化一個Java類的對象
<jsp:useBean id="persion" class="tw.ethan.cnblogs.xdp_gacl.javabean.Person" scope="page" />
    ┝<jsp:useBean>:表示在JSP中要使用JavaBean
    ┝id:表示生成的實例化對象，凡是在標籤中看見了id，則肯定表示一個實例對象
    ┝class:此對象對應的包.類名稱
    ┝scope:此javaBean的保存範圍，四種範圍：page, request, session, application
--%>
<jsp:useBean id="person" class="tw.ethan.cnblogs.xdp_gacl.javabean.Person" scope="page" />

<%--
使用jsp:setProperty標籤設置person對象的屬性值
jsp:setProperty在設置對象的屬性值時會自動把字符串轉換成8種基本數據類型
但是jsp:setProperty對於複合數據類型無法自動轉換
--%>
<jsp:setProperty property="name" name="person" value="白虎神皇" />
<jsp:setProperty property="sex" name="person" value="男" />
<jsp:setProperty property="age" name="person" value="24" />
<jsp:setProperty property="married" name="person" value="false" />

<%--
birthday屬性是一個Date類型，這個屬於複合數據類型，因此無法將字符串自動轉換成Date，
用下面這種寫法是會報錯的
<jsp:setProperty property="birthday" name="person" value="1988-05-07" />
--%>
<jsp:setProperty property="birthday" name="person" value="<%=new Date()%>" />

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>jsp:setProperty標籤使用範例</title>
    </head>
    <body>
        <%-- 使用getXxx()方法獲取對象的屬性值 --%>
        <h2>姓名：<jsp:getProperty property="name" name="person"/></h2>
        <h2>性別：<jsp:getProperty property="sex" name="person"/></h2>
        <h2>年齡：<jsp:getProperty property="age" name="person"/></h2>
        <h2>已婚：<jsp:getProperty property="married" name="person"/></h2>
        <h2>出生日期：<jsp:getProperty property="birthday" name="person"/></h2>
    </body>
</html>

<%--
JSP技術提供了三個關於JavaBean組件的動作元素，即JSP標籤，它們分別為：
    <jsp:useBean>標籤：用於在JSP頁面中查找或實例化一個JavaBean組件。
    <jsp:setProperty>標籤：用於在JSP頁面中設置一個JavaBean組件的屬性。
    <jsp:getProperty>標籤：用於在JSP頁面中獲取一個JavaBean組件的屬性。
----------------------------------------------------------------------------------------------------------------
<jsp:getProperty>標籤用於讀取JavaBean對象的屬性，也就是調用JavaBean對象的getter方法，
然後將讀取的屬性值轉換成字符串後插入進輸出的響應正文中。
　　語法：
    　　<jsp:getProperty name="beanInstanceName" property="PropertyName" />
　　　　name屬性用於指定JavaBean實例對象的名稱，其值應與<jsp:useBean>標籤的id屬性值相同。
　　　　property屬性用於指定JavaBean實例對象的屬性名。
　　如果一個JavaBean實例對象的某個屬性的值為null，那麼，使用<jsp:getProperty>標籤輸
出該屬性的結果將是一個內容為“null”的字符串。
--%>
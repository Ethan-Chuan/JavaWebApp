<%-- 
    Document   : jstl-forEach-tag
    Created on : 2018/7/16, 下午 12:01:24
    Author     : Ethan
--%>

<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page language="java" pageEncoding="UTF-8"%>

<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSTL： -- forEach標籤實例</title>
    </head>
    <body>
        <h4><c:out value="forEach實例" /></h4>
        <%
            List<String> list = new ArrayList<>();
            list.add(0, "貝貝");
            list.add(1, "晶晶");
            list.add(2, "歡歡");
            list.add(3, "盈盈");
            list.add(4, "妮妮");
            request.setAttribute("list", list);
        %>
        <B><c:out value="不指定begin和end的疊代：" /></B><br />
        <%-- 不使用begin和end的疊代，從集合的第一個元素開始，遍歷到最後一個元素 --%>
        <c:forEach var="fuwa" items="${list}">
        &nbsp;<c:out value="${fuwa}" /><br />
        </c:forEach>
        
        <B><c:out value="指定begin和end的疊代：" /></B><br />
        <%-- 指定begin的值為1, end的值為3, step的值為2,
        從第二個開始首先得到晶晶, 每兩個遍歷一次,
        則下一個顯示的結果為盈盈, end為3則遍歷結束。--%>
        <c:forEach var="fuwa" items="${list}" begin="1" end="3" step="2">
        &nbsp;<c:out value="${fuwa}" /><br />
        </c:forEach>
        
        <B><c:out value=" 指定varStatus的屬性名為s，並取出存儲的狀態信息：" /></B><br />
        <%-- 指定 varStatus 的屬性名為s，並取出存儲的狀態信息 --%>
        <c:forEach var="fuwa"
                   items="${list}"
                   begin="3"
                   end="4"
                   varStatus="s"
                   step="1">
        &nbsp;<c:out value="${fuwa}" />的四種屬性：<br />
        &nbsp;&nbsp;&nbsp;&nbsp;所在位置，即索引：<c:out value="${s.index}" /><br />
        &nbsp;&nbsp;&nbsp;&nbsp;總共已疊代的次數：<c:out value="${s.count}" /><br />
        &nbsp;&nbsp;&nbsp;&nbsp;是否為第一個位置：<c:out value="${s.first}" /><br />
        &nbsp;&nbsp;&nbsp;&nbsp;是否為最後一個位置：<c:out value="${s.last}" /><br />
        </c:forEach>
                   
    </body>
</html>

<%--
循環標籤——forEach標籤的使用
<c:forEach>標籤的功能
　　該標籤根據循環條件遍歷集合（Collection）中的元素。

<c:forEach>標籤的語法
　<c:forEach
    　　var=”name”
    　　items=”Collection”
    　　varStatus=”StatusName”
    　　begin=”begin”
    　　end=”end”
    　　step=”step”>
    本體內容
</c:forEach>

【參數解析】：
　　（1）var設定變量名用於存儲從集合中取出元素。
　　（2）items指定要遍歷的集合。
　　（3）varStatus設定變量名，該變量用於存放集合中元素的信息。
　　（4）begin、end用於指定遍歷的起始位置和終止位置（可選）。
　　（5）step指定循環的步長。

----------------------------------------------------------------------------------------------------------------
<c:forEach>標籤屬性

|                                             循環標籤屬性說明
  
屬性名                     是否支持EL                 屬性類型            是否必須                默認值
  
  var                           false                           String                     是                        無
  
  items                         true                           Arrays                  
|                                                                 Collection
|                                                                  Iterator                    是                        無
|                                                                Enumeration
|                                                                    Map
|                                                               String[] args
  
  bigin                          true                             int                        否                          0
  
  end                            true                             int                         否                  集合中最後一個元素
  
  step                           true                             int                         否                          1
  
  varStatus                   false                         String                       否                         無
  
----------------------------------------------------------------------------------------------------------------
其中varStatus有4個狀態屬性，如下表所示：
  
|                            varStatus的4個狀態
  屬性名             類型                                說明
  
  index                 int                             當前循環的索引值
  
  count                 int                             循環的次數
  
  frist                 boolean                         是否為第一個位置
  
  last                  boolean                         是否為最後一個位置
--%>
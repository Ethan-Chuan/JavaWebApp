<%-- 
    Document   : jstl-if-tag
    Created on : 2018/7/16, 上午 11:21:03
    Author     : Ethan
--%>

<%@page language="java" pageEncoding="UTF-8"%>

<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSTL： --流程控制標籤 if標籤示例</title>
    </head>
    <body>
        <h4>if標籤示例</h4>
        <hr />
        <form action="jstl-if-tag.jsp" method="post">
            <input type="text" name="uname" value="${param.uname}" />
            <input type="submit" value="登錄" />
        </form>
        <%-- 使用if標籤進行判斷並把檢驗後的結果賦給adminchock，存儲在默認的page範圍中。 --%>
        <c:if test="${param.uname==\"admin\"}" var="adminchock" >
            <%-- 可以把adminchock的屬性範圍設置為session，這樣就可以在其他的頁面中得到adminchock的值 
            使用<c:if text="${adminchock}"><c:if>判斷，實現不同的權限。--%>
            <c:out value="管理員歡迎您！" />
        </c:if>
        <%-- 使用EL表達式得到adminchock的值，如果輸入的用戶名為admin將顯示true。 --%>
        ${adminchock}
    </body>
</html>

<%--
流程控制標籤——if標籤的使用
<c:if>標籤的功能
　　<c:if>標籤和程序中的if語句作用相同，用來實現條件控制。

<c:if>標籤的語法
　　【語法1】：沒有標籤體內容(body)
　　　　<c:if test="testCondition" var="varName" [scope="{page|request|session|application}"]/>
　　【語法2】：有標籤體內容
　　　　<c:if test="testCondition" [var="varName"] [scope="{page|request|session|application}"]>
    　　　　　　標籤體內容
　　　　</c:if>

　　【參數說明】：
　　　　（1）test屬性用於存放判斷的條件，一般使用EL表達式來編寫。
　　　　（2）var屬性用來存放判斷的結果，類型為true或false。
　　　　（3）scopes屬性用來指定var屬性存放的範圍。

----------------------------------------------------------------------------------------------------------------
<c:if>標籤的屬性
  
屬性名                     是否支持EL                 屬性類型                                     屬性描述
  
  test                            true                           boolean                  決定是否處理標籤體中的內容的條件表達式
  
  var                            false                          String                   用於指定將 test 屬性的執行結果保存到
|                                                                                               某個 Web 域中的某個屬性的名稱
  
  scope                         false                          String                   指定將 test 屬性的執行結果保存到哪個 Web 域中
--%>
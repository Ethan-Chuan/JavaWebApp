<%-- 
    Document   : error404
    Created on : Jun 22, 2018, 10:02:13 AM
    Author     : Ethan
--%>

<%@page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>404錯誤提示頁面</title>
        <!-- 10秒後自動跳轉回 1.jsp -->
        <meta http-equiv="refresh" content="10;url=${pageContext.request.contextPath}/cnblogs/xdp_gacl/1.jsp" />
    </head>
    <body>
        <p style="text-align:center;">
        <img src="${pageContext.request.contextPath}/img/error404.png" 
             alt="對不起，你要訪問的頁面沒有找到，請聯繫管理員處理！" /> 
        </p>
        <br />
        <p style="text-align:center;">
            10秒鐘自動跳轉回首頁，如果沒有跳轉，請點擊
            <a href="${pageContext.request.contextPath}/cnblogs/xdp_gacl/1.jsp">這裡</a>
        </p>
    </body>
</html>

<%--
    1. errorPage屬性的設置值必須使用相對路徑，如果以“/”開頭，表示相對於當前Web應
用程序的根目錄(注意不是站點根目錄)，否則，表示相對於當前頁面。

    2. 可以在web.xml文件中使用<error-page>元素為整個Web應用程序設置錯誤處理頁面。

    3. <error-page>元素有3個子元素，<error-code>、<exception-type>、<location>
            <error-code>子元素指定錯誤的狀態碼，例如：<error-code>404</error-code>
            <exception-type>子元素指定異常類的完全限定名，例如：<exception-type>java.lang.ArithmeticException</exception-type>
            <location>子元素指定以“/”開頭的錯誤處理頁面的路徑，例如：<location>/ErrorPage/404Error.jsp</location>
    
    4. 如果設置了某個JSP頁面的errorPage屬性，那麼在web.xml文件中設置的錯誤處理將不對該頁面起作用。
--%>
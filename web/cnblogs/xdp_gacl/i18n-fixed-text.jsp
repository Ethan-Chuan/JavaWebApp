<%-- 
    Document   : i18n-fixed-text
    Created on : 2018/7/24, 上午 11:49:27
    Author     : Ethan

    在WEB應用中使用國際化標籤庫實現固定文本的國際化
--%>

<%@page language="java" pageEncoding="UTF-8"%>

<%-- 導入國際化標籤庫 --%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>國際化(i18n)測試</title>
    </head>
    
    <%--
    // 加載i18n資源文件，request.getLocale()獲取訪問用戶所在的國家地區
    ResourceBundle myResourcesBundle = ResourceBundle.getBundle("tw.ethan.cnblogs.xdp_gacl.i18n.resource.myproperties", request.getLocale());
    --%>
    
    <body>
        <fmt:setBundle var="bundle" basename="tw.ethan.cnblogs.xdp_gacl.i18n.resource.myproperties" scope="page" />
        <form action="">
            <fmt:message key="username" bundle="${bundle}" />：<input type="text" name="username" /><br />
            <fmt:message key="password" bundle="${bundle}" />：<input type="password" name="password" /><br />
            <input type="submit" value="<fmt:message key="submit" bundle="${bundle}" />" />
        </form>
    </body>
</html>

<%-- 
    Document   : error500
    Created on : Jun 22, 2018, 11:06:21 AM
    Author     : Ethan
--%>

<%@page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>500錯誤提示頁面</title>
        <!-- 10秒後自動跳轉回 1.jsp -->
        <meta http-equiv="refresh" content="10;url=${pageContext.request.contextPath}/cnblogs/xdp_gacl/1.jsp" />
    </head>
    <body>
        <p style="text-align:center;">
        <img src="${pageContext.request.contextPath}/img/error500.jpg" 
             alt="對不起，你要訪問的頁面沒有找到，請聯繫管理員處理！" /> 
        </p>
        <br />
        <p style="text-align:center;">
            10秒鐘自動跳轉回首頁，如果沒有跳轉，請點擊
            <a href="${pageContext.request.contextPath}/cnblogs/xdp_gacl/1.jsp">這裡</a>
        </p>
    </body>
</html>

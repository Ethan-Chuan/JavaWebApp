<%-- 
    Document   : jsp-javabean4
    Created on : 2018/6/24, 下午 02:58:39
    Author     : Ethan
--%>

<%@page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<%--
在jsp中使用jsp:useBean標籤來實例化一個Java類的對象
<jsp:useBean id="persion" class="tw.ethan.cnblogs.xdp_gacl.javabean.Person" scope="page" />
    ┝<jsp:useBean>:表示在JSP中要使用JavaBean
    ┝id:表示生成的實例化對象，凡是在標籤中看見了id，則肯定表示一個實例對象
    ┝class:此對象對應的包.類名稱
    ┝scope:此javaBean的保存範圍，四種範圍：page, request, session, application
--%>
<jsp:useBean id="person" class="tw.ethan.cnblogs.xdp_gacl.javabean.Person" scope="page" />

<%--
    jsp:setProperty標籤用所有的請求參數為bean的屬性賦值
    property="*"代表bean的所有屬性
--%>
<jsp:setProperty property="*" name="person" />

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>jsp:setProperty標籤使用範例</title>
    </head>
    <body>
        <%-- 使用getXxx()方法獲取對象的屬性值 --%>
        <h2>姓名：<%=person.getName()%></h2>
        <h2>性別：<%=person.getSex()%></h2>
        <h2>年齡：<%=person.getAge()%></h2>
    </body>
</html>

<%--
用所有的請求參數為bean的屬性賦值
http://localhost:8080/JavaWebApp/cnblogs/xdp_gacl/jsp-javabean4.jsp?sex=%E7%94%B7&name=%E7%99%BD%E8%99%8E%E7%A5%9E%E7%9A%87&age=24
--%>
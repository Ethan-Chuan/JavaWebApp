<%-- 
    Document   : jsp-four-scopes2
    Created on : Jun 22, 2018, 4:03:01 PM
    Author     : Ethan
--%>

<%@page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>jsp:forward page</title>
    </head>
    
     <%-- page scope --%>
    <%
        // 取得設置的屬性
        // 強制向下轉型成String類型
        String pageName = (String)pageContext.getAttribute("pageName");
        // 強制向下轉型成Date類型
        Date pageDate = (Date)pageContext.getAttribute("pageDate");
    %>
    
    <%-- request scope --%>
    <%
        // 取得設置的屬性
        // 強制向下轉型成String類型
        String requestName = (String)request.getAttribute("requestName");
        // 強制向下轉型成Date類型
        Date requestDate = (Date)request.getAttribute("requestDate");
    %>
    
    <%-- session scope --%>
    <%
        // 取得設置的屬性
        // 強制向下轉型成String類型
        String sessionName = (String)session.getAttribute("sessionName");
        // 強制向下轉型成Date類型
        Date sessionDate = (Date)session.getAttribute("sessionDate");
    %>
    
    <%-- application scope --%>
    <%
        // 取得設置的屬性
        // 強制向下轉型成String類型
        String applicationName = (String)application.getAttribute("applicationName");
        // 強制向下轉型成Date類型
        Date applicationDate = (Date)application.getAttribute("applicationDate");
    %>
    
    <body>
        <h1>jsp:forward page</h1>
        <h2>pageContext.getAttribute</h2>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            姓名：<%=pageName%><br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            日期：<%=pageDate%><br />
            (使用伺服器端跳轉，url不變，但資料依舊無法取得！)<br />
        </p>
        <hr />
        <h2>request.getAttribute</h2>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            姓名：<%=requestName%><br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            日期：<%=requestDate%><br />
        </p>
        <hr />
        <h2>session.getAttribute</h2>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            姓名：<%=sessionName%><br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            日期：<%=sessionDate%><br />
        </p>
        <hr />
        <h2>application.getAttribute</h2>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            姓名：<%=applicationName%><br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            日期：<%=applicationDate%><br />
        </p>
        
        <h1>
        <%--使用超鏈接的形式跳轉，這是客戶端跳轉，URL地址會改變--%>
        <a href="${pageContext.request.contextPath}/cnblogs/xdp_gacl/jsp-four-scopes3.jsp">跳轉到jsp-four-scopes3.jsp</a>
        </h1>
    </body>
</html>

<%-- 
    Document   : jstl-url-tag
    Created on : 2018/7/16, 下午 02:01:12
    Author     : Ethan
--%>

<%@page language="java" pageEncoding="UTF-8"%>

<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSTL： -- url標籤實例</title>
    </head>
    <body>
        <c:out value="url標籤使用"></c:out>
        <h4>使用url標籤生成一個動態的url，並把值存入session中</h4>
        <hr />
        
        <c:url value="https://www.google.com" var="url" scope="session"></c:url>
        <a href="${url}">Google首頁(不帶參數)</a>
        <hr />
        
        <h4>
            配合 &lt;c:param&gt;標籤給url加上指定參數及參數值，生成一個動態的url然後存儲到paramUrl變量中
        </h4>
        <c:url value="http://www.google.com" var="paramUrl">
            <c:param name="userName" value="白虎神皇" />
            <c:param name="pwd">123456</c:param>
        </c:url>
        <a href="${paramUrl}">Google首頁(帶參數)</a>
    </body>
</html>

<%--
URL操作標籤——url標籤使用總結
<c:url>標籤的功能
　　<c:url>標籤用於在JSP頁面中構造一個URL地址，其主要目的是實現URL重寫。

<c:url>標籤的語法
【語法1】：指定一個url不做修改，可以選擇把該url存儲在JSP不同的範圍中。
<c:url
    value=”value”
    [var=”name”]
    [scope=”page|request|session|application”]
    [context=”context”]/>
【語法2】：配合 <c:param>標籤給url加上指定參數及參數值，可以選擇以name存儲該url。
<c:url
    value=”value”
    [var=”name”]
    [scope=”page|request|session|application”]
    [context=”context”]>
    　　<c:param name=”參數名” value=”值”>
</c:url>
  
----------------------------------------------------------------------------------------------------------------
<c:url>標籤屬性
  
屬性名                     是否支持EL                 屬性類型                                     屬性描述
  
  value                        true                            String                                   指定要構造的URL
  
  var                           false                          String                   指定將構造出的URL結果保存到Web域中的屬性名稱
  
  scope                       false                           String                    指定將構造出的URL結果保存到哪個Web域中
--%>
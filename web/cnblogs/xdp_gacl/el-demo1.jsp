<%-- 
    Document   : el-demo1
    Created on : 2018/7/17, 下午 12:11:58
    Author     : Ethan
--%>

<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="tw.ethan.cnblogs.xdp_gacl.javabean.Person"%>
<%@page language="java" pageEncoding="UTF-8"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>el表達式獲取數據</title>
    </head>
    <body>
        <%
            request.setAttribute("name", "白虎神皇");
        %>
        <%-- ${name}等同於pageContext.findAttribute("name") --%>
        使用EL表達式獲取數據：${name}
        <hr />
        
        <!-- 在jsp頁面中，使用el表達式可以獲得bean的屬性 -->
        <%
            Person p = new Person();
            p.setAge(12);
            request.setAttribute("person", p);
        %>
        使用EL表達式可以獲取bean的屬性：${person.age}
        <hr />
        
        <!-- 在jsp頁面中，使用el表達式獲取list集合中指定位置的數據 -->
        <%
            Person p1 = new Person();
            p1.setName("白虎神皇");
            
            Person p2 = new Person();
            p2.setName("孤傲蒼狼");
            
            List<Person> list = new ArrayList<>();
            list.add(p1);
            list.add(p2);
            
            request.setAttribute("list", list);
        %>
        <!-- 選取list指定位置的數據 -->
        ${list[1].name}
        
        <!-- 疊代List集合 -->
        <c:forEach var="person" items="${list}">
            ${person.name}
        </c:forEach>
        <hr />
        
        <!-- 在jsp頁面中，使用EL表達式獲取map集合的數據 -->
        <%
            Map<String, String> map = new LinkedHashMap<>();
            map.put("a", "aaaaxxxx");
            map.put("b", "bbbb");
            map.put("c", "cccc");
            map.put("1", "aaaa1111");
            request.setAttribute("map", map);
        %>
        <!-- 根據關鍵字取map集合的數據 -->
        ${map.c}<br />
        ${map["1"]}<br /><br />
        <ht />
        <!-- 疊代Map集合 -->
        <c:forEach var="me" items="${map}">
            ${me.key}=${me.value}<br />
        </c:forEach>
        <hr />
    </body>
</html>

<%--
EL表達式簡介
　　EL 全名為Expression Language。 EL主要作用：
　　1、獲取數據
　　　　EL表達式主要用於替換JSP頁面中的腳本表達式，以從各種類型的web域 中檢索java對象、獲取數據。 (某個web域 中的對象，訪問javabean的屬性、訪問list集合、訪問map集合、訪問數組)
　　2、執行運算
　　　　利用EL表達式可以在JSP頁面中執行一些基本的關係運算、邏輯運算和算術運算，以在JSP頁面中完成一些簡單的邏輯運算。 ${user==null}
　　3、獲取web開發常用對象
　　　　EL 表達式定義了一些隱式對象，利用這些隱式對象，web開發人員可以很輕鬆獲得對web常用對象的引用，從而獲得這些對像中的數據。
　　4、調用Java方法
　　　　EL表達式允許用戶開發自定義EL函數，以在JSP頁面中通過EL表達式調用Java類的方法。

----------------------------------------------------------------------------------------------------------------
獲取數據
　　使用EL表達式獲取數據語法："${標識符}"
EL表達式語句在執行時，會調用pageContext.findAttribute方法，用標識符為關鍵字，分別從page、request、session、application四個域中查找相應的對象，找到則返回相應對象，找不到則返回”” （注意，不是null，而是空字符串）。

　　EL表達式可以很輕鬆獲取JavaBean的屬性，或獲取數組、Collection、Map類型集合的數據

--%>
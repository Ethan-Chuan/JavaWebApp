<%-- 
    Document   : jsp-tag-include
    Created on : Jun 22, 2018, 12:08:01 PM
    Author     : Ethan
--%>

<%@page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP的include指令</title>
    </head>
    <body>
        <%-- 使用@include標籤引入其它JSP頁面 --%>
        <%@include file="jspf/head.jspf" %>
        
        <h1>網頁主體內容</h1>
        
        <%-- 使用@include標籤引入其它JSP頁面 --%>
        <%@include file="jspf/foot.jspf" %>
    </body>
</html>

<%--
在JSP中對於包含有兩種語句形式：
    1. @include指令
    2. <jsp:include>指令
----------------------------------------------------------------------------------------------------------------
@include可以包含任意的文件，當然，只是把文件的內容包含進來。

　　include指令用於引入其它JSP頁面，如果使用include指令引入了其它JSP頁面，那麼
JSP引擎將把這兩個JSP翻譯成一個servlet。所以include指令引入通常也稱之為靜態引入。

    語法：<%@ include file="relativeURL"%>，其中的file屬性用於指定被引入文件的路徑。
路徑以“/”開頭，表示代表當前web應用。

    include指令細節注意問題：
        (1. 被引入的文件必須遵循JSP語法。
        (2. 被引入的文件可以使用任意的擴展名，即使其擴展名是html，JSP引擎也會按照處理jsp
    頁面的方式處理它裡面的內容，為了見明知意，JSP規范建議使用.jspf（JSP fragments(片段)）
    作為靜態引入文件的擴展名。
        (3. 由於使用include指令將會涉及到2個JSP頁面，並會把2個JSP翻譯成一個servlet，所以
    這2個JSP頁面的指令不能衝突（除了pageEncoding和導包除外）。

----------------------------------------------------------------------------------------------------------------
總結：
@include指令
　　使用@include可以包含任意的內容，文件的後綴是什麼都無所謂。這種把別的文件內容
包含到自身頁面的@include語句就叫作靜態包含，作用只是把別的頁面內容包含進來，屬於
靜態包含。

jsp:include指令
　　jsp:include指令為動態包含，如果被包含的頁面是JSP，則先處理之後再將結果包含，而
如果包含的是非*.jsp文件，則只是把文件內容靜態包含進來，功能與@include類似。
--%>
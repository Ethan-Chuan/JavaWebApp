<%-- 
    Document   : tag-custom
    Created on : 2018/7/2, 下午 05:12:21
    Author     : Ethan
--%>

<%@page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!-- 使用taglib指令引用tab_custom標籤庫,標籤庫的前綴(prefix)可以隨便設置,如這裡設置成 prefix="tct" -->
<%@taglib uri="/WEB-INF/tlds/tag_custom.tld" prefix="tct" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>輸出客戶端的IP</title>
    </head>
    <body>
        你的IP地址是(使用java代碼獲取輸出)：
        <%
            //在jsp頁面中使用java代碼獲取客戶端IP地址
            String ip = request.getRemoteAddr();
            out.write(ip);
        %>
        <hr />
        你的IP地址是(使用自定義標籤獲取輸出)：
        <%-- 使用自定義標籤viewIP --%>
        <tct:viewIP/>
    </body>
</html>

<%--
    從運行效果中可以看到，使用自定義標籤就可以將jsp頁面上的java代碼移除掉，如需要
在jsp頁面上輸出客戶端的IP地址時，使用<tct:viewIP/>標籤就可以代替jsp頁面上的這些代
碼：
<%
    //在jsp頁面中使用java代碼獲取客戶端IP地址
    String ip = request.getRemoteAddr();
    out.write(ip);
%>

這就是開發和使用自定義標籤的好處，可以讓我們的Jsp頁面上不嵌套java代碼。
--%>
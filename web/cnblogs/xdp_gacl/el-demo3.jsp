<%-- 
    Document   : el-demo3
    Created on : 2018/7/17, 下午 04:17:47
    Author     : Ethan
--%>

<%@page language="java" pageEncoding="UTF-8"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>el隱式對象</title>
    </head>
    <body>
        <br />---------- 1. pageContext對象：獲取JSP頁面中的pageContext對象 ----------<br />
        ${pageContext}<br />
        <br />---------- 2. pageScope對象：從page域(pageScope)中查找數據 ----------<br />
        <%
            pageContext.setAttribute("name", "白虎神皇"); //map
        %>
        ${pageScope.name}<br />
        <br />---------- 3.requestScope對象：從request域(requestScope)中查找數據 ----------<br />
        <%
            request.setAttribute("name", "孤傲蒼狼"); //map
        %>
        ${request.name}<br />
        <br />---------- 4.sessionScope對象：從session域(sessionScope)中獲取數據 ----------<br />
        <%
            session.setAttribute("user", "google"); //map
        %>
        ${sessionScope.user}<br />
        <br />---------- 5.applicationScope對象：從application域(applicationScope)中獲取數據 ----------<br />
        <%
            application.setAttribute("user", "yahoo"); //map
        %>
        ${applicationScope.user}<br />
        <br />---------- 6.param對象：獲得用於保存請求參數map，並從map中獲取數據 ----------<br />
        <!-- http://localhost:8080/JavaWebApp/cnblogs/xdp_gacl/el-demo3.jsp?name=aaa -->
        ${param.name}<br />
        <!-- 此表達式會經常用在數據回顯上 -->
        <form action="${pageContext.request.contextPath}/cnblogs/xdp_gacl/el-demo3" method="post">
            <input type="text" name="username" value="${param.username}" />
            <input type="submit" value="註冊" />
        </form><br />
        <br />---------- 7.paramValues對象：paramValues獲得請求參數 //map{"",String[]} ---------- <br />
        <!-- http://localhost:8080/JavaWebApp/cnblogs/xdp_gacl/el-demo3.jsp?like=aaa&like=bbb -->
        ${paramValues.like[0]}<br />
        ${paramValues.like[1]}<br />
        <br />---------- 8.header對象：header獲得請求頭 ----------<br />
        ${header.Accept}<br />
        <%-- ${header.Accept-Encoding}這樣寫會報錯
        測試headerValues時，如果裏頭有"-"，例Accept-Encoding，則要headerValues["Accept-Encoding"]--%>
        ${header["Accept-Encoding"]}<br />
        <br />---------- 9.headerValues對象：headerValues獲得請求頭的值 ----------<br />
        <%-- headerValues表示一個保存了所有http請求字段的Map對象，它對於某個請求參數，返回的是一個String[]數組
        例如：headerValues.Accept返回的是一個String[]數組，headerValues.Accept[0]取出數組中的第一個值---%>
        ${headerValues.Accept[0]}<br />
        <%--${headerValues.Accept-Encoding} 這樣寫會報錯
        測試headerValues時，如果裏頭有"-"，例Accept-Encoding，則要headerValues["Accept-Encoding"]
        headerValues["Accept-Encoding"]返回的是一個String[]數組，headerValues["Accept-Encoding"][0]取出數組中的第一個值--%>
        ${headerValues["Accept-Encoding"][0]}<br />
        <br />----------10.cookie對象：cookie對象獲取客戶機提交的cookie ----------<br />
        <!-- 從cookie隱式對象中根據名稱獲取到的是cookie對象，要想獲取值，還需要.value -->
        ${cookie.JSESSIONID.value} <%-- 保存所有cookie的map --%><br />
        <br />---------- 11.initParam對象：initParam對象獲取在web.xml文件中配置的初始化參數 ----------<br />
        <%--
        <!-- web.xml文件中配置初起化參數 -->
        <context-param>
            <param-name>xxx</param-name>
            <param-value>yyyy</param-value>
        </context-param>
        <context-param>
            <param-name>root</param-name>
            <param-value>/JavaWebApp</param-value>
        </context-param>
        --%>
        <%-- 獲取servletContext中用於保存初始化參數的map --%>
        ${initParam.xxx}<br />
        ${initParam.root}
    </body>
</html>

<%--
EL表達式語言中定義了11個隱含對象，使用這些隱含對象可以很方便地獲取web開發中的一些常見對象，並讀取這些對象的數據。
　　語法：${隱式對象名稱}：獲得對象的引用

序號            隱含對象名稱                                        描述
  1                 pageContext                         對應於JSP頁面中的pageContext對象 (注意：取的是pageContext對象)
  2                 pageScope                           代表page域中用於保存屬性的Map對象
  3                 requestScope                        代表request域中用來保存屬性的Map對象
  4                 sessionScope                        代表session域中用來保存屬性的Map對象
  5                 applicationScope                代表appliecation域中用來保存屬性的Map對象
  6                 param                                   表示一個保存了所有請求參數的Map對象
  7                 paramValues                         表示一個保存了所有請求參數的Map對象，它對於某個請求參數，返回的是一個String[]
  8                 header                                  表示一個保存了所有http請求頭字段的Map對象，注意：如果頭裡面有"-"，例Accept-Encoding，則要header["Accept-Encoding"]
  9                 headerValues                        表示一個保存了所有http請求頭字段的Map對象，它對於某個請求參數，返回的是一個String[]。注意：如果頭裡面有"-"，例Accept-Encoding，則要header["Accept-Encoding"]
  10                cookie                                  表示一個保存了所有cookie的Map對象
  11                initParam                               表示一個保存了所有web應用初始化參數的map對象
  
----------------------------------------------------------------------------------------------------------------
測試header和headerValues時，如果頭里面有“-” ，例Accept-Encoding，則要header["Accept-Encoding"]、headerValues["Accept-Encoding"]
測試cookie時，例${cookie.key}取的是cookie對象，如訪問cookie的名稱和值，須${cookie.key.name}或${cookie.key.value}
--%>
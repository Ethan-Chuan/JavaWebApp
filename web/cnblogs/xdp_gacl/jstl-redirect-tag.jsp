<%-- 
    Document   : jstl-redirect-tag
    Created on : 2018/7/16, 下午 02:18:30
    Author     : Ethan
--%>

<%@page language="java" pageEncoding="UTF-8"%>

<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSTL： -- redirect標籤實例</title>
    </head>
    <body>
        <c:redirect url="http://www.google.com">
            <%--在重定向時使用<c:param>標籤為URL添加了兩個參數： uname=ABC和password=123 --%>
            <c:param name="uname">ABC</c:param>
            <c:param name="password">123</c:param>
        </c:redirect>
    </body>
</html>

<%--
URL操作標籤——redirect標籤的使用
<c:redirect>標籤的功能
　　該標籤用來實現請求的重定向。同時可以配合使用<c:param>標籤在url中加入指定的參數。

<c:redirect>標籤的語法
【語法1】：
<c:redirect url=”url” [context=”context”]/>
【語法2】：
<c:redirect url=”url”[context=”context”]>
    <c:param name=”name1” value=”value1”>
</c:redirect>
【參數說明】：
（1）url指定重定向頁面的地址，可以是一個string類型的絕對地址或相對地址。
（2）context用於導入其他web應用中的頁面。

----------------------------------------------------------------------------------------------------------------
<c:redirect>標籤屬性
  
屬性名                     是否支持EL                 屬性類型                                     屬性描述
  
  url                            true                            String                    指定要轉發或重定向到的目標資源的URL地址
  
  context                      true                            String                   當要使用相對路徑重定向到同一個服務器下的其他WEB應用
|                                                                                               程序中的資源時，context屬性指定其他WEB應用程序的名稱

----------------------------------------------------------------------------------------------------------------
在JSP頁面進行URL的相關操作時，經常要在URL地址後面附加一些參數。 
<c:param>標籤可以嵌套在<c:import>、<c:url>或<c:redirect>標籤內，為這些標籤所使用的URL地址附加參數。
<c:param>標籤在為一個URL地址附加參數時，將自動對參數值進行URL編碼，這也就是使用<c:param>標籤的最大好處。

----------------------------------------------------------------------------------------------------------------
<c:param> 標籤屬性
  
  Attribute                                              Description                                                    Required	Default
name                               Name of the request parameter to set in the URL                          Yes	None
value                               Value of the request parameter to set in the URL                            No	Body
--%>
<%-- 
    Document   : jstl-out-tag
    Created on : 2018/7/13, 下午 02:48:41
    Author     : Ethan
    表達式控制標籤——out標籤使用
--%>

<%@page language="java" pageEncoding="UTF-8"%>

<%-- 引入JSTL核心標籤庫 --%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSTL: --表達式控制標籤"out"標籤的使用</title>
    </head>
    <body>
        <h3><c:out value="下面的代碼演示了c:out的使用，以及在不同屬性值狀態下的結果。" /></h3>
        <hr />
        <ul>
            <%-- (1) 直接輸出了一個字符串 --%>
            <li> (1) <c:out value="JSTL的out標籤使用" /></li>
            
            <li> (2) <c:out value="<a href='http://www.google.com/'>點擊鏈結到Google首頁</a>" /></li>
            <%-- escapeXml="false"表示value值中的html標籤不進行轉義，而是直接輸出 --%>
            <li> (3) <c:out value="<a href='http://www.google.com/'>點擊鏈結到Google首頁</a>" escapeXml="false" /></li>
            
            <%-- (4) 字符串中有轉義字符，但在默認情況下沒有轉換 --%>
            <li> (4) <c:out value="&lt未使用字符轉義&gt" /></li>
            <%-- (5) 使用了轉義字符&lt和&gt分別轉換成<和>符號 --%>
            <li> (5) <c:out value="&lt使用字符轉義&gt" escapeXml="false" ></c:out></li>
            
            <%-- (6) 設定了默認值，從EL表達式 ${null} 得到空值，所以直接輸出設定的默認值。 --%>
            <li> (6) <c:out value="${null}">使用了默認值</c:out></li>
            <%-- (7) 未設定默認值，輸出結果為空。 --%>
            <li> (7) <c:out value="${null}"></c:out></li>
            
            <%-- (8) 設定了默認值，從EL表達式 ${null} 得到空值，所以直接輸出設定的默認值。 --%>
            <li> (8) <c:out value="${null}" default="默認值"/> </li>
            
            <%-- (9) 未設定默認值，輸出結果為空。 --%>
            <li> (9) <c:out value="${null}" /> </li>
        </ul>
    </body>
</html>

<%--
<c:out>標籤主要是用來輸出數據對象（字符串、表達式）的內容或結果。
在使用Java腳本輸出時常使用的方式為： <% out.println(“字符串”)%> 或者<%=表達式%> ，在web開發中，為了避免暴露邏輯代碼會盡量減少頁面中的Java腳本，使用<c:out>標籤就可以實現以上功能。

(1.  <c:out value=”字符串”>
(2.  <c:out value=”EL表達式”>

JSTL的使用是和EL表達式分不開的，EL表達式雖然可以直接將結果返回給頁面，但有時得到的結果為空，<c:out>有特定的結果處理功能，EL的單獨使用會降低程序的易讀性，建議把EL的結果輸入放入<c:out>標籤中。

----------------------------------------------------------------------------------------------------------------
<c:out>標籤的使用有兩種語法格式：
　　　　【語法1】：<c:out value=”要顯示的數據對象” [escapeXml=”true | false”] [default=”默認值”]/>
　　　　【語法2】：<c:out value=”要顯示的數據對象” [escapeXml=”true | false”]>默認值</c:out>
　　這兩種方式沒有本質的區別，只是格式上的差別。 [escapeXml=”true | false”] [default=”默認值”]這些使用[]屬性表示是不是必須的。

----------------------------------------------------------------------------------------------------------------
<c:out>標籤屬性
  
屬性名                     是否支持EL                 屬性類型                                     屬性描述
  
  value                        true                           Object                                   指定要輸出的內容
  
  escapeXml                true                           Boolean                   指定是否將> , < , & , ' , " 等特殊字符進行
|                                                                                               HTML編碼轉換後再進行輸出。默認值為true。
  
  default                      true                          Object                    指定如果value屬性值為null時，所輸出的默認值
--%>

<%--
JSTL標籤庫的使用是為彌補html標籤的不足，規範自定義標籤的使用而誕生的。使用JSLT標籤的目的就是不希望在jsp頁面中出現java邏輯代碼

JSTL標籤庫的分類
    (1) 核心標籤(用得最多)
    (2) 國際化標籤(I18N格式化標籤)
    (3) 數據庫標籤(SQL標籤，很少使用)
    (4) XML標籤(幾乎不用)
    (5) JSTL函數(EL函數)

JSTL的核心標籤庫標籤共13個，使用這些標籤能夠完成JSP頁面的基本功能，減少編碼工作。

　　從功能上可以分為4類：表達式控制標籤、流程控制標籤、循環標籤、URL操作標籤。
　　　　（1）表達式控制標籤：out標籤、set標籤、remove標籤、catch標籤。
　　　　（2）流程控制標籤：if標籤、choose標籤、when標籤、otherwise標籤。
　　　　（3）循環標籤：forEach標籤、forTokens標籤。
　　　　（4）URL操作標籤：import標籤、url標籤、redirect標籤、param標籤。

　　在JSP頁面引入核心標籤庫的代碼為：<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
--%>
<%-- 
    Document   : wrong-calculation
    Created on : Jun 22, 2018, 11:03:21 AM
    Author     : Ethan
--%>

<%-- 
        如果設置了某個JSP頁面的errorPage屬性 ( errorPage="/cnblogs/xdp_gacl/error.jsp" )
        那麼在web.xml文件中設置的錯誤處理將不對該頁面起作用。 
--%>
<%@page language="java" import="java.util.*"  pageEncoding="UTF-8"  %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>計算出錯頁面</title>
    </head>
    <body>
        <%
            //這行代碼肯定出錯，因為除數是0，一運行就會拋出異常
            int x = 1/0;
        %>
    </body>
</html>

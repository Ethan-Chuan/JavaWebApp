<%-- 
    Document   : el-demo5
    Created on : 2018/7/17, 下午 06:20:59
    Author     : Ethan
--%>

<%@page import="tw.ethan.cnblogs.xdp_gacl.ElDemo5"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.List"%>
<%@page language="java" pageEncoding="UTF-8"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>EL函數庫中的方法使用範例</title>
    </head>
    <body>
        <h3>fn:toLowerCase函數使用範例：</h3>
        <%-- fn:toLowerCase函數將一個字符串包含的所有字符轉換為小寫形式，並返回轉換後的字符串，
        它接收一個字符串類型的參數。fn:toLowerCase("")的返回值為空字符串 --%>
        <%-- fn:toLowerCase("Www.CNBLOGS.COM") 的返回值為字符串"www.cnblogs.com" --%>
        fn:toLowerCase("Www.CNBLOGS.COM")的結果是：${fn:toLowerCase("Www.CNBLOGS.COM")}
        <hr />
        
        <h3>fn:toUpperCase函數使用範例：</h3>
        <%-- fn:toUpperCase函數將一個字符串包含的所有字符轉換為大寫形式，並返回轉換後的字符串，
        它接收一個字符串類型的參數。fn:toUpperCase("")的返回值為空字符串 --%>
        fn:toUpperCase("cnblogs.com")的結果是：${fn:toUpperCase("cnblogs.com")}
        <hr />
        
        <h3>fn:trim函數使用範例：</h3>
        <%-- fn:trim函數刪除一個字符串的首尾的空格，並返回刪除空格後的結果字符串
        它接收一個字符串類型的參數。需要注意的是，fn:trim函數不能刪除字符串中間位置的空格。 --%>
        fn:trim("   cnblogs.com  ")的結果是：${fn:trim("   cnblogs.com  ")}
        <hr />
        
        <h3>fn:length函數使用範例</h3>
        <%-- fn:length函數返回一個集合或數組大小，或返回一個字符串中包含的字符的個數，返回值為int類型。
        fn:length函數接收一個參數，這個參數可以是<c:forEach>標籤的items屬性支持的任何類型，包含任意類型的
        數組, java.util.Collection, java.util.Iterator, java.util.Enumeration, java.util.Map等類的實例對象和字符串。
        如果fn:length函數的參數為null或者是元素個數為0的集合或數組對象，則函數返回0; 如果參數是空字符串，則函數返回0 --%>
        <%
            List<String> list = Arrays.asList("1", "2", "3");
            request.setAttribute("list", list);
        %>
        fn:length(list)計算集合list的size的值是：${fn:length(list)}
        <br />
        fn:length("cnblogs.com")計算字符串的長度是：${fn:length("cnblogs.com")}
        <hr />
        
        <h3>fn:split函數使用範例：</h3>
        <%--fn:split函數以指定字符串作為分隔符，將一個字符串分割成字符串數組並返回這個字符串數組。
        fn:split函數接收兩個字符串類型的參數，第一個參數表示要分割的字符串，第二個參數表示做為分隔符的字符串 --%>
        fn:split("cnblogs.com", ".")[0]的結果：${fn:split("cnblogs.com", ".")[0]}
        <hr />
        
        <h3>fn:join函數使用範例</h3>
        <%--fn:join函數以一個字符串作為分隔符，將一個字符串數組中的所有元素合併為一個字符串返回合併後的結果字符串。
        fn:join函數接收兩個參數，第一個參數是要操作的字符串數組，第二個參數是作為分隔符的字符串。
        如果fn:join函數的第二個參數是空字符串，則fn:join函數的返回值直接將元素連接起來。 --%>
        <%
            String[] StringArray = new String[]{"www", "iteye", "com"};
            pageContext.setAttribute("StringArray", StringArray);
        %>
        <%-- fn:join(StringArray,".")返回字符串"www.iteye.com" --%>
        fn:join(StringArray,".")的結果是：${fn:join(StringArray,".")}
        <br />
        <%-- fn:join( fn:split("www,iteye,com", ","), "." )返回字符串"www.iteye.com" --%>
        fn:join( fn:split("www,iteye,com", ","), "." )的結果是：${fn:join( fn:split("www,iteye,com", ","), "." )}
        <hr />
        
        <h3>fn:indexOf函數使用範例：</h3>
        <%--fn:indexOf函數返回指定字符串在一個字符串中第一次出現的索引值，返回值為int類型。
        fn:indexOf函數接收兩個字符串類型的參數，如果第一個參數字符串中包含第二個參數字符串，
        那麼，不管第二個參數字符串在第一個參數字符串中出現幾次，fn:indexOf函數總是返回第一次出現的索引值;
        如果第一個參數中不包含第二個參數，則fn:indexOf函數返回-1。如果第二個參數為空字符串，則fn:indexOf函數總是返回0。--%>
        fn:indexOf("www.iteye.com","eye")的返回值為：${fn:indexOf("www.iteye.com","eye")}
        <hr />
        
        <h3>fn:contains函數使用範例：</h3>
        <%--fn:contains函數檢測一個字符串中使否包含指定的字符串，返回值為布林類型。
        fn:contains函數在比較兩個字符串是否相等時是大小寫敏感的。
        fn:contains函數接收兩個字符串類型的參數，如果第一個參數字符串中包含第二個參數字符串，則fn:contains函數返回true，否則返回false。
        如果第二個參數的值為空字符串，則fn:contains函數總是返回true。
        實際上，fn:contains(string, substring)等價於fn:indexOf(string, substring) != -1
        忽略大小的EL函數：fn:containsIgnoreCase --%>
        <%
            ElDemo5 user = new ElDemo5();
            String[]likes = new String[]{"sing", "dance"};
            user.setLikes(likes);
            // 數據回顯
            request.setAttribute("user", user);
        %>
        <%-- 使用el函數回顯數據 --%>
        <input type="checkbox" name="like" 
               value="sing" ${fn:contains( fn:join(user.likes,","),"sing") ? "checked" : ""}/>唱歌
        <input type="checkbox" name="like" 
               value="dance" ${fn:contains( fn:join(user.likes,","),"dance") ? "checked" : ""}/>跳舞
        <input type="checkbox" name="like" 
               value="basketball" ${fn:contains( fn:join(user.likes,","),"basketball") ? "checked" : ""}/>籃球
        <input type="checkbox" name="like" 
               value="football" ${fn:contains( fn:join(user.likes,","),"football") ? "checked" : ""}/>足球
        <hr />
        
        <h3>fn:startsWith函數和fn:endsWith函數使用範例：</h3>
        <%-- fn:startsWith函數用於檢測一個字符串是否是以指定字符串開始的，返回值為布林類型。      
        fn:startsWith函數接收兩個字符串類型的參數，如果第一個參數字符串以第二個參數字符串開始，則函數返回true，否則函數返回false。
        如果第二個參數為空字符串，則fn:startsWith函數總是返回true。
        與fn:startsWith函數對應的另一個EL函數為：fn:endsWith，用於檢測一個字符串是否是以指定字符串結束的，返回值為布林類型。--%>
        fn:startsWith("www.iteye.com","iteye")的返回值為：${fn:startsWith("www.iteye.com","iteye")}
        <br/>
        fn:endsWith("www.iteye.com","com")的返回值為：${fn:endsWith("www.iteye.com","com")}
        <hr/>
        
        <h3>fn:replace使用範例：</h3>
        <%-- fn:replace函數將一個字符串中包含的指定子字符串替換為其它的指定字符串，並返回替換後的結果字符串。
        fn:replace方法接收三個字符串類型的參數，第一個參數表示要操作的源字符串，第二個參數表示源字符串中要被替換的子字符串，
               第三個參數表示要被替換成的字符串。--%>
        fn:replace("www iteye com ", " ", ".")的返回值為字符串：${fn:replace("www iteye com", " ", ".")}
        <hr/>
        
        <h3>fn:substring使用範例：</h3>
        <%-- fn:substring函數用於截取一個字符串的子字符串並返回截取到的子字符串。
        fn:substring函數接收三個參數，第一個參數是用於指定要操作的源字符串，第二個參數是用於指定截取子字符串開始的索引值，
               第三個參數是用於指定截取子字符串結束的索引值，第二個參數和第三個參數都是int類型，其值都從0開始。--%>
        fn:substring("www.it315.org", 4, 9) 的返回值為字符串：${fn:substring("www.it315.org", 4, 9)}
        <hr />
        
        <h3>fn:substringAfter函數和fn:substringBefore函數使用範例：</h3>
        <%-- fn:substringAfter函數用於截取並返回一個字符串中的指定子字符串第一次出現之後的子字符串。
        fn:substringAfter函數接收兩個字符串類型的參數，第一個參數表示要操作的源字符串，第二個參數表示指定的子字符串
               與之對應的EL函數為：fn:substringBefore--%>
        fn:substringAfter("www.it315.org",".")的返回值為字符串：${fn:substringAfter("www.it315.org",".")}
        <br/>
        fn:substringBefore("www.it315.org",".")的返回值為字符串：${fn:substringBefore("www.it315.org",".")}
        <hr/>
    </body>
</html>

<%--
由於在JSP頁面中顯示數據時，經常需要對顯示的字符串進行處理，SUN公司針對於一些常見處理定義了一套EL函數庫供開發者使用。
這些EL函數在JSTL開發包中進行描述，因此在JSP頁面中使用SUN公司的EL函數庫，需要導入JSTL開發包，並在頁面中導入EL函數庫。
--%>
<%-- 
    Document   : html-escape-tag-test
    Created on : 2018/7/12, 下午 06:42:03
    Author     : Ethan
--%>

<%@page language="java" pageEncoding="UTF-8"%>

<%@taglib  uri="/WEB-INF/tlds/web_tag.tld" prefix="wtt" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>html轉義標籤測試</title>
    </head>
    <body>
        <wtt:htmlEscape>
            <a href="http://www.google.com">訪問Google首頁</a>
        </wtt:htmlEscape>
    </body>
</html>

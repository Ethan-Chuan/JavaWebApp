<%-- 
    Document   : referer-tag-test
    Created on : 2018/7/12, 下午 03:20:56
    Author     : Ethan
--%>

<%@page language="java" pageEncoding="UTF-8"%>

<%-- 在jsp頁面中導入自定義標籤庫 --%>
<%@taglib  uri="/WEB-INF/tlds/web_tag.tld" prefix="wtt" %>

<%--在Jsp頁面中使用防盜鏈標籤
當用戶嘗試直接通過URL地址 (http://localhost:8080/JavaWebApp/cnblogs/xdp_gacl/webtag/referer-tag-test.jsp) 訪問這個頁面時
防盜鏈標籤的標籤處理器內部就會進行處理，將請求重定向到 /cnblogs/xdp_gacl/webtag/index.jsp
--%>
<wtt:referer site="http://localhost:8080" page="/cnblogs/xdp_gacl/webtag/index.jsp" />

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>防盜鏈標籤測試</title>
    </head>
    <body>
        網站內部資料
    </body>
</html>

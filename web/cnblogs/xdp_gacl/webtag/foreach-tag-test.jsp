<%-- 
    Document   : foreach-tag-test
    Created on : 2018/7/12, 下午 05:24:40
    Author     : Ethan
--%>

<%@page  import="java.util.HashMap" %>
<%@page  import="java.util.Map" %>
<%@page  import="java.util.ArrayList" %>
<%@page  import="java.util.List" %>
<%@page language="java" pageEncoding="UTF-8"%>

<%-- 在jsp頁面中導入自定義標籤庫 --%>
<%@taglib  uri="/WEB-INF/tlds/web_tag.tld" prefix="wtt" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>foreach標籤測試</title>
    </head>
    
    <%
        // list集合
        List<String> listData = new ArrayList<>();
        listData.add("白虎皇神");
        listData.add("tiger");
        
        // 對象數組
        Integer intObjArr[] = new Integer[]{1, 2, 3};
        
        // 基本數據類型數組
        int intArr[] = new int[]{4, 5, 6};
        
        // map集合
        Map<String, String> mapData = new HashMap<>();
        mapData.put("a", "aaaaaa");
        mapData.put("b", "bbbbbb");
        
        // 將集合存儲到pageContext對象中
        pageContext.setAttribute("listData", listData);
        pageContext.setAttribute("intObjArr", intObjArr);
        pageContext.setAttribute("intArr", intArr);
        pageContext.setAttribute("mapData", mapData);
    %>
    
    <body>
        <%-- 疊代存儲在pageContext對象中的list集合 --%>
        <wtt:foreach items="${listData}" var="str">
            ${str} <br />
        </wtt:foreach>
        <hr />
        <%-- 疊代存儲在pageContext對象中的數組 --%>
        <wtt:foreach items="${intObjArr}" var="num">
            ${num} <br />
        </wtt:foreach>
        <hr />
        <%-- 疊代存儲在pageContext對象中的數組 --%>
        <wtt:foreach items="${intArr}" var="num">
            ${num} <br />
        </wtt:foreach>
        <hr />
        <%-- 疊代存儲在pageContext對象中的map集合 --%>
        <wtt:foreach items="${mapData}" var="me">
            ${me} <br />
        </wtt:foreach>
    </body>
</html>

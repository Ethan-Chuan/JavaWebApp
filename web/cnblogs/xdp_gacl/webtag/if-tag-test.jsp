<%-- 
    Document   : if-tag-test
    Created on : 2018/7/12, 下午 04:11:15
    Author     : Ethan
--%>

<%@page language="java" pageEncoding="UTF-8"%>

<%@taglib uri="/WEB-INF/tlds/web_tag.tld" prefix="wtt" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>if鏈標籤測試</title>
    </head>
    <body>
        <%-- if標籤的test屬性值為true，標籤體的內容會輸出 --%>
        <wtt:if test="true">
            <h3>網站內部資料</h3>
        </wtt:if>
        <%-- if標籤的test屬性值為false，標籤體的內容不會輸出 --%>
        <wtt:if test="false">
            這裡的內部不輸出
        </wtt:if>
    </body>
</html>

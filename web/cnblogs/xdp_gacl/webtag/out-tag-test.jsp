<%-- 
    Document   : out-tag-test
    Created on : 2018/7/12, 下午 06:59:23
    Author     : Ethan
--%>

<%@page language="java" pageEncoding="UTF-8"%>

<%@taglib uri="/WEB-INF/tlds/web_tag.tld" prefix="wtt"  %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>out標籤測試</title>
    </head>
    <body>
        <%-- 使用 out 標籤輸出 content 屬性的內容 --%>
        <wtt:out content="<a href=\"http://www.google.com\">訪問Google首頁</a>" />
        <br />
        <hr/>
        <%-- 使用 out 標籤輸出 content 屬性的內容 --%>
        <wtt:out content="<a href=\"http://www.google.com\">訪問Google首頁</a>"  escapeHtml="true"/>
    </body>
</html>

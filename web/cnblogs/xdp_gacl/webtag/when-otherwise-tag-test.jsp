<%-- 
    Document   : when-otherwise-tag-test
    Created on : 2018/7/12, 下午 04:40:17
    Author     : Ethan
--%>

<%@page language="java" pageEncoding="UTF-8"%>

<%@taglib  uri="/WEB-INF/tlds/web_tag.tld" prefix="wtt" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>when 和 otherwise 標籤測試</title>
    </head>
    <body>
        <wtt:choose>
            <wtt:when test="${user==null}">
                when標籤 標籤體輸出的內容：
                <h3>用戶為空</h3>
            </wtt:when>
            <wtt:otherwise>
                用戶不為空
            </wtt:otherwise>
        </wtt:choose>
        <hr />
        <wtt:choose>
            <wtt:when test="${user!=null}">
                用戶不為空
            </wtt:when>
            <wtt:otherwise>
                otherwise標籤 標籤體輸出的內容：
                <h3>用戶為空</h3>
            </wtt:otherwise>
        </wtt:choose>
    </body>
</html>

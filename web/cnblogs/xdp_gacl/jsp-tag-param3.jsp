<%-- 
    Document   : jsp-tag-param3
    Created on : Jun 22, 2018, 6:40:59 PM
    Author     : Ethan
--%>

<%@page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>jsp-tag-param3</title>
    </head>
    <body>
        <h1>跳轉之後的頁面！！</h1>
        <h1>接收從 jsp-tag-param2.jsp 傳遞過來的參數：</h1>
        <h1><%=request.getParameter("ref1")%> </h1>
        <h1><%=request.getParameter("ref2")%> </h1>
    </body>
</html>

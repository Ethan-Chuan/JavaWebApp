<%-- 
    Document   : jsp-tag-param2
    Created on : Jun 22, 2018, 6:40:52 PM
    Author     : Ethan
--%>

<%@page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>jsp-tag-param2</title>
    </head>
    <body>
        <h1>使用 jsp:forward 標籤調轉到 jsp-tag-param3.jsp</h1>
        <%--使用<jsp:param>標籤向 jsp-tag-param3.jsp 傳遞參數--%>
        <jsp:forward page="jsp-tag-param3.jsp">
            <jsp:param name="ref1" value="hello" />
            <jsp:param name="ref2" value="jsp-tag-param222222" />
        </jsp:forward>
    </body>
</html>

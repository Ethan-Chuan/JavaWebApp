<%-- 
    Document   : jstl-import-tag
    Created on : 2018/7/16, 下午 01:02:29
    Author     : Ethan
--%>

<%@page language="java" pageEncoding="UTF-8"%>

<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSTL： -- import 標籤實例</title>
    </head>
    <body>
        <h4><c:out value="import實例" /></h4>
        <hr />
        <h4><c:out value="絕對路徑引用的實例" /></h4>
        <%--使用絕對路徑導入Google首頁，
        導入時使用<c:catch></c:catch>捕抓異常--%>
        <c:catch var="error1">
            <c:import url="https://www.google.com" charEncoding="utf-8" />
        </c:catch>
        ${error1}
        <hr />
        
        <h4>
            <c:out value="相對路徑引用本應用中的文件" />
        </h4>
        <hr />
        <%--使用相對路徑導入同一文件夾下的"JSTL的import標籤使用說明.txt"文件,
        接收的字符編碼格式使用charEncoding設置為utf-8。--%>
        <c:catch var="error2">
            <c:import url="JSTL的import標籤使用說明.txt" charEncoding="utf-8" />
        </c:catch>
        ${error2}}
        <hr />
        
        <h4><c:out value="使用字符串輸出相對路徑引用的實例，並保存在session範圍內" /></h4>
        <%-- 導入"JSTL的import標籤使用說明.txt",
        使用 var 定義的變量接收要導入的文件，並存儲在session中,
        如果在其他頁面同樣也要導入該文件，只須使用<c:out>輸出"JSTL的import標籤使用說明.txt"的值即可--%>
        <c:catch var="error3">
            <c:import var="myurl"
                      url="JSTL的import標籤使用說明.txt"
                      scope="session"
                      charEncoding="utf-8" />
            <c:out value="${myurl}"></c:out>
            <hr />
            <c:out value="${myurl}"/>
        </c:catch>
            ${error3}
    </body>
</html>

<%--
URL操作標籤——import標籤使用講解
<c:import>標籤的功能
　　該標籤可以把其他靜態或動態文件包含到本JSP頁面，與<jsp:include>的區別為：<jsp:include>只能包含同一個web應用中的文件。而<c:import>可以包含其他web應用中的文件，甚至是網絡上的資源。

<c:import>標籤的語法
【語法1】：
<c:import
    url=”url”
    [context=”context”]
    [value=”value”]
    [scope=”page|request|session|application”]
    [charEncoding=”encoding”]/>

【語法2】：
<c:import
    url=”url”
    varReader=”name”
    [context=”context”]
    [charEncoding=”encoding”]/>

【參數說明】：
（1）URL為資源的路徑，當引用的資源不存在時系統會拋出異常，因此該語句應該放在<c:catch></c:catch>語句塊中捕獲。
（2）引用資源有兩種方式：絕對路徑和相對路徑。
    使用絕對路徑的示例如下：<c:import url=”http://www.baidu.com”>
    使用相對路徑的示例如下：<c:import url=”aa.txt”>，aa.txt放在同一文件目錄。
（3）如果以“/”開頭表示應用的根目錄下。例如：tomcat應用程序的根目錄文件夾為webapps。導入webapps下的文件bb.txt的編寫方式為：<c:import url=”/bb.txt”>
    如果訪問webapps管理文件夾中其他web應用就要用context屬性。
（4）context屬性用於在訪問其他web應用的文件時，指定根目錄。例如，訪問root下的index.jsp的實現代碼為：<c:import url=”/index.jsp” context=”/root”>
    等同於webapps/root/index.jsp
（5）var、scope、charEncoding、varReader是可選屬性。
  
----------------------------------------------------------------------------------------------------------------
<c:import>標籤屬性
  
  Attribute	                             Description	                              Required	Default
url	                            URL to retrieve and import into the page                 Yes     	None
context	                     / followed by the name of a local web application	      No      	Current application
charEncoding	               Character set to use for imported data	      No	              ISO-8859-1
var	                         Name of the variable to store imported text	      No      	Print to page
scope	                     Scope of the variable used to store imported text	      No    	Page
varReader	Name of an alternate variable to expose java.io.Reader	      No     	None
--%>
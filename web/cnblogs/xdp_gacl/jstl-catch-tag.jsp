<%-- 
    Document   : jstl-catch-tag
    Created on : 2018/7/16, 上午 10:59:29
    Author     : Ethan
--%>

<%@page language="java" pageEncoding="UTF-8"%>

<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSTL： --表達式控制標籤"catch"標籤實例</title>
    </head>
    <body>
        <h1>catch標籤實例</h1>
        <hr />
        <%-- 把容易產生異常的代碼放在<c:catch></c:catch>中，
        自定義一個變量errorInfo用於存儲異常信息 --%>
        <c:catch var="errorInfo">
            <%-- 實現了一段異常代碼，向一個不存在的JavaBean中插入一個值 --%>
            <c:set target="person" property="hao"></c:set>
        </c:catch>
        <%-- 用EL表達式得到errorInfo的值，並使用<c:out>標籤輸出 --%>
        異常：<c:out value="${errorInfo}" /><br />
        異常 errorInfo.getMessage：<c:out value="${errorInfo.message}" /><br />
        異常 errorInfo.getCause：<c:out value="${errorInfo.cause}" /><br />
        異常 errorInfo.getStackTrace：<c:out value="${errorInfo.stackTrace}" />
    </body>
</html>

<%--
表達式控制標籤——catch標籤的使用
<c:catch>標籤的功能
　　<c:catch>標籤用於捕獲嵌套在標籤體中的內容拋出的異常。

<c:catch>標籤的語法
　　其語法格式如下：<c:catch [var="varName"]>容易產生異常的代碼</c:catch>
　　var屬性用於標識<c:catch>標籤捕獲的異常對象，它將保存在page這個Web域中。
--%>
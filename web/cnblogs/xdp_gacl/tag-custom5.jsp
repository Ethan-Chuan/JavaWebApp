<%-- 
    Document   : tag-custom5
    Created on : 2018/7/9, 下午 05:19:38
    Author     : Ethan
--%>

<%@page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<%-- 在 jsp 頁面中導入自定義標籤庫 --%>
<%@taglib  uri="/WEB-INF/tlds/tag_custom.tld" prefix="tct" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>修改jsp頁面內容輸出</title>
    </head>
    <body>
        <%-- 在 jsp 頁面中使用自定義標籤 custom5標籤 --%>
        <tct:custom5>
            <h3>tag custom (traditional)</h3>
        </tct:custom5>
    </body>
</html>

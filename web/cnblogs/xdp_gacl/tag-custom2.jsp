<%-- 
    Document   : tag-custom2
    Created on : 2018/7/9, 下午 03:02:24
    Author     : Ethan
--%>

<%@page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<%-- 在 jsp 頁面中導入自定義標籤庫 --%>
<%@taglib  uri="/WEB-INF/tlds/tag_custom.tld" prefix="tct" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>控制標籤體是否執行</title>
    </head>
    <body>
        <%-- 在jsp頁面中使用自然定義標籤 custom2 標籤是帶有標籤體的，標籤體的內容是"白虎神皇"這幾個字符串 --%>
        <tct:custom2>
            白虎神皇
        </tct:custom2>
    </body>
</html>

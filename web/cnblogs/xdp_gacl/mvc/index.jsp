<%-- 
    Document   : index
    Created on : Jun 28, 2018, 3:23:27 PM
    Author     : Ethan
--%>

<%@page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<%-- 為了避免在jsp頁面中出現java代碼，這裡引入jstl標籤庫，利用jstl標籤庫提供的標籤來做一些邏輯判斷處理 --%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>首頁</title>
        <script type="text/javascript">
            function doLogout(){
                // 訪問LogoutServlet註銷當前登錄的用戶
                window.location.href="${pageContext.request.contextPath}/logout-servlet";
            }
        </script>
    </head>
    <body>
        <h1>這是網站首頁</h1>
        <hr />
        <c:if test="${user==null}">
            <a href="${pageContext.request.contextPath}/register-ui" target="_blank" >註冊</a>
            <a href="${pageContext.request.contextPath}/login-ui" >登錄</a>
        </c:if>
        <c:if test="${user!=null}" >
            歡迎您：${user.userName}
            <input type="button" value="退出登錄" onclick="doLogout()" />
        </c:if>
    </body>
</html>

<%--
    Servlet+JSP+JavaBean模式(MVC)適合開發複雜的web應用，在這種模式下，servlet負責
處理用戶請求，jsp負責數據顯示，javabean負責封裝數據。 Servlet+JSP+JavaBean模式程
序各個模塊之間層次清晰，web開發推薦採用此種模式。

　　這里以一個最常用的用戶登錄註冊程序來講解Servlet+JSP+JavaBean開發模式，通過
這個用戶登錄註冊程序綜合案例，把之前的學過的XML、Xpath、Servlet、jsp的知識點都
串聯起來。
----------------------------------------------------------------------------------------------------------------
開發包
(1. dom4j-1.6.1.jar                             dom4j用於操作XML文件
(2. jaxen-1.1.6.jar                              用於解析XPath表達式
(3. commons-beanutils-1.9.3.jar         工具類，用於處理bean對象
(4. commons-logging-1.2.jar              commons-beanutils-1.9.3.jar的依賴jar包
(5. JSTL 1.2.1 - jstl-impl.jar              jstl標籤庫和EL表達式依賴包
(6. JSTL 1.2.1 - jstl-api.jar                jstl標籤庫和EL表達式依賴包
----------------------------------------------------------------------------------------------------------------
結構
(1. tw.ethan.cnblogs.xdp_gacl.mvc.domain  domain(域模型)層
    存放系統的JavaBean類(只包含簡單的屬性以及屬性對應的get和set方法，不包含具體的
業務處理方法)，提供給【數據訪問層】、【業務處理層】、【 Web層】來使用 
    domain(域模型)層
  
(2. tw.ethan.cnblogs.xdp_gacl.mvc.dao 
    存放訪問數據庫的操作接口類 
    數據訪問層
  
(3. tw.ethan.cnblogs.xdp_gacl.mvc.dao.impl 
    存放訪問數據庫的操作接口的實現類
    數據訪問層
  
(4. tw.ethan.cnblogs.xdp_gacl.mvc.service 
    存放處理系統業務接口類 
    業務處理層
  
(5. tw.ethan.cnblogs.xdp_gacl.mvc.service.impl 
    存放處理系統業務接口的實現類
    業務處理層
  
(6. tw.ethan.cnblogs.xdp_gacl.mvc.web.controller 
    存放作為系統控制器的Servlet 
    Web層(表現層)
  
(7. tw.ethan.cnblogs.xdp_gacl.mvc.web.UI 
    存放為用戶提供用戶界面的servlet(UI指的是user interface)
    Web層(表現層)
  
(8. tw.ethan.cnblogs.xdp_gacl.mvc.web.filter 
    存放系統的用到的過濾器(Filter)
    Web層(表現層)
  
(9. tw.ethan.cnblogs.xdp_gacl.mvc.web.listener 
    存放系統的用到的監聽器(Listener)
    Web層(表現層)
  
(10. tw.ethan.cnblogs.xdp_gacl.mvc.util 
    存放系統的通用工具類，提供給【數據訪問層】、【業務處理層】、【Web層】來使用
  
(11. junit.test 
    存放系統的測試類
----------------------------------------------------------------------------------------------------------------
    搭建JavaWeb項目架構時，就按照上面的1~11的序號順序創建包：
domain→dao→dao.impl→service→service.impl→web.controller→web.UI→web.filter→web.listener→util →junit.test，
包的層次創建好了，項目的架構也就定下來了，當然，在實際的項目開發中，也不一定是
完完全全按照上面說的來創建包的層次結構，而是根據項目的實際情況，可能還需要創建
其他的包，這個得根據項目的需要來定了
----------------------------------------------------------------------------------------------------------------
開發總結
　　通過這個小例子，可以了解到mvc分層架構的項目搭建，在平時的項目開發中，也都是按照如下的順序來進行開發的：
  
　　1、搭建開發環境
　　　　1.1 創建web項目
　　　　1.2 導入項目所需的開發包
　　　　1.3 創建程序的包名，在java中是以包來體現項目的分層架構的

　　2、開發domain：把一張要操作的表當成一個VO類(VO類只定義屬性以及屬性對應的
get和set方法，沒有涉及到具體業務的操作方法)，VO表示的是值對象，通俗地說，就是把
表中的每一條記錄當成一個對象，表中的每一個字段就作為這個對象的屬性。每往表中插
入一條記錄，就相當於是把一個VO類的實例對象插入到數據表中，對數據表進行操作時，
都是直接把一個VO類的對象寫入到表中，一個VO類對象就是一條記錄。每一個VO對象可
以表示一張表中的一行記錄，VO類的名稱要和表的名稱一致或者對應。

　　3、開發dao
　　　　3.1 DAO操作接口：每一個DAO操作接口規定了，一張表在一個項目中的具體操作方法，此接口的名稱最好按照如下格式編寫：“I表名稱Dao”。

　　　　　　├DAO接口裡面的所有方法按照以下的命名編寫：
　　　　　　　　├更新數據庫：doXxx()
　　　　　　　　├查詢數據庫：findXxx()或getXxx()

　　　　3.2 DAO操作接口的實現類：實現類中完成具體的增刪改查操作
　　　　　　├此實現類完成的只是數據庫中最核心的操作，並沒有專門處理數據庫的打開和關閉，因為這些操作與具體的業務操作無關。

　　4、開發service(service 對web層提供所有的業務服務)

　　5、開發web層
--%>
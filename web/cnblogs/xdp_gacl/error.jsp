<%-- 
    Document   : error
    Created on : Jun 22, 2018, 11:23:12 AM
    Author     : Ethan
--%>



<%@page  language="java" import="java.util.*" pageEncoding="UTF-8" isErrorPage="true"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>錯誤信息提示頁面</title>
    </head>
    <body>
        <p style="text-align: center;">
            對不起，出錯了，請聯繫管理員解決！ <br />
            異常信息如下：<%=exception.getMessage()%>
        </p>
    </body>
</html>

<%--
    將error.jsp頁面顯式聲明為錯誤處理頁面後，有什麼好處呢，好處就是Jsp引擎在將jsp頁
面翻譯成Servlet的時候，在Servlet的_jspService方法中會聲明一個exception對象，然後將運
行jsp出錯的異常信息存儲到exception對象中。

    由於Servlet的_jspService方法中聲明了exception對象，那麼就可以在error.jsp頁面中使用
exception對象，這樣就可以在Jsp頁面中拿到出錯的異常信息了。

    如果沒有設置isErrorPage="true"，那麼在jsp頁面中是無法使用exception對象的，因為在
Servlet的_jspService方法中不會聲明一個exception對象。
--%>
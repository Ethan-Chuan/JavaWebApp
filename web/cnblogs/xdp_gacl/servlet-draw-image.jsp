<%-- 
    Document   : servlet-draw-image
    Created on : Jun 11, 2018, 6:19:16 PM
    Author     : Ethan
--%>

<%@page contentType="text/html" language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>在 Form 表單中使用驗證碼</title>
        <script type="text/javascript">
            //刷新驗證碼
            function changeImg(){
                document.getElementById("validateCodeImg").src="${pageContext.request.contextPath}/servlet-draw-image?" + Math.random();
            }
        </script>
    </head>
    <body>
        <h1>Form Validate Code</h1>
        <form action="${pageContext.request.contextPath}/servlet-validate-code-check" method="post">
            驗證碼：<input type="text" name="validateCode" />
            <img alt="驗證碼取得失敗" src="${pageContext.request.contextPath}/servlet-draw-image" id="validateCodeImg" onclick="changeImg()">
            <a href="javascript:void(0)" onclick="changeImg()">看不清，換一張</a>
            <br/>
            <input type="submit" value="提交">
        </form>
    </body>
</html>

<%-- 
    Document   : jsp-tag-include2
    Created on : Jun 22, 2018, 6:05:07 PM
    Author     : Ethan
--%>

<%@page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>@include v.s. jsp:include</title>
    </head>
    <%!
        int i=10;
    %>
    <body>
        <h1>jsp-tag-include2.jsp中 i 的值为：<%=i%></h1>
        
     <%-- 
        使用 <jsp:include page="relativeUrlPath"
        jsp:include 屬於動態包含，動態包含就是指先將各個頁面分別處理，
        處理完之後再將處理後的結果包含進來。兩個變數 i 不會相互影響。
        --%>
        <h1><jsp:include page="jspfragments/variable-i.jsp" /></h1>
        
     <%--
        使用 <%@include file="relativeUrlPath"
        此時在編譯jsp時就已經提示出錯了
        這個錯誤說的是變量 i 已經重複定義了
        variable i is already defined in class SimplifiedJSPServlet
     
        運行後發現出現了重複定義變量i的錯誤提示信息，因為靜態包含是將全部內容包含
        進來之後，再進行處理，屬於先包含後處理。由於被包含進來的頁面variable-i.jsp中
        定義了一個變量i，而包含頁面jsp-tag-include2.jsp本身又定義了一個變量i，所以服務
        器在處理jsp-tag-include2.jsp這個頁面時就會發現裡面有兩個重複定義的變量i，因此
        就會報錯。
        --%>
        <%--<h1><%@include file="jspfragments/variable-i.jsp"%></h1>--%>
        
    </body>
</html>

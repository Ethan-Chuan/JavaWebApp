<%-- 
    Document   : jsp-four-scopes3
    Created on : Jun 22, 2018, 4:12:01 PM
    Author     : Ethan
--%>

<%@page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>a href page</title>
    </head>
    
     <%-- page scope --%>
    <%
        // 取得設置的屬性
        // 強制向下轉型成String類型
        String pageName = (String)pageContext.getAttribute("pageName");
        // 強制向下轉型成Date類型
        Date pageDate = (Date)pageContext.getAttribute("pageDate");
    %>
    
    <%-- request scope --%>
    <%
        // 取得設置的屬性
        // 強制向下轉型成String類型
        String requestName = (String)request.getAttribute("requestName");
        // 強制向下轉型成Date類型
        Date requestDate = (Date)request.getAttribute("requestDate");
    %>
    
    <%-- session scope --%>
    <%
        // 取得設置的屬性
        // 強制向下轉型成String類型
        String sessionName = (String)session.getAttribute("sessionName");
        // 強制向下轉型成Date類型
        Date sessionDate = (Date)session.getAttribute("sessionDate");
    %>
    
    <%-- application scope --%>
    <%
        // 取得設置的屬性
        // 強制向下轉型成String類型
        String applicationName = (String)application.getAttribute("applicationName");
        // 強制向下轉型成Date類型
        Date applicationDate = (Date)application.getAttribute("applicationDate");
    %>
    
    <body>
        <h1>a href page</h1>
        <h2>pageContext.getAttribute</h2>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            姓名：<%=pageName%><br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            日期：<%=pageDate%><br />
        </p>
        <hr />
        <h2>request.getAttribute</h2>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            姓名：<%=requestName%><br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            日期：<%=requestDate%><br />
            (使用客戶端跳轉後，url改變，資料無法取得！)<br />
        </p>
        <hr />
        <h2>session.getAttribute</h2>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            姓名：<%=sessionName%><br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            日期：<%=sessionDate%><br />
            (如果使用「無痕模式」或「不同的」瀏覽器訪問此頁面，將會取不到資料！)<br />
        </p>
        <hr />
        <h2>application.getAttribute</h2>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            姓名：<%=applicationName%><br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            日期：<%=applicationDate%><br />
            (只有在「伺服器」關閉或重啟，此參數的資料將會消失！)
        </p>
    </body>
</html>

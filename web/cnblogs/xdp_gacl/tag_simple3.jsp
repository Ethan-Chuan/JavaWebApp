<%-- 
    Document   : tag_simple3
    Created on : 2018/7/12, 上午 10:47:20
    Author     : Ethan
--%>

<%@page language="java" pageEncoding="UTF-8"%>

<!-- 在jsp頁面中導入自定義標籤庫 -->
<%@taglib  uri="/WEB-INF/tlds/tag_simple.tld" prefix="tst" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>用簡單標籤修改jsp頁面內容輸出</title>
    </head>
    <body>
        <%-- 在jsp頁面中使用自定義標籤 --%>
        <tst:simple3>
            hello, simple tag!
        </tst:simple3>
    </body>
</html>

<%-- 
    Document   : jsp-javabean3
    Created on : 2018/6/24, 下午 02:48:24
    Author     : Ethan
--%>

<%@page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<%--
在jsp中使用jsp:useBean標籤來實例化一個Java類的對象
<jsp:useBean id="persion" class="tw.ethan.cnblogs.xdp_gacl.javabean.Person" scope="page" />
    ┝<jsp:useBean>:表示在JSP中要使用JavaBean
    ┝id:表示生成的實例化對象，凡是在標籤中看見了id，則肯定表示一個實例對象
    ┝class:此對象對應的包.類名稱
    ┝scope:此javaBean的保存範圍，四種範圍：page, request, session, application
--%>
<jsp:useBean id="person" class="tw.ethan.cnblogs.xdp_gacl.javabean.Person" scope="page" />

<%--
    jsp:setProperty標籤可以使用請求參數為bean的屬性賦值
    param="param_name"用於接收參數名為param_name的參數值，然後將接收到的值賦給name屬性
--%>
<jsp:setProperty property="name" name="person" param="param_name" />

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>jsp:setProperty標籤使用範例</title>
    </head>
    <body>
        <%-- 使用getXxx()方法獲取對象的屬性值 --%>
        <h2>姓名：<%=person.getName()%></h2>
    </body>
</html>

<%--
使用請求參數為bean的屬性賦值
http://localhost:8080/JavaWebApp/cnblogs/xdp_gacl/jsp-javabean3.jsp?param_name=%E7%99%BD%E8%99%8E%E7%A5%9E%E7%9A%87
--%>
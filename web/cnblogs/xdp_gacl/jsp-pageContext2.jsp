<%-- 
    Document   : jsp-pageContext2
    Created on : Jun 22, 2018, 1:48:44 PM
    Author     : Ethan
--%>

<%@page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>pageContext訪問其它域</title>
    </head>
    
    <%
        // 此時相當於往session對象中存放了一個name屬性，等價於 session.setAttribute("name", "白虎神皇");
        pageContext.setAttribute("name", "白虎神皇", PageContext.SESSION_SCOPE);
    %>
    <%
        // 取得session對象的屬性，使用pageContext對象獲取
        String refName1 = (String) pageContext.getAttribute("name", PageContext.SESSION_SCOPE);
        // 由於取得的值為Object類型，因此必須使用String強制向下轉型，轉換成String
        String refName2 = (String) session.getAttribute("name");
    %>
    
    <body>
        <h1>取出存放在session對象中的屬性值：</h1>
        <p>第一種做法：使用pageContext.getAttribute("attributeName", PageContext.SESSION_SCOPE);去取出session對象中值</p>
        <h3>姓名：<%=refName1%></h3>
        <p>第二種做法：使用session.getAttribute("attributeName");去取出session對象中值</p>
        <h3>姓名：<%=refName2%></h3>
    </body>
</html>

<%--
代表各個域的常量：
    1 PageContext.APPLICATION_SCOPE
    2 PageContext.SESSION_SCOPE
    3 PageContext.REQUEST_SCOPE
    4 PageContext.PAGE_SCOPE
----------------------------------------------------------------------------------------------------------------

--%>
<%-- 
    Document   : jsp-pageContext3
    Created on : Jun 22, 2018, 2:04:40 PM
    Author     : Ethan
--%>

<%@page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>使用pageContext的forward方法跳轉頁面</title>
    </head>
    
    <%
        // 使用RequestDispatcher的forward方法實現的跳轉方式
        //pageContext.getRequest().getRequestDispatcher("/cnblogs/xdp_gacl/jsp-pageContext4.jsp").forward(request, response);
        
        // 使用pageContext.forward(relativeUrlPath)替代RequestDispather.forward(relativeUrlPath)
        // 使用pageContext的forward方法跳轉到jsp-pageContext4.jsp頁面， / 代表了當前的web應用
        pageContext.forward("/cnblogs/xdp_gacl/jsp-pageContext4.jsp");
    %>
    
    <body>
    </body>
</html>

<%--
PageContext引入和跳轉到其他資源
　　PageContext類中定義了一個forward方法(用來跳轉頁面)簡化RequestDispatcher.forward方法。
　　方法接收的資源如果以“/”開頭， “/”代表當前web應用。

這種寫法是用來簡化和替代
pageContext.getRequest().getRequestDispatcher("/cnblogs/xdp_gacl/jsp-pageContext4.jsp").forward(request, response);
這種寫法的。在實際開發中，使用pageContext.forward(relativeUrlPath)方法跳轉頁面用得不多，
主要是因為要在Jsp頁面中嵌套java代碼，所以這種做法簡單了解一下即可。

在開發中，要想從一個Jsp頁面採用服務器端跳轉的方式跳轉到另一個Jsp頁面，那麼一般
會使用<jsp:forward>標籤，<jsp:forward>標籤用於把請求轉發給另外一個資源。
--%>
<%-- 
    Document   : jsp-javabean1
    Created on : 2018/6/24, 下午 01:34:01
    Author     : Ethan
--%>

<%@page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<%--
在jsp中使用jsp:useBean標籤來實例化一個Java類的對象
<jsp:useBean id="persion" class="tw.ethan.cnblogs.xdp_gacl.javabean.Person" scope="page" />
    ┝<jsp:useBean>:表示在JSP中要使用JavaBean
    ┝id:表示生成的實例化對象，凡是在標籤中看見了id，則肯定表示一個實例對象
    ┝class:此對象對應的包.類名稱
    ┝scope:此javaBean的保存範圍，四種範圍：page, request, session, application
--%>
<jsp:useBean id="person" class="tw.ethan.cnblogs.xdp_gacl.javabean.Person" scope="page" />
<%
    // person對象在上面已經使用jsp:useBean標籤實例化了，因此在這裡可以直接使用person對象
    // 使用setXxx方法為對象的屬性賦值
    // 為person對象的name屬性賦值
    person.setName("白虎神皇");
    // 為person對象的sex屬性賦值
    person.setSex("男");
    // 為person對象的Age屬性賦值
    person.setAge(24);
    // 為person對象的married屬性賦值
    person.setMarried(false);
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>jsp:useBean標籤使用範例</title>
    </head>
    <body>
        <%-- 使用getXxx()方法獲取對象的屬性值 --%>
        <h2>姓名：<%=person.getName()%></h2>
        <h2>性別：<%=person.getSex()%></h2>
        <h2>年齡：<%=person.getAge()%></h2>
        <h2>已婚：<%=person.isMarried()%></h2>
    </body>
</html>

<%--
JSP技術提供了三個關於JavaBean組件的動作元素，即JSP標籤，它們分別為：
    <jsp:useBean>標籤：用於在JSP頁面中查找或實例化一個JavaBean組件。
    <jsp:setProperty>標籤：用於在JSP頁面中設置一個JavaBean組件的屬性。
    <jsp:getProperty>標籤：用於在JSP頁面中獲取一個JavaBean組件的屬性。
----------------------------------------------------------------------------------------------------------------
<jsp:useBean>標籤用於在指定的域範圍內查找指定名稱的JavaBean對象，如果存在則直接
返回該JavaBean對象的引用，如果不存在則實例化一個新的JavaBean對象並將它以指定的
名稱存儲到指定的域範圍中。
　　常用語法：
    <jsp:useBean id="beanName" class="package.class" scope="page|request|session|application"/>
　　"id"屬性用於指定JavaBean實例對象的引用名稱和其存儲在域範圍中的名稱。
　　"class"屬性用於指定JavaBean的完整類名（即必須帶有包名）。
　　"scope"屬性用於指定JavaBean實例對象所存儲的域範圍，其取值只能是page、request、session和application等四個值中的一個，其默認值是page。
--%>
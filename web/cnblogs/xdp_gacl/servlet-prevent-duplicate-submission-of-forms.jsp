<%-- 
    Document   : servlet-prevent-duplicate-submission-of-forms
    Created on : Jun 21, 2018, 11:29:39 AM
    Author     : Ethan
--%>

<%@page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Form表單</title>
        <script type="text/javascript">
            // 解決 場景一：在網絡延遲的情況下讓用戶有時間點擊多次submit按鈕導致表單重複提交
            // 表單是否已經提交標識，默認為false
            var isCommitted = false;
            function dosubmit(){
                if(isCommitted===false){
                    // 提交表單後，將表單是否已經提交標識設置為true
                    isCommitted = true;
                    // 返回true讓表單正常提交
                    return true;
                }else{
                    // 返回false，那麼表單將不提交
                    return false;
                }
            }
        </script>
    </head>
    <body>
        <form action="<%=request.getContextPath()%>/servlet-prevent-duplicate-submission-of-forms1" onsubmit="return dosubmit()" method="post">
            <%-- 使用隱藏域存儲生成的token，使用EL表達式取出存儲在session中的token --%>
            <input type="hidden" name="token" value="${token}" />
            用戶名：<input type="text" name="username" />
            <input type="submit" value="提交" />
        </form>
    </body>
</html>

<%-- 
    Document   : jsp-four-scopes
    Created on : Jun 22, 2018, 2:59:42 PM
    Author     : Ethan
--%>

<%@page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Current Page</title>
    </head>
    
    <%-- page scope --%>
    <%
        // page屬性只能夠在本頁中取得
        pageContext.setAttribute("pageName", "page白虎神皇");
        pageContext.setAttribute("pageDate", new Date());
        // 注意：這裡設置的兩個屬性名字為字符串類型數據，但對應的屬性值是 Object 類型數據
    %>
    <%
        // 取得設置的屬性
        // 強制向下轉型成String類型
        String pageName = (String)pageContext.getAttribute("pageName");
        // 強制向下轉型成Date類型
        Date pageDate = (Date)pageContext.getAttribute("pageDate");
    %>
    
    <%-- request scope --%>
    <%
        // request屬性範圍表示在一次服務器跳轉中有效，只要是服務器跳轉，則設置的request屬性可以一直傳遞下去。
        request.setAttribute("requestName", "request白虎神皇");
        request.setAttribute("requestDate", new Date());
        // 注意：這裡設置的兩個屬性名字為字符串類型數據，但對應的屬性值是 Object 類型數據
    %>
    <%
        // 取得設置的屬性
        // 強制向下轉型成String類型
        String requestName = (String)request.getAttribute("requestName");
        // 強制向下轉型成Date類型
        Date requestDate = (Date)request.getAttribute("requestDate");
    %>
    
    <%-- session scope --%>
    <%
        // session設置的屬性不管如何跳轉，都可以取得的。當然，session只針對一個用戶。
        session.setAttribute("sessionName", "session白虎神皇");
        session.setAttribute("sessionDate", new Date());
        // 注意：這裡設置的兩個屬性名字為字符串類型數據，但對應的屬性值是 Object 類型數據
    %>
    <%
        // 取得設置的屬性
        // 強制向下轉型成String類型
        String sessionName = (String)session.getAttribute("sessionName");
        // 強制向下轉型成Date類型
        Date sessionDate = (Date)session.getAttribute("sessionDate");
    %>
    
    <%-- application scope (注意：如果在服務器上設置了過多的application屬性，則會影響到服務器的性能。) --%>
    <%
        // application屬性範圍是在服務器上設置的一個屬性，所以一旦設置之後任何用戶都可以瀏覽到此屬性。
        application.setAttribute("applicationName", "application白虎神皇");
        application.setAttribute("applicationDate", new Date());
        // 注意：這裡設置的兩個屬性名字為字符串類型數據，但對應的屬性值是 Object 類型數據
    %>
    <%
        // 取得設置的屬性
        // 強制向下轉型成String類型
        String applicationName = (String)application.getAttribute("applicationName");
        // 強制向下轉型成Date類型
        Date applicationDate = (Date)application.getAttribute("applicationDate");
    %>
    
    <body>
        <h1>Current page</h1>
        <h2>pageContext.getAttribute</h2>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            姓名：<%=pageName%><br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            日期：<%=pageDate%><br />
        </p>
        <hr />
        <h2>request.getAttribute</h2>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            姓名：<%=requestName%><br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            日期：<%=requestDate%><br />
        </p>
        <hr />
        <h2>session.getAttribute</h2>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            姓名：<%=sessionName%><br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            日期：<%=sessionDate%><br />
        </p>
        <hr />
        <h2>application.getAttribute</h2>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            姓名：<%=applicationName%><br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            日期：<%=applicationDate%><br />
        </p>
        
        
        <h1>
        <%--使用jsp:forward標籤進行服務器端跳轉--%>
        <jsp:forward page="/cnblogs/xdp_gacl/jsp-four-scopes2.jsp" />
        </h1>
    </body>
</html>

<%--
JSP中提供了四種屬性範圍，四種屬性範圍分別指以下四種：
    當前頁：一個屬性只能在一個頁面中取得，跳轉到其他頁面無法取得
    一次服務器請求：一個頁面中設置的屬性，只要經過了服務器跳轉，則跳轉之後的頁面可以繼續取得。
    一次會話：一個用戶設置的內容，只要是與此用戶相關的頁面都可以訪問（一個會話表示一個人，這個人設置的東西只要這個人不走，就依然有效）
    上下文中：在整個服務器上設置的屬性，所有人都可以訪問
----------------------------------------------------------------------------------------------------------------
page屬性範圍相對好理解一些：
    在一個頁面設置的屬性，跳轉到其他頁面就無法訪問了。但是在使用page屬性範圍的時候
必須注意的是，雖然習慣上將頁面範圍的屬性稱為page範圍，但是實際上操作的時候是使用
pageContext內置對象完成的。
----------------------------------------------------------------------------------------------------------------
request屬性範圍：
    表示在一次服務器跳轉中有效，只要是服務器跳轉，則設置的request屬性可以一直傳遞下去。
----------------------------------------------------------------------------------------------------------------
session屬性範圍：
    session設置的屬性不管如何跳轉，都可以取得的。當然，session只針對一個用戶。
----------------------------------------------------------------------------------------------------------------
application屬性範圍：
    在服務器上設置一個屬性，所以一旦設置之後任何用戶都可以瀏覽到此屬性。
----------------------------------------------------------------------------------------------------------------
jsp四種屬性範圍的使用場合：
    1、request：如果客戶向服務器發請求，產生的數據，用戶看完就沒用了，像這樣的數據就存在request域,像新聞數據，屬於用戶看完就沒用的。
    2、session：如果客戶向服務器發請求，產生的數據，用戶用完了等一會兒還有用，像這樣的數據就存在session域中，像購物數據，用戶需要看到自己購物信息，並且等一會兒，還要用這個購物數據結帳。
    3、application(servletContext)：如果客戶向服務器發請求，產生的數據，用戶用完了，還要給其它用戶用，像這樣的數據就存在application(servletContext)域中，像聊天數據。
--%>
<%-- 
    Document   : tag-simple1
    Created on : 2018/7/10, 下午 12:31:51
    Author     : Ethan
--%>

<%@page language="java" pageEncoding="UTF-8"%>

<%-- 在jsp頁面中導入自定義標籤庫 --%>
<%@taglib  uri="/WEB-INF/tlds/tag_simple.tld" prefix="tst" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>用簡單標籤控制標籤體是否執行</title>
    </head>
    <body>
        <%-- 在jsp頁面中使用自定義標籤 tst標籤是帶有標籤體的，標籤體的內容是"白虎神皇"這幾個字符串 --%>
        <tst:simple1>
            白虎神皇
        </tst:simple1>
    </body>
</html>

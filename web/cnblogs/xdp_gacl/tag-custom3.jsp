<%-- 
    Document   : tag-custom3
    Created on : 2018/7/9, 下午 04:03:43
    Author     : Ethan
--%>

<%@page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<%-- 在 jsp 頁面中導入自定義標籤庫 --%>
<%@taglib  uri="/WEB-INF/tlds/tag_custom.tld" prefix="tct" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>控制jsp頁面是否執行</title>
    </head>
    <body>
        <h1>jsp頁面的內容1</h1>
        <%-- 在jsp頁面中使用自定義標籤 custom3標籤是不帶標籤體的 --%>
        <tct:custom3/>
        <h1>jsp頁面的內容2</h1>
    </body>
</html>

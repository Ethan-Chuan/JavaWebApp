<%-- 
    Document   : jsp-pageContext1
    Created on : Jun 22, 2018, 1:18:26 PM
    Author     : Ethan
--%>

<%@page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>pageContext的findAttribute方法查找屬性值</title>
    </head>
    
    <%
        pageContext.setAttribute("name1", "孤傲蒼狼");
        request.setAttribute("name2", "白虎神皇");
        session.setAttribute("name3", "玄天邪帝");
        application.setAttribute("name4", "滅世魔尊");
    %>
    <%
        //使用pageContext的findAttribute方法查找屬性，由於取得的值為Object類型，因此必須使用String強制向下轉型，轉換成String類型
        //查找name1屬性，按照順序 "page-->request-->session-->application 在這四個對象中去查找
        String refName1 = (String)pageContext.findAttribute("name1");
        String refName2 = (String)pageContext.findAttribute("name2");
        String refName3 = (String)pageContext.findAttribute("name3");
        String refName4 = (String)pageContext.findAttribute("name4");
        //查找一個不存在的屬性
        String refName5 = (String)pageContext.findAttribute("name5");
    %>
    
    <body>
        <%--當要查找某個屬性時，findAttribute方法按照查找順序"
        page→request→session→application"在這四個對象中去查找，只要找到了就返回屬性值，
        如果四個對象都沒有找到要查找的屬性，則返回一個null。--%>
        <h1>pageContext.findAttribute方法查找到的屬性值：</h1>
        <h3>pageContext對象的name1屬性：<%=refName1%></h3>
        <h3>request對象的name2屬性：<%=refName2%></h3>
        <h3>session對象的name3屬性：<%=refName3%></h3>
        <h3>application對象的name4屬性：<%=refName4%></h3>
        <h3>查找不存在的name5屬性：<%=refName5%></h3>
        <hr />
        <%--EL表達式語句在執行時，會調用pageContext.findAttribute方法，用標識符
        為關鍵字，分別從page、request、 session、application四個域中查找相應的對象，
        找到則返回相應對象，找不到則返回”” （注意，不是null，而是空字符串）。--%>
        <h1>使用EL表達式進行輸出：</h1>
        <h3>pageContext對象的name1屬性：${name1}</h3>
        <h3>request對象的name2屬性：${name2}</h3>
        <h3>session對象的name3屬性：${name3}</h3>
        <h3>application對象的name4屬性：${name4}</h3>
        <h3>查找不存在的name5屬性：${name5}</h3>
    </body>
</html>


<%--
pageContext對像是JSP技術中最重要的一個對象，它代表JSP頁面的運行環境，這個對象
不僅封裝了對其它8大隱式對象的引用，它自身還是一個域對象(容器)，可以用來保存數據。
並且，這個對象還封裝了web開發中經常涉及到的一些常用操作，例如引入和跳轉其它資源、
檢索其它域對象中的屬性等。
----------------------------------------------------------------------------------------------------------------
通過pageContext獲得其他對象：
    getException方法返回 exception 隱式對象
    getPage方法返回 page 隱式對象
    getRequest方法返回 request 隱式對象
    getResponse方法返回 response 隱式對象
    getServletConfig方法返回 config 隱式對象
    getServletContext方法返回 application 隱式對象
    getSession方法返回 session 隱式對象
    getOut方法返回 out 隱式對象
----------------------------------------------------------------------------------------------------------------
pageContext封裝其它8大內置對象的意義
    如果在編程過程中，把pageContext對象傳遞給一個普通java對象，那麼這個java對象將可
以獲取8大隱式對象，此時這個java對象就可以和瀏覽器交互了，此時這個java對象就成為了
一個動態web資源了，這就是pageContext封裝其它8大內置對象的意義。

    把pageContext傳遞給誰，誰就能成為一個動態web資源，那麼什麼情況下需要把
pageContext傳遞給另外一個java類呢？什麼情況下需要使用這種技術呢？

    在比較正規的開發中，jsp頁面是不允許出現java代碼的，如果jsp頁面出現了java代碼，那麼
就應該想辦法把java代碼移除掉，我們可以開發一個自定義標籤來移除jsp頁面上的java代
碼，首先圍繞自定義標籤寫一個java類，jsp引擎在執行自定義標籤的時候就會調用圍繞自定
義標籤寫的那個java類，在調用java類的時候就會把pageContext對象傳遞給這個java類，由於
pageContext對象封裝了對其8大隱式對象的引用，因此在這個java類中就可以使用jsp頁面中的
8大隱式對象(request，response，config，application，exception，Session，page，out)了，
pageContext對象在jsp自定義標籤開發中特別重要。
--%>
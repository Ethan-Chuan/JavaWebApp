<%-- 
    Document   : el-demo4
    Created on : 2018/7/17, 下午 05:58:11
    Author     : Ethan
--%>

<%@page language="java" pageEncoding="UTF-8"%>
<%@taglib  uri="/WEB-INF/tlds/elFunction.tld" prefix="efnt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>使用EL調用Java方法</title>
    </head>
    <body>
        <%-- 使用EL調用filter方法 --%>
        ${efnt:filter("<a href=''>點點</a>")}
    </body>
</html>

<%--
一般來說， EL自定義函數開發與應用包括以下三個步驟：
　　1、編寫一個Java類的靜態方法
　　2、編寫標籤庫描述符（tld）文件，在tld文件中描述自定義函數。
　　3、在JSP頁面中導入和使用自定義函數

----------------------------------------------------------------------------------------------------------------
開發EL Function注意事項
　　編寫完標籤庫描述文件後，需要將它放置到<web應用>\WEB-INF目錄中或WEB-INF目錄下的除了classes和lib目錄之外的任意子目錄中。
　　TLD文件中的<uri> 元素用指定該TLD文件的URI，在JSP文件中需要通過這個URI來引入該標籤庫描述文件。
　　<function>元素用於描述一個EL自定義函數，其中：
　　<name>子元素用於指定EL自定義函數的名稱。
　　<function-class>子元素用於指定完整的Java類名，
　　<function-signature>子元素用於指定Java類中的靜態方法的簽名，方法簽名必須指明方法的返回值類型及各個參數的類型，各個參數之間用逗號分隔。

----------------------------------------------------------------------------------------------------------------
EL注意事項
　　EL表達式是JSP 2.0規範中的一門技術 。因此，若想正確解析EL表達式，需使用支持Servlet2.4/JSP2.0技術的WEB服務器。
注意：有些Tomcat服務器如不能使用EL表達式
    （1）升級成tomcat6
    （2）在JSP中加入<%@ page isELIgnored="false" %>

----------------------------------------------------------------------------------------------------------------
EL表達式保留關鍵字
  And                      eq           gt        true
  Or                        ne            le        false
  No                        lt            ge        null
  instanceOf        empty         div        mod
  所謂保留字的意思是指變量在命名時，應該避開上述的名字，以免程序編譯時發生錯誤，關於EL表達式的內容的總結就這麼多。
--%>
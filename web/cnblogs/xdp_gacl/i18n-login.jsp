<%-- 
    Document   : i18n-login
    Created on : 2018/7/18, 下午 04:13:19
    Author     : Ethan
--%>

<%@page import="java.util.ResourceBundle"%>
<%@page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>國際化(i18n)測試</title>
    </head>
    
    <%
        // 加載i18n資源文件, request.getLocale()獲取訪問用戶所在的國家地區
        ResourceBundle myResourcesBundle = ResourceBundle.getBundle("tw.ethan.cnblogs.xdp_gacl.i18n.resource.myproperties", request.getLocale());
    %>
    
    <body>
        <%=request.getLocale()%><br /><br />
        <form action="" method="post">
            <%=myResourcesBundle.getString("username")%>:<input type="text" name="username" /><br />
            <%=myResourcesBundle.getString("password")%>:<input type="password" name="password" /><br />
            <input type="submit" value="<%=myResourcesBundle.getString("submit")%>" />
        </form>
    </body>
</html>

<%--
一，國際化開發概述
軟件的國際化：軟件開發時，要使它能同時應對世界不同地區和國家的訪問，並針對不同地區和國家的訪問，提供相應的，符合來訪者閱讀習慣的頁面或數據。
國際化（國際化）又稱為i18n（讀法為i 18 n，據說是因為國際化（國際化）這個單詞從i到n之間有18個英文字母，i18n的名字由此而來）

二，合格的國際化軟件
軟件實現國際化，需具備以下兩個特徵：
如圖1所示，對於程序中固定使用的文本元素，例如菜單欄，導航條等中使用的文本元素，或錯誤提示信息，狀態信息等，需要根據來訪者的地區和國家，選擇不同語言的文本為之服務。
2，對於程序動態產生的數據，例如（日期，貨幣等），軟件應能根據當前所在的國家或地區的文化習慣進行顯示。

三，固定文本元素的國際化
對於軟件中的菜單欄，導航條，錯誤提示信息，狀態信息等這些固定不變的文本信息，可以把它們寫在一個屬性文件中，並根據不同的國家編寫不同的屬性文件。這一組屬性文件稱之為一個資源包。

3.1，創建資源包和資源文件
一個資源包中的每個資源文件都必須擁有共同的基名除了基名，每個資源文件的名稱中還必須有標識其本地信息的附加部分例如：。一個資源包的基名是“myproperties “，則與簡體中文，英文環境相對應的資源文件名則為：”myproperties_zh.properties“”myproperties_en.properties“

----------------------------------------------------------------------------------------------------------------
資源文件的內容通常採用"關鍵字＝值"的形式，軟件根據關鍵字檢索值顯示在頁面上。一個資源包中的所有資源文件的關鍵字必須相同，值則為相應國家的文字。
並且資源文件中採用的是properties格式文件，所以文件中的所有字符都必須是ASCII字碼，屬性(properties)文件是不能保存中文的，對於像中文這樣的非ACSII字符，須先進行編碼。

java提供了一個native2ascII工具用於將中文字符進行編碼處理。
--%>
<%-- 
    Document   : el-demo2
    Created on : 2018/7/17, 下午 12:52:19
    Author     : Ethan
--%>

<%@page import="tw.ethan.cnblogs.xdp_gacl.javabean.Person"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page language="java" pageEncoding="UTF-8"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>el表達式運算符</title>
    </head>
    <body>
        <h3>el表達式進行四則運算：</h3>
        加法運算：${365+24}<br />
        減法運算：${365-24}<br />
        乘法運算：${365*24}<br />
        除法運算：${365/24}<br />
        <hr />
        
        <h3>el表達式進行關係運算：</h3>
        <%-- ${user == null}和${user eq null}兩種寫法等價 --%>
        ${user1 == null}<br />
        ${user1 eq null}<br />
        <hr />
        
        <h3>el表達式使用empty運算符檢查對象是否為null(空)</h3>
        <%
            List<String> list = new ArrayList<>();
            list.add("www");
            list.add("google");
            request.setAttribute("list",list);
        %>
        <%-- 使用empty運算符檢查對象是否為null(空) --%>
        <c:if test="${!empty(list)}">
            <c:forEach var="str" items="${list}">
                ${str}<br />
            </c:forEach>
        </c:if>
        <br />
        <%
            List<String> emptyList = null;
        %>
        <%-- 使用empty運算符檢查對象是否為null(空) --%>
        <c:if test="${empty(emptyList)}">
            對不起，沒有您想看的數據
        </c:if>
        <br />
        <hr />
        
        <h3>EL表達式中使用二元表達式</h3>
        <%
            Person p1 = new Person();
            p1.setName("白虎神皇");
            session.setAttribute("user", p1);
        %>
        ${user==null ? "對不起, 您沒有登錄..." : user.name}
        <br />
        <hr />
        
        <h3>EL表達式數據回顯</h3>
        <%
            p1.setSex("male");
            // 數據回顯
            request.setAttribute("user", p1);
        %>
        <input type="radio" name="gender" value="male" ${user.sex=="male" ? "checked" : ""} />男
        <input type="radio" name="gender" value="female" ${user.sex=="female" ? "checked" : ""} />女
        <br />
    </body>
</html>

<%--
執行運算
    語法：${運算表達式}, EL表達式支持如下運算符：

    1. 關係運算符：
|       關係運算符          說明                       範例                            結果
|        == 或 eq              等於           ${5==5} 或 ${5 eq 5}               true
|         != 或 ne            不等於          ${5!=5} 或 ${5 ne 5}              false
|          < 或 lt               小於            ${3 < 5} 或 ${3 lt 5}               true
|          > 或 gt              大於            ${3 > 5} 或 ${3 gt 5}              false
|        <= 或 le            小於等於       ${3<=5} 或 ${3 le 5}               true
|        >= 或 ge           大於等於       ${3>=5} 或 ${3 ge 5}              false

----------------------------------------------------------------------------------------------------------------
    2. 邏輯運算符：
|       邏輯運算符          說明                         範例                            結果
|       && 或 and           交集            ${A&&B} 或 ${A and B}       true / false
|         ||   或  or            並集              ${A || B} 或 ${A or B}          true / false
|         !   或  not            非                    ${!A}  或 ${ not A}           true / false

----------------------------------------------------------------------------------------------------------------
    3. empty運算符：
|           檢查對象是否為null(空)

----------------------------------------------------------------------------------------------------------------
    4. 二元表達式：
|           ${user!=null ? user.name : ""}

----------------------------------------------------------------------------------------------------------------
    5. [] 和 . 號運算符
--%>
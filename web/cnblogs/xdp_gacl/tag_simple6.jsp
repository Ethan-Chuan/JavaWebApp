<%-- 
    Document   : tag_simple6
    Created on : 2018/7/12, 下午 02:11:03
    Author     : Ethan
--%>

<%@page language="java" import="java.util.Date" pageEncoding="UTF-8"%>

<%-- 在jsp頁面中導入自定義標籤庫 --%>
<%@taglib  uri="/WEB-INF/tlds/tag_simple.tld" prefix="tst" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>如果標籤接收的屬性值是一個複合數據類型，該如何給標籤的屬性賦值</title>
    </head>
    <body>
        <%--
        在jsp頁面中使用自定義標籤，標籤有一個date屬性，是一個複合數據類型
        如果標籤的屬性值是8種基本數據類型，那麼在JSP頁面在傳遞字符串時，JSP引擎會自動轉成相應的類型
        但如果標籤的屬性值是複合數據類型，那麼JSP引擎是無法自動轉換的，
        這裡將一個字符串賦值給simple6標籤的date屬性，在運行標籤時就會出現如下的錯誤：
        Unable to convert string "1988-05-07" to class "java.util.Date" for attribute "date":
        Property Editor not registered with the PropertyEditorManager
       <tst:simple6 date="1988-05-07">
        </tst:simple6>
        --%>
        <%-- 如果依定要給標籤的複合屬性賦值，那麼可以採用表達式的方法給複合屬性賦值，如下所示 --%>
        <%
            Date d = new Date();
            request.setAttribute("date", d);
        %>
        
        <tst:simple6 date="${date}" />
        <hr />
        <tst:simple6 date="<%=new Date()%>" />
    </body>
</html>

<%-- 
    Document   : jsp-tag-param1
    Created on : Jun 22, 2018, 6:31:31 PM
    Author     : Ethan
--%>

<%@page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>jsp-tag-param1</title>
    </head>
    <body>
        <h1>使用 jsp:param 標籤向被包含的頁面傳遞參數</h1>
        <hr />
        <jsp:include page="jspfragments/inc.jsp" >
            <jsp:param name="param1" value="hello" />
            <jsp:param name="param2" value="jsp-tag-param11111111" />
        </jsp:include>
    </body>
</html>

<%-- 
    Document   : tag-simple2
    Created on : 2018/7/12, 上午 10:26:23
    Author     : Ethan
--%>

<%@page language="java" pageEncoding="UTF-8"%>

<%-- 在jsp頁面中導入自定義標籤庫 --%>
<%@taglib uri="/WEB-INF/tlds/tag_simple.tld" prefix="tst" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>用簡單標籤控制標籤體執行5次</title>
    </head>
    <body>
        <%-- 在jsp頁面中使用自定義標籤 --%>
        <tst:simple2>
            白虎神皇<br />
        </tst:simple2>
    </body>
</html>

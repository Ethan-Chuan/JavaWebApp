package tw.ethan.cnblogs.xdp_gacl;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ethan
 */
@WebServlet(name = "ServletPreventDuplicateSubmissionOfForms2", urlPatterns = {"/servlet-prevent-duplicate-submission-of-forms2"})
public class ServletPreventDuplicateSubmissionOfForms2 extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // 用於生成Token(令牌)和跳轉到servlet-prevent-duplicate-submission-of-forms.jsp頁面
        
        // 創建令牌
        String token = TokenProccessor.getInstance().makeToken();
        
        System.out.println("在ServletPreventDuplicateSubmissionOfForms2中生成的token : " + token);
        
        // 在服務器中使用session保存token(令牌)
        request.getSession().setAttribute("token", token);
        
        // 跳轉到servlet-prevent-duplicate-submission-of-forms.jsp頁面
        request.getRequestDispatcher("/cnblogs/xdp_gacl/servlet-prevent-duplicate-submission-of-forms.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.doGet(request, response);
    }

}

class TokenProccessor{
    /**
     * 單例設計模式 (保證類的對象在內存中只有一個)
     * 1. 把類的構造函數私有
     * 2. 自己創建一個類的對象
     * 3. 對外提供一個公共的方法，返回類的對象
     */
    
    private TokenProccessor(){}
    
    private static final TokenProccessor instance = new TokenProccessor();
    
    /**
     * 返回類的對象
     * @return 
     */
    public static TokenProccessor getInstance(){
        return instance;
    }
    
    public String makeToken(){
        String token = (System.currentTimeMillis() + new java.util.Random().nextInt(999999999)) + "";
        
        // CheckException
        // 數據指紋 128位長 16字節 md5
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("md5");
            byte[] md5 = md.digest(token.getBytes());
            // base64編碼--任意二進制編碼明文字符 
            sun.misc.BASE64Encoder encoder = new sun.misc.BASE64Encoder();
            return encoder.encode(md5);
        } catch (java.security.NoSuchAlgorithmException ex) {
            throw new RuntimeException(ex);
        }
        
    }
}

/*
    場景一：在網絡延遲的情況下讓用戶有時間點擊多次submit按鈕導致表單重複提交
    場景二：表單提交後用戶點擊【刷新】按鈕導致表單重複提交
    場景三：用戶提交表單後，點擊瀏覽器的【後退】按鈕回退到表單頁面後進行再次提交
--------------------------------------------------------------------------------
    【場景一】可由javascript解決多次點擊submit按鈕的問題
--------------------------------------------------------------------------------
    對於【場景二】和【場景三】導致表單重複提交的問題，既然客戶端無法解決，那麼就在服
務器端解決，在服務器端解決就需要用到session了。

    具體的做法：在服務器端生成一個唯一的隨機標識號，專業術語稱為Token(令牌)，同時在
當前用戶的Session域中保存這個Token。然後將Token發送到客戶端的Form表單中，在Form表單
中使用隱藏域來存儲這個Token，表單提交的時候連同這個Token一起提交到服務器端，然後在服
務器端判斷客戶端提交上來的Token與服務器端生成的Token是否一致，如果不一致，那就是重複
提交了，此時服務器端就可以不處理重複提交的表單。如果相同則處理表單提交，處理完後清除
當前用戶的Session域中存儲的標識號。

    在下列情況下，服務器程序將拒絕處理用戶提交的表單請求：
    存儲Session域中的Token(令牌)與表單提交的Token(令牌)不同。
    當前用戶的Session中不存在Token(令牌)。
    用戶提交的表單數據中沒有Token(令牌)。
*/
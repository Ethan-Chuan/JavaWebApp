package tw.ethan.cnblogs.xdp_gacl;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ethan
 */
@WebServlet(name = "ServletContext1", urlPatterns = {"/servlet-context1"})
public class ServletContext1 extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        String data = "Ethan's sharing data!";
        /*
            ServletConfig對像中維護了ServletContext對象的引用，開發人員在編寫servlet時，
            可以通過ServletConfig.getServletContext方法獲得ServletContext對象。
        */
        javax.servlet.ServletContext context = this.getServletConfig().getServletContext();
        context.setAttribute("data", data);
        
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServletContext1</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ServletContext1 at " + request.getContextPath() + "</h1>");
            out.println("javax.servlet.ServletContext setAttribute<br/><br/>Shareing data!");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
    WEB容器在啟動時，它會為每個WEB應用程序都創建一個對應的ServletContext對象，
它代表當前web應用。
　　ServletConfig對像中維護了ServletContext對象的引用，開發人員在編寫servlet時，
可以通過ServletConfig.getServletContext方法獲得ServletContext對象。
　　由於一個WEB應用中的所有Servlet共享同一個ServletContext對象，因此Servlet對象
之間可以通過ServletContext對象來實現通訊。 ServletContext對象通常也被稱之為
context域對象。
*/
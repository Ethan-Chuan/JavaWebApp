package tw.ethan.cnblogs.xdp_gacl;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ethan
 */
@WebServlet(name = "ServletDefault", urlPatterns = {"/servlet-default"})
public class ServletDefault extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String url = ((HttpServletRequest)request).getRequestURL().toString();
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServletDefault</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ServletDefault at " + request.getContextPath() + "</h1>");
            out.println("   This is " + this.getClass() + " , using the processRequest method.<br/><br/>");
            out.println("Request from " + url);
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
Servlet程序是由WEB服務器調用，web服務器收到客戶端的Servlet訪問請求後：
　　①Web服務器首先檢查是否已經裝載並創建了該Servlet的實例對象。如果是，則直接執行第④步，否則，執行第②步。
　　②裝載並創建該Servlet的一個實例對象。
　　③調用Servlet實例對象的init()方法。
　　④創建一個用於封裝HTTP請求消息的HttpServletRequest對象和一個代表HTTP響應消息的HttpServletResponse對象，然後調用Servlet的service()方法並將請求和響應對像作為參數傳遞進去。
　　⑤WEB應用程序被停止或重新啟動之前，Servlet引擎將卸載Servlet，並在卸載之前調用Servlet的destroy()方法。

--------------------------------------------------------------------------------
Servlet接口SUN公司定義了兩個默認實現類，分別為：GenericServlet、HttpServlet。
　　HttpServlet指能夠處理HTTP請求的servlet，它在原有Servlet接口上添加了一些與HTTP協議處理方法，它比Servlet接口的功能更為強大。因此開發人員在編寫Servlet時，通常應繼承這個類，而避免直接去實現Servlet接口。
　　HttpServlet在實現Servlet接口時，覆寫了service方法，該方法體內的代碼會自動判斷用戶的請求方式，如為GET請求，則調用HttpServlet的doGet方法，如為Post請求，則調用doPost方法。因此，開發人員在編寫Servlet時，通常只需要覆寫doGet或doPost方法，而不要去覆寫service方法。
*/
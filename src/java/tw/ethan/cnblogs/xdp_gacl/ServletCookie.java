package tw.ethan.cnblogs.xdp_gacl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ethan
 * cookie實例：獲取用戶上一次訪問的時間與中文編碼問題
 */
@WebServlet(name = "ServletCookie1", urlPatterns = {"/servlet-cookie1"})
public class ServletCookie extends HttpServlet {
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // 設置服務器端以UTF-8編碼進行輸出
        response.setCharacterEncoding("UTF-8");
        // 設置瀏覽器以UTF-8編碼進行接收，解決中文亂碼問題
        response.setContentType("text/html;charset=UTF-8");
        
        // 設置時間Cookie
        // mode: delete:-1, setMaxAge:1, otherwise:default
        timeCookie(request, response,  0);
        
        // 設置中文Cookie
        chineseCookie(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.doGet(request, response);
    }
    
    
    /**
     * 當關閉瀏覽器時， cookie 會自動失效
     * 
     * @param response 
     */
    private void setDefaultCookie(HttpServletResponse response){
        // 用戶訪問過之後重新設置用戶的訪問時間，儲存到cookie中，然後發送到客戶瀏覽器
        // 創建一個cookie，cookie的名字是lastAccessTime
        javax.servlet.http.Cookie cookie = new javax.servlet.http.Cookie("lastAccessTime", System.currentTimeMillis()+"");
        // 將cookie對象添加到response對象中，這樣服務器在輸出response對象中的內容時就會把cookie也輸出到客戶端瀏覽器
        response.addCookie(cookie);
    }
    
    /**
     * 使用 setMaxAge 設置一個有效期限
     * 當關閉瀏覽器時， cookie 依然有效
     * 
     * @param response 
     */
    private void setAgeCookie(HttpServletResponse response){
        // 用戶訪問過之後重新設置用戶的訪問時間，儲存到cookie中，然後發送到客戶瀏覽器
        // 創建一個cookie，cookie的名字是lastAccessTime
        javax.servlet.http.Cookie cookie = new javax.servlet.http.Cookie("lastAccessTime", System.currentTimeMillis()+"");
        
        // 設置Cookie的有效期為1天
        cookie.setMaxAge(24*60*60);
        
        // 將cookie對象添加到response對象中，這樣服務器在輸出response對象中的內容時就會把cookie也輸出到客戶端瀏覽器
        response.addCookie(cookie);
    }
    
    /**
     * 刪除 Cookie
     * 把有效期限設置為0，即可刪除 Cookie
     * 
     * @param response 
     */
    private void deleteCookie(HttpServletResponse response, String path){
        // 刪除cookie時，path必須一致，否則不會刪除
        javax.servlet.http.Cookie cookie = new javax.servlet.http.Cookie(path, System.currentTimeMillis()+"");
        
        // 將cookie的有效期設置為0，命令瀏覽器刪除該cookie
        cookie.setMaxAge(0);
        
        // 將cookie對象添加到response對象中，這樣服務器在輸出response對象中的內容時就會把cookie也輸出到客戶端瀏覽器
        response.addCookie(cookie);
    }
    
    /**
     * Cookie 存取時間
     * @param request
     * @param response
     * @param mode
     * @throws IOException 
     */
    private void timeCookie(HttpServletRequest request, HttpServletResponse response, int mode) 
            throws IOException{
        java.io.PrintWriter out = response.getWriter();
        
        // 獲取瀏覽器訪問服務器時傳遞過來的cookie數組
        javax.servlet.http.Cookie[] cookies = request.getCookies();
        
        // 如果用戶是第一次訪問，那麼得到的cookies將是null
        if(cookies!=null){
            out.write("您上次訪問的時間是：");
            for(int i=0;i<cookies.length;i++){
                javax.servlet.http.Cookie cookie = cookies[i];
                if(cookie.getName().equals("lastAccessTime")){
                    Long lastAccessTime = Long.parseLong(cookie.getValue());
                    java.util.Date date = new java.util.Date(lastAccessTime);
                    out.write(date.toString());
                }
            }
        }else{
            out.write("這是您第一次訪問本站！");
        }
        
        switch(mode){
            case 1:
                setAgeCookie(response);
            case -1:
                deleteCookie(response, "lastAccessTime");
            default:
                setDefaultCookie(response);
        }
        
        out.write("<br />");
    }
    
    
    /**
     * Cookie 存取中文
     * 
     * @param request
     * @param response
     * @throws UnsupportedEncodingException
     * @throws IOException 
     */
    private void chineseCookie(HttpServletRequest request, HttpServletResponse response) 
            throws UnsupportedEncodingException, IOException{
        java.io.PrintWriter out = response.getWriter();
        
        // 獲取瀏覽器訪問服務器時傳遞過來的cookie數組
        javax.servlet.http.Cookie[] cookies = request.getCookies();
        
        // 如果用戶是第一次訪問，那麼得到的cookies將是null
        if(cookies!=null){
            out.write("您上次訪問的名稱是：");
            for(int i=0;i<cookies.length;i++){
                javax.servlet.http.Cookie cookie = cookies[i];
                if(cookie.getName().equals("userName")){
                    String userName = cookie.getValue();
                    // 獲取cookie的中文數據時，使用URLDecoder類裡面的decode(String s, String enc)進行解碼
                    userName = java.net.URLDecoder.decode(userName, "UTF-8");
                    out.write(userName);
                }
            }
        }else{
            out.write("這是您第一次訪問本站！");
        }
        
        // 要想在cookie中存儲中文，那麼必須使用URLEncoder類裡面的encode(String s, String enc)方法進行中文轉碼
        javax.servlet.http.Cookie cookie = new javax.servlet.http.Cookie("userName", java.net.URLEncoder.encode("中文Cookie，需先編碼！", "UTF-8"));
        // 將cookie對象添加到response對象中，這樣服務器在輸出response對象中的內容時就會把cookie也輸出到客戶端瀏覽器
        response.addCookie(cookie);
        out.write("<br />");
    }
}

/*
會話的概念：
　　會話可簡單理解為：用戶開一個瀏覽器，點擊多個超鏈接，訪問服務器多個web資源，然後關閉瀏覽器，整個過程稱之為一個會話。
　　有狀態會話：一個同學來過教室，下次再來教室，我們會知道這個同學曾經來過，這稱之為有狀態會話。

會話過程中要解決的一些問題？
　　每個用戶在使用瀏覽器與服務器進行會話的過程中，不可避免各自會產生一些數據，程序要想辦法為每個用戶保存這些數據。

保存會話數據的兩種技術：
    1、Cookie
        Cookie是客戶端技術，程序把每個用戶的數據以cookie的形式寫給用戶各自的瀏覽器。當用戶使用瀏覽器再去訪問服務器中的web資源時，就會帶著各自的數據去。這樣，web資源處理的就是用戶各自的數據了。
    2、Session
        Session是服務器端技術，利用這個技術，服務器在運行時可以為每一個用戶的瀏覽器創建一個其獨享的session對象，由於session為用戶瀏覽器獨享，所以用戶在訪問服務器的web資源時，可以把各自的數據放在各自的session中，當用戶再去訪問服務器中的其它web資源時，其它web資源再從用戶各自的session中取出數據為用戶服務。

Cookie注意細節：
    一個Cookie只能標識一種信息，它至少含有一個標識該信息的名稱（NAME）和設置值（VALUE）。
    一個WEB站點可以給一個WEB瀏覽器發送多個Cookie，一個WEB瀏覽器也可以存儲多個WEB站點提供的Cookie。
    瀏覽器一般只允許存放300個Cookie，每個站點最多存放20個Cookie，每個Cookie的大小限制為4KB。

    如果創建了一個cookie，並將他發送到瀏覽器，默認情況下它是一個會話級別的cookie（即存儲在瀏覽器的內存中），用戶退出瀏覽器之後即被刪除。
    若希望瀏覽器將該cookie存儲在磁盤上，則需要使用maxAge，並給出一個以秒為單位的時間。
    將最大時效設為0則是命令瀏覽器刪除該cookie。
*/
package tw.ethan.cnblogs.xdp_gacl;

/**
 *
 * @author Ethan
 */
public class ElDemo5 {
    
    // 興趣愛好
    private String[] likes;
    
    public String[] getLikes(){
        return likes;
    }
    
    public void setLikes(String[] likes){
        this.likes = likes;
    }
}

package tw.ethan.cnblogs.xdp_gacl;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ethan
 */
@WebServlet(name = "ServletThreadSafety", urlPatterns = {"/servlet-thread-safety"})
public class ServletThreadSafety extends HttpServlet {
    
    // Global variable
    int i=1;

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    /*
    // 當多線程並發訪問這個方法裡面的代碼時，會存在線程安全問題嗎?
    // i變量被多個線程並發訪問，但是沒有線程安全問題，因為i是doGet方法裡面的局部變量，
    // 當有多個線程並發訪問doGet方法時，每一個線程裡面都有自己的i變量，
    // 各個線程操作的都是自己的i變量，所以不存在線程安全問題
    // 多線程並發訪問某一個方法的時候，如果在方法內部定義了一些資源(變量，集合等)
    // 那麼每一個線程都有這些東西，所以就不存在線程安全問題了  
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int i=1;
        i++;
        response.getWriter().write(i + "");
    }
    */
    //*
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // 加了synchronized後，並發訪問i時就不存在線程安全問題了！
        // 為什麼加了synchronized後就沒有線程安全問題了呢？
        // 假如現在有一個線程訪問Servlet對象，那麼它就先拿到了Servlet對象的那把鎖
        // 等到它執行完之後才會把鎖還給Servlet對象，由於是它先拿到了Servlet對象的那把鎖，
        // 所以當有別的線程來訪問這個Servlet對象時，由於鎖已經被之前的線程拿走了，後面的線程只能排隊等候了
        
        // 如果 同時開啟兩個瀏覽器模擬並發訪問同一個Servlet，第一個瀏覽器先拿到那
        // 把鎖，而第二個瀏覽器必須排隊等待鎖頭還回來，最後結果第一個瀏覽器看到2，
        // 第二個瀏覽器看到3。
        
        synchronized(this){ //在java中，每一個對像都有一把鎖，這裡的this指的就是Servlet對象
            i++;
            try {
                Thread.sleep(1000*4);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
            response.getWriter().write(i + "");
        }
        // 這種做法雖然解決了線程安全問題，但是編寫Servlet卻萬萬不能用這種方式處理
        // 線程安全問題，假如有9999個人同時訪問這個Servlet，那麼這9999個人必須按先
        // 後順序排隊輪流訪問。
    }
    //*

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.doGet(request, response);
    }
    
}

/*
    針對Servlet的線程安全問題，Sun公司是提供有解決方案的：讓Servlet去實現一個
SingleThreadModel接口，如果某個Servlet實現了SingleThreadModel接口，那麼Servlet
引擎將以單線程模式來調用其service方法。
    查看Sevlet的API可以看到，SingleThreadModel接口中沒有定義任何方法和常量，
在Java中，把沒有定義任何方法和常量的接口稱之為標記接口，經常看到的一個最典型的標記
接口就是"Serializable "，這個接口也是沒有定義任何方法和常量的，標記接口在Java中有
什麼用呢？主要作用就是給某個對像打上一個標誌，告訴JVM，這個對象可以做什麼，比如實現
了"Serializable"接口的類的對象就可以被序列化，還有一個"Cloneable"接口，這個也是一
個標記接口，在默認情況下，Java中的對像是不允許被克隆的，就像現實生活中的人一樣，不
允許克隆，但是只要實現了"Cloneable"接口，那麼對象就可以被克隆了。

    讓Servlet實現了SingleThreadModel接口，只要在Servlet類的定義中增加實現
SingleThreadModel接口的聲明即可。
    對於實現了SingleThreadModel接口的Servlet，Servlet引擎仍然支持對該Servlet的多
線程並發訪問，其採用的方式是產生多個Servlet實例對象，並發的每個線程分別調用一個獨立
的Servlet實例對象。
    實現SingleThreadModel接口並不能真正解決Servlet的線程安全問題，因為Servlet引擎
會創建多個Servlet實例對象，而真正意義上解決多線程安全問題是指一個Servlet實例對像被
多個線程同時調用的問題。事實上，在Servlet API 2.4中，已經將SingleThreadModel標記
為Deprecated（過時的）。
*/
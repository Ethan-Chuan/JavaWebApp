package tw.ethan.cnblogs.xdp_gacl;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;

/**
 * 自定義標籤開發
 * 編寫一個實現Tag接口的Java類(標籤處理器類)
 * 資源：WEB-INF/tlds/tag_custom.tld
 * UI：cnblogs/xdp_gacl/tag-custom.jsp
 * @author Ethan
 */
public class TagCustom implements Tag{
    // 接收傳遞過來的PageContext對象
    private PageContext pageContext;

    @Override
    public void setPageContext(PageContext pc) {
        System.out.println("setPageContext(PageContext pc)");
        this.pageContext = pc;
    }

    @Override
    public void setParent(Tag t) {
        
    }

    @Override
    public Tag getParent() {
        return null;
    }

    @Override
    public int doStartTag() throws JspException {
        System.out.println("調用doStartTag()方法");
        HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
        JspWriter out = pageContext.getOut();
        String ip = request.getRemoteAddr();
        try{
            // 這裡輸出的時候會拋出IOException異常
            out.write(ip);
        }catch(IOException e){
            // 捕獲IOException異常後繼續拋出
            throw new RuntimeException(e);
        }
        return 0;
    }

    @Override
    public int doEndTag() throws JspException {
        System.out.println("調用doEndTag()方法");
        return 0;
    }

    @Override
    public void release() {
        System.out.println("調用release()方法");
    }
    
}

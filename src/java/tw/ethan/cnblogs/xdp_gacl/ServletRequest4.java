package tw.ethan.cnblogs.xdp_gacl;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ethan
 */
@WebServlet(name = "ServletRequest4", urlPatterns = {"/servlet-request4"})
public class ServletRequest4 extends HttpServlet {
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Do nothing!
        response.setHeader("content-type", "text/html;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write("Do nothing!");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // 客戶端是以UTF-8編碼傳輸數據到服務器端的，所以需要設置服務器端以UTF-8的編
        // 碼進行接收，否則對於中文數據就會產生亂碼
        request.setCharacterEncoding("UTF-8");
        
        // 獲得參數
        String userName = request.getParameter("userName");
        System.out.println("userName: " + userName);
    }
    
}

/*
    會產生亂碼，就是因為服務器和客戶端溝通的編碼不一致造成的，因此解決的辦法是：
在客戶端和服務器之間設置一個統一的編碼，之後就按照此編碼進行數據的傳輸和接收。

    由於客戶端是以UTF-8字符編碼將表單數據傳輸到服務器端的，因此服務器也需要設置
以UTF-8字符編碼進行接收，要想完成此操作，服務器可以直接使用從ServletRequest接口
繼承而來的"setCharacterEncoding(charset)"方法進行統一的編碼設置
*/
package tw.ethan.cnblogs.xdp_gacl;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ethan
 */
@WebServlet(name = "ServletConfig", urlPatterns = {"/servlet-config"})
public class ServletConfig extends HttpServlet {
    /*
        定義ServletConfig對象來接收配置的初始化參數
    */
    private javax.servlet.ServletConfig config;
    
    
    /*
        當servlet配置了初始化參數後，web容器在創建servlet實例對象時，會自動將這些
    初始化參數封裝到ServletConfig對像中，並在調用servlet的init方法時，將
    ServletConfig對像傳遞給servlet。進而，我們通過ServletConfig對象就可以得到當前
    servlet的初始化參數信息。
    */
    @Override    
    public void init(javax.servlet.ServletConfig config)
            throws ServletException {
        this.config = config;
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServletConfig</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ServletConfig at " + request.getContextPath() + "</h1>");
            
            // 獲取在web.xml中配置的初始化參數
            String paramVal = this.config.getInitParameter("name");
            out.println(paramVal + "<hr/>");
            
            // 獲取所有初始化參數
            Enumeration<String> enu = this.config.getInitParameterNames();
            while(enu.hasMoreElements()){
                String name = enu.nextElement();
                String value = this.config.getInitParameter(name);
                out.println(name + " = " + value + "<br/>");
            }
            
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

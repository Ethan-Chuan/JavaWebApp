package tw.ethan.cnblogs.xdp_gacl;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ethan
 */
@WebServlet(name = "HttpDateHeader", urlPatterns = {"/http-date-header"})
public class HttpDateHeader extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String data = "abcddefghhijkllmnoppqrsttuvwxxyz";
        
        // 設置數據合理的緩存時間值，以避免瀏覽器頻繁向服務器發送請求，提升服務器性能
        // 這裡是將數據的緩存時間設置為1天
        response.setDateHeader("expires", System.currentTimeMillis() + 24 * 3600 * 1000);
        
        response.getOutputStream().write(data.getBytes());
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.doGet(request, response);
    }

}

package tw.ethan.cnblogs.xdp_gacl.javabean;

import java.math.BigDecimal;

/**
 *
 * @author Ethan
 */
public class CalculatorBean {
    // 用戶輸入的第一個數
    private double firstNum;
    // 用戶輸入的第二個數
    private double secondNum;
    // 用戶選擇的操作運算符
    private char operator = '+';
    // 運算結果
    private double result;

    public double getFirstNum() {
        return firstNum;
    }

    public double getSecondNum() {
        return secondNum;
    }

    public char getOperator() {
        return operator;
    }

    public double getResult() {
        return result;
    }

    public void setFirstNum(double firstNum) {
        this.firstNum = firstNum;
    }

    public void setSecondNum(double secondNum) {
        this.secondNum = secondNum;
    }

    public void setOperator(char operator) {
        this.operator = operator;
    }

    public void setResult(double result) {
        this.result = result;
    }
    
    /**
     * 用於計算
     */
    public void calculate(){
        switch(this.operator){
            case '+':
                this.result = this.firstNum + this.secondNum;
                break;
            case '-':
                this.result = this.firstNum - this.secondNum;
                break;
            case '*':
                this.result = this.firstNum * this.secondNum;
                break;
            case '/':
                if(this.secondNum == 0)
                    throw new RuntimeException("被除數不能為0!!!");
                this.result = this.firstNum / this.secondNum;
                // 四捨五入
                this.result = new BigDecimal(this.result).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
                break;
            default:
                throw new RuntimeException("對不起，傳入的運算符非法!!");
        }
    }
}

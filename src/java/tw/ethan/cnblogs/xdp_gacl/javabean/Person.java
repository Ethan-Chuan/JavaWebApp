package tw.ethan.cnblogs.xdp_gacl.javabean;

import java.util.Date;

/**
 *
 * @author Ethan
 * Person類就是一個最簡單的javabean
 */
public class Person {
    // ------------------------- Person類封裝的私有屬性 -------------------------
    // 姓名 String類型
    private String name;
    // 性別 String類型
    private String sex;
    // 年齡 int類型
    private int age;
    // 是否已婚 boolean類型
    private boolean married;
    // 出生日期 Date類型
    private Date birthday;
    // -------------------------------------------------------------------------
    
    // ------------------------ Person類的無參數構造方法 ------------------------
    /**
     * 無參數構造方法
     */
    public Person(){}
    // -------------------------------------------------------------------------
    
    // -------------- Person類對外提供的用於訪問私有屬性的public方法 --------------

    public void setName(String name) {
        this.name = name;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setMarried(boolean married) {
        this.married = married;
    }
    
    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getName() {
        return name;
    }

    public String getSex() {
        return sex;
    }

    public int getAge() {
        return age;
    }

    public boolean isMarried() {
        return married;
    }
    
    public Date getBirthday() {
        return birthday;
    }
    // -------------------------------------------------------------------------
    
}

package tw.ethan.cnblogs.xdp_gacl;

import java.io.IOException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTag;
import javax.servlet.jsp.tagext.BodyTagSupport;
import javax.servlet.jsp.tagext.Tag;

/**
 * BodyTagSupport類實現了BodyTag接口， TagCustom5 繼承 BodyTagSupport 類
 * @author Ethan
 */
public class TagCustom5 extends BodyTagSupport{

    /**
     * 控制doStartTag()方法返回EVAL_BODY_BUFFERED
     * @see javax.servlet.jsp.tagext.BodyTagSupport#doStartTag() 
     * @return
     * @throws JspException 
     */
    @Override
    public int doStartTag() throws JspException {
        return BodyTag.EVAL_BODY_BUFFERED;
    }
    
    @Override
    public int doEndTag() throws JspException {
        // this.getBodyContent()得到代表標籤體的bodyContent對象
        BodyContent bodyContent = this.getBodyContent();
        
        // 拿到標籤體
        String content = bodyContent.getString();
        
        // 修改標籤體裡面的內容，將標籤體的內容轉換成大寫
        String result = content.toUpperCase();
        
        try{
            //輸出修改後的內容
            this.pageContext.getOut().write(result);
        }catch(IOException e){
            throw new RuntimeException(e);
        }
        
        return Tag.EVAL_PAGE;
    }
}

/*
            JspTag接口
               ↑
               ∣
        —  —  —  —  —
       ｜               ｜
    Tag接口        SimpleTag接口
       ↑               ↑
       ｜         SimpleTagSupport類
       ｜
       ｜
 IterationTag接口 ←  — TagSupport類
       ↑                    ↑
       ｜                    ｜
       ｜                    ｜
   BodyTag接口 ←  — BodyTagSupport類

--------------------------------------------------------------------------------
    JspTag接口是所有自定義標籤的父接口，它是JSP2.0中新定義的一個標記接口，沒有任何屬
性和方法。 JspTag接口有Tag和SimpleTag兩個直接子接口，JSP2.0以前的版本中只有Tag接口，
所以把實現Tag接口的自定義標籤也叫做傳統標籤，把實現SimpleTag接口的自定義標籤叫做簡單
標籤。

開發傳統標籤實現頁面邏輯
　　開發人員在編寫Jsp頁面時，經常還需要在頁面中引入一些邏輯，例如：

    控制jsp頁面某一部分內容是否執行。
    控制整個jsp頁面是否執行。
    控制jsp頁面內容重複執行。
    修改jsp頁面內容輸出。

　　自定義標籤除了可以移除jsp頁面java代碼外，它也可以實現以上功能。
--------------------------------------------------------------------------------
傳統標籤接口中的各個方法可以返回的返回值說明
　　下圖列舉了Tag接口、IterationTag接口和BodyTag接口中的主要方法及它們分別可以返回的返回值的說明。


                EVAL_BODY_INCLUDE   SKIP_BODY   EVAL_BODY_BUFFERED  EVAL_BODY_AGAIN     EVAL_PAGE   SKIP_PAGE

doStartTag      將標籤體的執行結      忽略標籤    將標籤體的執行結果              
                果插入到輸出流中       體部分        放入到一個               X                X          X
                                                   BodyContent
                                                    對象中

doAfterBody                          不再重複                           重複執行
                    X                執行標籤           X                標籤體               X          X
                                     體內容                               內容

doEndTag                                                                                繼續處理結   忽略結束
                                                                                        束標籤後面   標籤後面
                    X                   X              X                    X             所有的      所有的
                                                                                         JSP代碼     JSP代碼
                                        
*/
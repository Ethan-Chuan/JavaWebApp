package tw.ethan.cnblogs.xdp_gacl;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ethan
 */
@WebServlet(name = "ServletDrawImage", urlPatterns = {"/servlet-draw-image"})
public class ServletDrawImage extends HttpServlet {
    private static final long serialVersionUID = 3038623696184546092L;
    
    private static final int WIDTH = 120; // 生成圖片的寬度
    private static final int HEIGHT = 30; // 生成圖片的高度
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.doPost(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // 接收客戶端傳遞的 createTypeFlag 標誌
        String createTypeFlag = request.getParameter("createTypeFlag");
        
        //1.在內存中創建一張圖片
        java.awt.image.BufferedImage bi = new java.awt.image.BufferedImage(
                WIDTH, 
                HEIGHT, 
                java.awt.image.BufferedImage.TYPE_INT_RGB);
        
        //2.得到圖片
        Graphics g = bi.getGraphics();
        
        //3.設置圖片的背景色
        setBackGround(g);
        
        //4.設置圖片的邊框
        setBorder(g);
        
        //5.在圖片上畫干擾線
        drawRandomLine(g);
        
        //6.寫在圖片上隨機數
        //String random = drawRandomNum((Graphics2D)g, "ch"); //生成中文驗證碼圖片
        //String random = drawRandomNum((Graphics2D)g, "nl"); //生成數字和字母組合的驗證碼圖片
        //String random = drawRandomNum((Graphics2D)g, "n"); //生成純數字的驗證碼圖片
        //String random = drawRandomNum((Graphics2D)g, "l"); //生成純字母的驗證碼圖片
        String random = drawRandomNum((Graphics2D)g, createTypeFlag); //根據客戶端傳遞的createTypeFlag標誌生成驗證碼圖片
        
        //7.將隨機數存在session中
        request.getSession().setAttribute("checkcode", random);
        
        //8.設置響應頭通知瀏覽器以圖片的形式打開
        response.setContentType("image/jpeg"); //等同於response.setHeader("content-type", "image/jpeg");
        
        //9.設置響應頭控制瀏覽器不要緩存
        response.setDateHeader("expries", -1);
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Pragma", "no-cache");
        
        //10.將圖片寫給瀏覽器
        javax.imageio.ImageIO.write(bi, "jpg", response.getOutputStream());
    }

    private void setBackGround(Graphics g) {
        //設置畫筆顏色
        g.setColor(Color.WHITE);
        //填充區域
        g.fillRect(0, 0, WIDTH, HEIGHT);
    }

    private void setBorder(Graphics g) {
        //設置畫筆顏色
        g.setColor(Color.BLUE);
        //邊框區域
        g.drawRect(1, 1, WIDTH-2, HEIGHT-2);
    }

    private void drawRandomLine(Graphics g) {
        //設置畫筆顏色
        g.setColor(Color.GREEN);
        //設置線條個數並畫線
        for(int i=0;i<5;i++){
            int x1 = new java.util.Random().nextInt(WIDTH);
            int y1 = new java.util.Random().nextInt(HEIGHT);
            int x2 = new java.util.Random().nextInt(WIDTH);
            int y2 = new java.util.Random().nextInt(HEIGHT);
            g.drawLine(x1, y1, x2, y2);
        }
    }

    private String drawRandomNum(Graphics2D g, String... createTypeFlag) {
        //設置畫筆顏色
        g.setColor(Color.RED);
        //設置字體
        g.setFont(new java.awt.Font("標楷體", java.awt.Font.BOLD, 20));
        //常用的繁體字
        String baseChineseChar = 
                "\u7684\u4e00\u4e86\u662f\u6211\u4e0d\u5728\u4eba\u5011\u6709"
                + "\u4f86\u4ed6\u9019\u4e0a\u8457\u500b\u5730\u5230\u5927\u91cc"
                + "\u8aaa\u5c31\u53bb\u5b50\u5f97\u4e5f\u548c\u90a3\u8981\u4e0b"
                + "\u770b\u5929\u6642\u904e\u51fa\u5c0f\u9ebc\u8d77\u4f60\u90fd"
                + "\u628a\u597d\u9084\u591a\u6c92\u70ba\u53c8\u53ef\u5bb6\u5b78"
                + "\u53ea\u4ee5\u4e3b\u6703\u6a23\u5e74\u60f3\u751f\u540c\u8001"
                + "\u4e2d\u5341\u5f9e\u81ea\u9762\u524d\u982d\u9053\u5b83\u5f8c"
                + "\u7136\u8d70\u5f88\u50cf\u898b\u5169\u7528\u5979\u570b\u52d5"
                + "\u9032\u6210\u56de\u4ec0\u908a\u4f5c\u5c0d\u958b\u800c\u5df1"
                + "\u4e9b\u73fe\u5c71\u6c11\u5019\u7d93\u767c\u5de5\u5411\u4e8b"
                + "\u547d\u7d66\u9577\u6c34\u5e7e\u7fa9\u4e09\u8072\u65bc\u9ad8"
                + "\u624b\u77e5\u7406\u773c\u5fd7\u9ede\u5fc3\u6230\u4e8c\u554f"
                + "\u4f46\u8eab\u65b9\u5be6\u5403\u505a\u53eb\u7576\u4f4f\u807d"
                + "\u9769\u6253\u5462\u771f\u5168\u624d\u56db\u5df2\u6240\u6575"
                + "\u4e4b\u6700\u5149\u7522\u60c5\u8def\u5206\u7e3d\u689d\u767d"
                + "\u8a71\u6771\u5e2d\u6b21\u89aa\u5982\u88ab\u82b1\u53e3\u653e"
                + "\u5152\u5e38\u6c23\u4e94\u7b2c\u4f7f\u5beb\u8ecd\u5427\u6587"
                + "\u904b\u518d\u679c\u600e\u5b9a\u8a31\u5feb\u660e\u884c\u56e0"
                + "\u5225\u98db\u5916\u6a39\u7269\u6d3b\u90e8\u9580\u7121\u5f80"
                + "\u8239\u671b\u65b0\u5e36\u968a\u5148\u529b\u5b8c\u537b\u7ad9"
                + "\u4ee3\u54e1\u6a5f\u66f4\u4e5d\u60a8\u6bcf\u98a8\u7d1a\u8ddf"
                + "\u7b11\u554a\u5b69\u842c\u5c11\u76f4\u610f\u591c\u6bd4\u968e"
                + "\u9023\u8eca\u91cd\u4fbf\u9b25\u99ac\u54ea\u5316\u592a\u6307"
                + "\u8b8a\u793e\u4f3c\u58eb\u8005\u4e7e\u77f3\u6eff\u65e5\u6c7a"
                + "\u767e\u539f\u62ff\u7fa4\u7a76\u5404\u516d\u672c\u601d\u89e3"
                + "\u7acb\u6cb3\u6751\u516b\u96e3\u65e9\u8ad6\u55ce\u6839\u5171"
                + "\u8b93\u76f8\u7814\u4eca\u5176\u66f8\u5750\u63a5\u61c9\u95dc"
                + "\u4fe1\u89ba\u6b65\u53cd\u8655\u8a18\u5c07\u5343\u627e\u722d"
                + "\u9818\u6216\u5e2b\u7d50\u584a\u8dd1\u8ab0\u8349\u8d8a\u5b57"
                + "\u52a0\u8173\u7dca\u611b\u7b49\u7fd2\u9663\u6015\u6708\u9752"
                + "\u534a\u706b\u6cd5\u984c\u5efa\u8d95\u4f4d\u5531\u6d77\u4e03"
                + "\u5973\u4efb\u4ef6\u611f\u6e96\u5f35\u5718\u5c4b\u96e2\u8272"
                + "\u81c9\u7247\u79d1\u5012\u775b\u5229\u4e16\u525b\u4e14\u7531"
                + "\u9001\u5207\u661f\u5c0e\u665a\u8868\u5920\u6574\u8a8d\u97ff"
                + "\u96ea\u6d41\u672a\u5834\u8a72\u4e26\u5e95\u6df1\u523b\u5e73"
                + "\u5049\u5fd9\u63d0\u78ba\u8fd1\u4eae\u8f15\u8b1b\u8fb2\u53e4"
                + "\u9ed1\u544a\u754c\u62c9\u540d\u5440\u571f\u6e05\u967d\u7167"
                + "\u8fa6\u53f2\u6539\u6b77\u8f49\u756b\u9020\u5634\u6b64\u6cbb"
                + "\u5317\u5fc5\u670d\u96e8\u7a7f\u5167\u8b58\u9a57\u50b3\u696d"
                + "\u83dc\u722c\u7761\u8208\u5f62\u91cf\u54b1\u89c0\u82e6\u9ad4"
                + "\u773e\u901a\u885d\u5408\u7834\u53cb\u5ea6\u8853\u98ef\u516c"
                + "\u65c1\u623f\u6975\u5357\u69cd\u8b80\u6c99\u6b72\u7dda\u91ce"
                + "\u5805\u7a7a\u6536\u7b97\u81f3\u653f\u57ce\u52de\u843d\u9322"
                + "\u7279\u570d\u5f1f\u52dd\u6559\u71b1\u5c55\u5305\u6b4c\u985e"
                + "\u6f38\u5f37\u6578\u9109\u547c\u6027\u97f3\u7b54\u54e5\u969b"
                + "\u820a\u795e\u5ea7\u7ae0\u5e6b\u5566\u53d7\u7cfb\u4ee4\u8df3"
                + "\u975e\u4f55\u725b\u53d6\u5165\u5cb8\u6562\u6389\u5ffd\u7a2e"
                + "\u88dd\u9802\u6025\u6797\u505c\u606f\u53e5\u5340\u8863\u822c"
                + "\u5831\u8449\u58d3\u6162\u53d4\u80cc\u7d30";
        //數字和字母的組合
        String baseNumLetter = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        //純數字
        String baseNum = "0123456789";
        //純字母
        String baseLetter = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        //createTypeFlag[0]==null 表示沒有傳遞參數
        if((createTypeFlag.length>0) && (createTypeFlag[0]!=null)){
            switch (createTypeFlag[0]) {
                case "ch":
                    //截取漢字
                    return createRandomChar(g, baseChineseChar);
                case "nl":
                    //截取數字和字母的組合
                    return createRandomChar(g, baseNumLetter);
                case "n":
                    //截取數字
                    return createRandomChar(g, baseNum);
                case "l":
                    //截取字母
                    return createRandomChar(g, baseLetter);
                default:
                    break;
            }
        }else
            //默認截取數字和字母的組合
            return createRandomChar(g, baseNumLetter);
        
        return "";
    }

    private String createRandomChar(Graphics2D g, String baseChar) {
        StringBuffer sb = new StringBuffer();
        int x = 5;
        String ch;
        System.out.print("baseChar length:" + baseChar.length() + ", char:");
        //控制字數
        for(int i=0;i<4;i++){
            //設置字體旋轉角度
            int degree = new java.util.Random().nextInt() % 30;
            ch = baseChar.charAt(new java.util.Random().nextInt(baseChar.length())) + "";
            System.out.print(ch);
            sb.append(ch);
            //正向角度
            g.rotate(degree * Math.PI / 180, x, 20);
            g.drawString(ch, x, 20);
            //反向角度
            g.rotate(-degree * Math.PI / 180, x, 20);
            x += 30;
        }
        System.out.println();
        return sb.toString();
    }
    
    
}

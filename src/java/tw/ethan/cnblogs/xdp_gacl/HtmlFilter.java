package tw.ethan.cnblogs.xdp_gacl;

/**
 * html轉義處理工具類
 * @author Ethan
 */
public class HtmlFilter {
    
    /**
     * 靜態方法, html標籤轉義處理
     * @param message 要轉義的內容
     * @return 轉義後的內容
     */
    public static String filter(String message){
        if(message == null)
            return null;
        
        char content[] = new char[message.length()];
        message.getChars(0, message.length(), content, 0);
        StringBuffer result = new StringBuffer(content.length + 50);
        for(int i=0;i<content.length;i++){
            switch(content[i]){
                case '<':
                    result.append("&lt;");
                    break;
                case '>':
                    result.append("&gt;");
                    break;
                case '&':
                    result.append("&amp;");
                    break;
                case '"':
                    result.append("&quot;");
                    break;
                default:
                    result.append(content[i]);
            }
        }
        return result.toString();
    }
}

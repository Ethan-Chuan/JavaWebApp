package tw.ethan.cnblogs.xdp_gacl;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ethan
 */
@WebServlet(name = "ServletRequest6", urlPatterns = {"/servlet-request6"})
public class ServletRequest6 extends HttpServlet {
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String data = "大家好，我正在總結JavaWeb";
        
        // 將數據存放到request對象中，此時把request對象當作一個Map容器來使用
        request.setAttribute("data", data);
        
        // 客戶端訪問ServletRequest6這個Servlet後，ServletRequest6通知服務器將請求轉發(forward)到servlet-request6.jsp頁面進行處理
        request.getRequestDispatcher("/cnblogs/xdp_gacl/servlet-request6.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.doGet(request, response);
    }
    
}

/*
請求轉發：指一個web資源收到客戶端請求後，通知服務器去調用另外一個web資源進行處理。
　　請求轉發的應用場景：MVC設計模式

　　在Servlet中實現請求轉發的兩種方式：

　　1、通過ServletContext的getRequestDispatcher(String path)方法，該方法返回
一個RequestDispatcher對象，調用這個對象的forward方法可以實現請求轉發。
RequestDispatcher reqDispatcher =this.getServletContext().getRequestDispatcher("/cnblogs/xdp_gacl/servlet-request6.jsp");
reqDispatcher.forward(request, response);

　　2、通過request對象提供的getRequestDispatche(String path)方法，該方法返回
一個RequestDispatcher對象，調用這個對象的forward方法可以實現請求轉發。
request.getRequestDispatcher("/cnblogs/xdp_gacl/servlet-request6.jsp").forward(request, response);

--------------------------------------------------------------------------------
request對像作為一個域對象(Map容器​​)使用時，主要是通過以下的四個方法來操作
    1.setAttribute(String name,Object o)方法，將數據作為request對象的一個屬性存放到request對像中，例如：request.setAttribute("data", data);
    2.getAttribute(String name)方法，獲取request對象的name屬性的屬性值，例如：request.getAttribute("data")
    3.removeAttribute(String name)方法，移除request對象的name屬性，例如：request.removeAttribute("data")
    4.getAttributeNames方法，獲取request對象的所有屬性名，返回的是一個，例如：Enumeration<String> attrNames = request.getAttributeNames();

--------------------------------------------------------------------------------
請求重定向和請求轉發的區別
　　一個web資源收到客戶端請求後，通知「服務器」去調用另外一個web資源進行處理，稱之為請求轉發/307。
　　一個web資源收到客戶端請求後，通知「瀏覽器」去訪問另外一個web資源進行處理，稱之為請求重定向/302。
*/
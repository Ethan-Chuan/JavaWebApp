package tw.ethan.cnblogs.xdp_gacl;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ethan
 */
@WebServlet(name = "ServletSession5", urlPatterns = {"/servlet-session5"})
public class ServletSession5 extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setHeader("content-type", "text/html;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        /*
            在程序中第一次調用request.getSession()方法時就會創建一個新的Session，
            可以用isNew()方法來判斷Session是不是新創建的
        */
        
        // 使用request對象的getSession()獲取session，如果session不存在則創建一個
        javax.servlet.http.HttpSession session = request.getSession();
        // 獲取session的Id
        String sessionId = session.getId();
        // 判斷session是不是新創建的
        if(session.isNew()){
            response.getWriter().print("session創建成功，session的id是：" + sessionId);
        }else{
            response.getWriter().print("服務器已經存在session，session的id是：" + sessionId);
        }
        
        /*
            當需要在程序中手動設置Session失效時，可以手動調用
            session.invalidate方法，摧毀session。
        */
        //session.invalidate();
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.doGet(request, response);
    }

}

/*
    session對象默認30分鐘沒有使用，則服務器會自動銷毀session，
    在web.xml文件中可以手動配置session的失效時間
*/
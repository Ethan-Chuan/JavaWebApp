package tw.ethan.cnblogs.xdp_gacl;

import java.io.IOException;
import java.util.Date;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 * SimpleTagSupport類實現了SimpleTag接口
 * TagSample6類繼承SimpleTagSupport
 * 標籤的屬性說明
 * @author Ethan
 */
public class TagSimple6 extends SimpleTagSupport {
    /**
     * 定義標籤的屬性
     */
    private Date date;

    /**
     * data屬性對應的set方法
     * @param date 
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * 簡單標籤使用這個方法就可以完成所有的業務邏輯
     * 重寫doTag方法，輸出date屬性值
     * @see javax.servlet.jsp.tagext.SimpleTagSupport#doTag() 
     * @throws JspException
     * @throws IOException 
     */
    @Override
    public void doTag() throws JspException, IOException {
        this.getJspContext().getOut().write(date.toString());
    }
    
    
}
/*
一、JspFragment類介紹
　　javax.servlet.jsp.tagext.JspFragment類是在JSP2.0中定義的，它的實例對象代表JSP
頁面中的一段符合JSP語法規範的JSP片段，這段JSP片段中不能包含JSP腳本元素。
　　WEB容器在處理簡單標籤的標籤體時，會把標籤體內容用一個JspFragment對象表示，並調用
標籤處理器對象的setJspBody方法把JspFragment對像傳遞給標籤處理器對象。 JspFragment類
中只定義了兩個方法，如下所示：

　　getJspContext方法
　　　　用於返回代表調用頁面的JspContext對象.

　　public abstract void invoke(java.io.Writer out)
        用於執行JspFragment對象所代表的JSP代碼片段，參數out用於指定將JspFragment
對象的執行結果寫入到哪個輸出流對像中，如果傳遞給參數out的值為null，則將執行結果寫入
到JspContext .getOut()方法返回的輸出流對像中。 (簡而言之，可以理解為寫給瀏覽器)

invoke方法詳解
　　JspFragment.invoke方法是JspFragment最重要的方法，利用這個方法可以控制是否執行
和輸出標籤體的內容、是否迭代執行標籤體的內容或對標籤體的執行結果進行修改後再輸出。
例如：
　　(1. 在標籤處理器中如果沒有調用JspFragment.invoke方法，其結果就相當於忽略標籤體內容；
　　(2. 在標籤處理器中重複調用JspFragment.invoke方法，則標籤體內容將會被重複執行；
若想在標籤處理器中修改標籤體內容，只需在調用invoke方法時指定一個可取出結果數據的輸出
流對象（例如StringWriter），讓標籤體的執行結果輸出到該輸出流對像中，然後從該輸出流對
象中取出數據進行修改後再輸出到目標設備，即可達到修改標籤體的目的。

--------------------------------------------------------------------------------
二、開發帶屬性的標籤
自定義標籤可以定義一個或多個屬性，這樣，在JSP頁面中應用自定義標籤時就可以設置這些屬
性的值，通過這些屬性為標籤處理器傳遞參數信息，從而提高標籤的靈活性和復用性。
　　要想讓一個自定義標籤具有屬性，通常需要完成兩個任務：

    (1. 在標籤處理器中編寫每個屬性對應的setter方法
    (2. 在TLD文件中描術標籤的屬性

　　為自定義標籤定義屬性時，每個屬性都必須按照JavaBean的屬性命名方式，在標籤處理器中
定義屬性名對應的setter方法，用來接收 JSP頁面調用自定義標籤時傳遞進來的屬性值。例如屬
性url，在標籤處理器類中就要定義相應的setUrl(String url)方法。
　　在標籤處理器中定義相應的set方法後，JSP引擎在解析執行開始標籤前，也就是調用
doStartTag方法前，會調用set屬性方法，為標籤設置屬性。
*/
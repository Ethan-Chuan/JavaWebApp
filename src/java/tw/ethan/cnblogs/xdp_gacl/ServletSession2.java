package tw.ethan.cnblogs.xdp_gacl;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ethan
 */
// 首頁：列出所有書
@WebServlet(name = "ServletSession2", urlPatterns = {"/servlet-session2"})
public class ServletSession2 extends HttpServlet {
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setHeader("content-type", "text/html;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        java.io.PrintWriter out =  response.getWriter();
        
        // 創建Session
        request.getSession();
        out.write("本網站有如下的書：<br /><br />");
        java.util.Set<java.util.Map.Entry<String, Book>> set = DB.getAll().entrySet();
        for(java.util.Map.Entry<String, Book> map : set){
            Book book = map.getValue();
            String url = request.getContextPath() + "/servlet-session3?id=" + book.getId();
            
            // 用於對表單action和超連結的url地址進行重寫
            url = response.encodeURL(url);
            
            out.println(book.getName() + "    <a href=\"" + url + "\">購買</a><br />");
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.doGet(request, response);
    }
    
}

/**
 * 模擬數據庫
 */
class DB{
    private static java.util.Map<String, Book> map = new java.util.LinkedHashMap<>();
    static{
        map.put("1", new Book("1", "javaweb開發"));
        map.put("2", new Book("2", "spring開發"));
        map.put("3", new Book("3", "hibernate開發"));
        map.put("4", new Book("4", "struts開發"));
        map.put("5", new Book("5", "ajax開發"));
    }
    
    public static java.util.Map<String, Book> getAll(){
        return map;
    }
}

class Book{
    private String id;
    private String name;
    
    public Book(){
        super();
    }
    public Book(String id, String name){
        super();
        this.id = id;
        this.name = name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
    
}


/*
    當瀏覽器禁用了cookie後，就可以用URL重寫這種解決方案解決Session數據共享問題。
而且response. encodeRedirectURL(java.lang.String url) 和
response.encodeURL(java.lang.String url)是兩個非常智能的方法，
當檢測到瀏覽器沒有禁用cookie時，那麼就不進行URL重寫了。
--------------------------------------------------------------------------------
通過查看ServletSession2生成的html代碼可以看到，每一個超連結後面都帶上了session的Id
Cookie禁用：
    <body>本網站有如下的書：<br><br>javaweb開發    <a href="/JavaWebApp/servlet-session3;jsessionid=20F2E3A028ABD6EFFD739F837A1F214E?id=1">購買</a><br>
    spring開發    <a href="/JavaWebApp/servlet-session3;jsessionid=20F2E3A028ABD6EFFD739F837A1F214E?id=2">購買</a><br>
    hibernate開發    <a href="/JavaWebApp/servlet-session3;jsessionid=20F2E3A028ABD6EFFD739F837A1F214E?id=3">購買</a><br>
    struts開發    <a href="/JavaWebApp/servlet-session3;jsessionid=20F2E3A028ABD6EFFD739F837A1F214E?id=4">購買</a><br>
    ajax開發    <a href="/JavaWebApp/servlet-session3;jsessionid=20F2E3A028ABD6EFFD739F837A1F214E?id=5">購買</a><br>
    </body>

Cookie開啟：
    <body>本網站有如下的書：<br><br>javaweb開發    <a href="/JavaWebApp/servlet-session3?id=1">購買</a><br>
    spring開發    <a href="/JavaWebApp/servlet-session3?id=2">購買</a><br>
    hibernate開發    <a href="/JavaWebApp/servlet-session3?id=3">購買</a><br>
    struts開發    <a href="/JavaWebApp/servlet-session3?id=4">購買</a><br>
    ajax開發    <a href="/JavaWebApp/servlet-session3?id=5">購買</a><br>
    </body>
*/
package tw.ethan.cnblogs.xdp_gacl.mvc.domain;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Ethan
 */
public class User implements Serializable{
    private static final long serialVersionUID = -4313782718477229465L;
    
    // 用戶ID
    private String id;
    // 用戶名
    private String userName;
    // 用戶密碼
    private String userPwd;
    // 用戶郵箱
    private String email;
    // 用戶生日
    private Date birthday;

    public String getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public String getUserPwd() {
        return userPwd;
    }

    public String getEmail() {
        return email;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setUserPwd(String userPwd) {
        this.userPwd = userPwd;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }
    
}

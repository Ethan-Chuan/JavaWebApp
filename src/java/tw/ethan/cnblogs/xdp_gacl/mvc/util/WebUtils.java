package tw.ethan.cnblogs.xdp_gacl.mvc.util;

import java.util.Enumeration;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.beanutils.BeanUtils;

/**
 * 该工具类的功能就是封装客户端提交的表单数据到formbean中
 * 把request對象中的請求參數封裝到bean中
 * @author Ethan
 */
public class WebUtils {
    
    /**
     * 將request對象轉換成T對象
     * @param <T>
     * @param request
     * @param clazz
     * @return bean
     */
    public static <T> T request2Bean(HttpServletRequest request, Class<T> clazz){
        try {
            T bean = clazz.newInstance();
            Enumeration<String> enu = request.getParameterNames();
            while(enu.hasMoreElements()){
                String name = (String) enu.nextElement();
                String value = request.getParameter(name);
                if(name.equals("birthday") && (value==null || value.trim().equals(""))){
                    BeanUtils.setProperty(bean, name, null);
                }else{
                    BeanUtils.setProperty(bean, name, value);
                }
            }
            return bean;
        } catch (Exception e) {
            throw new RuntimeException(e);
        } 
    }
    
    /**
     * 生成UUID
     * @return 
     */
    public static String makeId(){
        return UUID.randomUUID().toString();
    }
}

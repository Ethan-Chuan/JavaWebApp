package tw.ethan.cnblogs.xdp_gacl.mvc.exception;

/**
 * @author Ethan
 * 自定義異常
 */
public class UserExistException extends Exception {

	public UserExistException() {
		super();
	}

	public UserExistException(String message, Throwable cause) {
		super(message, cause);
	}

	public UserExistException(String message) {
		super(message);
	}

	public UserExistException(Throwable cause) {
		super(cause);
	}
}

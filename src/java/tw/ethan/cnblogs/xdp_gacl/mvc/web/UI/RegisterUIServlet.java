package tw.ethan.cnblogs.xdp_gacl.mvc.web.UI;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 為用戶提供註冊的用戶界面的Servlet
 * RegisterUIServlet負責為用戶輸出註冊介面
 * 當用戶訪問RegisterUIServlet時，就跳轉到WEB-INF/pages目錄下的register.jsp頁面
 * @author Ethan
 */
@WebServlet(name = "RegisterUIServlet", urlPatterns = {"/register-ui"})
public class RegisterUIServlet extends HttpServlet {
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/pages/register.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.doGet(request, response);
    }
    
}

/*
    凡是位於WEB-INF目錄下的JSP頁面是無法直接通過URL地址直接訪問的
    在開發中如果項目中有一些敏感的網絡資源不想被外界直接訪問，那麼可以考慮將這些
敏感的網絡資源放到WEB-INF目錄下，這樣就可以禁止外界直接通過URL來訪問了。
*/
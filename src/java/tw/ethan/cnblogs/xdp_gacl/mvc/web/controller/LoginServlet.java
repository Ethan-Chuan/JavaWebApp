package tw.ethan.cnblogs.xdp_gacl.mvc.web.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import tw.ethan.cnblogs.xdp_gacl.mvc.domain.User;
import tw.ethan.cnblogs.xdp_gacl.mvc.service.IUserService;
import tw.ethan.cnblogs.xdp_gacl.mvc.service.impl.UserServiceImpl;

/**
 * 處理用戶登錄的servlet
 * @author Ethan
 */
@WebServlet(name = "LoginServlet", urlPatterns = {"/login-servlet"})
public class LoginServlet extends HttpServlet {
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // 獲取用戶填寫的登錄用戶名
        String username = request.getParameter("username");
        // 獲取用戶填寫的登錄密碼
        String password = request.getParameter("password");
        
        IUserService service = new UserServiceImpl();
        // 用戶登錄
        User user = service.loginUser(username, password);
        if(user==null){
            String message = String.format(
                    "對不起，用戶名或密碼有誤！！請重新登錄！2秒後為您自動跳到登錄頁面！！<meta http-equiv=\"refresh\" content=\"2;url=%s\">",
                    request.getContextPath() + "/login-ui");
            request.setAttribute("message", message);
            request.getRequestDispatcher("/cnblogs/xdp_gacl/mvc/message.jsp").forward(request, response);
            return;
        }
        
        // 登錄成功後，就將用戶存儲到session中
        request.getSession().setAttribute("user", user);
        String message = String.format("恭喜！%s, 登錄成功！ 本頁將在3秒後跳到首頁！！<meta http-equiv=\"refresh\" content=\"3;url=%s\">", 
                user.getUserName(),
                request.getContextPath() + "/cnblogs/xdp_gacl/mvc/index.jsp");
        request.setAttribute("message", message);
        request.getRequestDispatcher("/cnblogs/xdp_gacl/mvc/message.jsp").forward(request, response);
    }
    
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.doGet(request, response);
    }
    
}

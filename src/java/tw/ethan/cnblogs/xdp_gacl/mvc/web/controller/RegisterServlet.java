package tw.ethan.cnblogs.xdp_gacl.mvc.web.controller;

import java.io.IOException;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.converters.DateConverter;
import org.apache.commons.beanutils.locale.converters.DateLocaleConverter;
import tw.ethan.cnblogs.xdp_gacl.mvc.domain.User;
import tw.ethan.cnblogs.xdp_gacl.mvc.exception.UserExistException;
import tw.ethan.cnblogs.xdp_gacl.mvc.service.IUserService;
import tw.ethan.cnblogs.xdp_gacl.mvc.service.impl.UserServiceImpl;
import tw.ethan.cnblogs.xdp_gacl.mvc.util.WebUtils;
import tw.ethan.cnblogs.xdp_gacl.mvc.web.formbean.RegisterFormBean;

/**
 * 處理用戶註冊的Servlet
 * @author Ethan
 */
@WebServlet(name = "RegisterServlet", urlPatterns = {"/register-servlet"})
public class RegisterServlet extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // 將客戶端提交的表單數據封裝到RegisterFormBean對象中
        RegisterFormBean formbean = WebUtils.request2Bean(request, RegisterFormBean.class);
        // 校驗用戶註冊填寫的表單數據
        if(formbean.validate() == false){
            // 如果校驗失敗
            // 將封裝了用戶填寫的表單數據的formbean對象發送回register.jsp頁面的form表單中進行顯示
            request.setAttribute("formbean", formbean);
            // 校驗失敗就說明是用戶填寫的表單數據有問題，那麼就跳轉回register.jsp
            request.getRequestDispatcher("/WEB-INF/pages/register.jsp").forward(request, response);
            return;
        }
        
        User user = new User();
        try {
            // 註冊字符串到日期的轉換器
            ConvertUtils.register(new DateLocaleConverter(null), Date.class);
            // ConversionException: No value specified for Date的解决方法
            ConvertUtils.register(new DateConverter(null), Date.class);
            // 把表單的數據填充到javabean中
            BeanUtils.copyProperties(user, formbean);
            // 設置用戶的Id屬性
            user.setId(WebUtils.makeId());
            
            IUserService service = new UserServiceImpl();
            // 調用service層提供的註冊用戶服務實現用戶註冊
            service.registerUser(user);
            
            String message = String.format(
                    "註冊成功！！ 3秒後為您自動跳轉到登錄頁面！！<meta http-equiv=\"refresh\" content=\"3;url=%s\"/>", 
                    request.getContextPath()+"/login-ui");
            request.setAttribute("message", message);
            request.getRequestDispatcher("/cnblogs/xdp_gacl/mvc/message.jsp").forward(request, response);
        } catch (UserExistException e) {
            formbean.getErrors().put("userName", "註冊用戶已經存在！！");
            request.setAttribute("formbean", formbean);
            request.getRequestDispatcher("WEB-INF/pages/register.jsp").forward(request, response);
        } catch (Exception e) {
            // 在後台紀錄異常
            e.printStackTrace();
            request.setAttribute("message", "對不起，註冊失敗！！");
            request.getRequestDispatcher("/cnblogs/xdp_gacl/mvc/message.jsp");
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.doGet(request, response);
    }

}

/*
RegisterServlet擔任著以下幾個職責：
    (1. 接收客戶端提交到服務端的表單數據。
    (2. 校驗表單數據的合法性，如果校驗失敗跳回到register.jsp，並回顯錯誤信息。
    (3. 如果校驗通過，調用服務層向數據庫中註冊用戶。

為了方便RegisterServlet接收表單數據和校驗表單數據，在此我設計一個用於校驗註冊表單
數據RegisterFormbean，再寫WebUtils工具類，封裝客戶端提交的表單數據到的FormBean中。
--------------------------------------------------------------------------------
用戶註冊時如果填寫的表單數據校驗不通過，那麼服務器端就將一個存儲了錯誤提示消息和表單
數據的formbean的對象存儲到請求對象中，然後發送回register.jsp頁面，因此我們需要在寄
存器中。JSP頁面中取出的請求對象中的FormBean對象，然後將用戶填寫的表單數據重新回顯到
對應的表單項上面，將出錯時的提示消息也顯示到表格表單上面，讓用戶知道是哪些數據填寫不
合法！
*/
package tw.ethan.cnblogs.xdp_gacl.mvc.web.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ethan
 */
@WebServlet(name = "LogoutServlet", urlPatterns = {"/logout-servlet"})
public class LogoutServlet extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // 移除存儲在session中的user對象，實現註銷功能
        request.getSession().removeAttribute("user");
        
        /**
         * 輸出結果：
         * 註銷成功！ ！ 3秒後為您自動跳到登錄頁面！ ！
         * <meta http-equiv="refresh" content="3;url=/JavaWebApp/login-ui"/>
         */
        String message = String.format(
                "註銷成功！！ 3秒後為您自動跳到首頁！！<meta http-equiv=\"refresh\" content=\"3;url=%s\"",
                request.getContextPath() + "/cnblogs/xdp_gacl/mvc/index.jsp");
        request.setAttribute("message", message);
        request.getRequestDispatcher("/cnblogs/xdp_gacl/mvc/message.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.doGet(request, response);
    }

}

/*
    用戶登錄成功後，會將登錄的用戶信息存儲在session中，所以我們要將存儲在session中
的user刪除掉，這樣就可以實現用戶註銷了。

    用戶登錄成功後就會跳轉到index.jsp頁面，在index.jsp頁面中放一個【退出登陸】按鈕，
當點擊【退出登陸】按鈕時，就訪問LogoutServlet，將用戶註銷。
*/
package tw.ethan.cnblogs.xdp_gacl.mvc.dao;

import tw.ethan.cnblogs.xdp_gacl.mvc.domain.User;

/**
 *
 * @author Ethan
 */
public interface IUserDao {
    /**
     * 根據用戶名和密碼來查找用戶
     * @param userName
     * @param userPwd
     * @return 查詢到的用戶
     */
    User find(String userName, String userPwd);
    
    /**
     * 添加用戶
     * @param user 
     */
    void add(User user);
    
    /**
     * 根據用戶名來查找用戶
     * @param userName
     * @return 查詢到的用戶
     */
    User find(String userName);
}

/*
    對於開發接口類，我習慣以字母我作類的前綴，這樣一眼就看出當前這個類是一個接口，這
也算是一種良好的開發習慣吧，通過看類名就可以方便區分出是接口還是具體的實現類。
--------------------------------------------------------------------------------
    對於接口中的方法定義，這個只能是根據具體的業務來分析需要定義哪些方法了，但是無論
是多麼複雜的業務，都離不開基本的CRUD（增刪改查）操作，道層是直接和數據庫交互的，所以
道層的接口一般都會有增刪改查這四種操作的相關方法。
*/
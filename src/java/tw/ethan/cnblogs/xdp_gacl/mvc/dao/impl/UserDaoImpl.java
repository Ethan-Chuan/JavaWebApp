package tw.ethan.cnblogs.xdp_gacl.mvc.dao.impl;

import java.text.SimpleDateFormat;
import org.dom4j.Document;
import org.dom4j.Element;
import tw.ethan.cnblogs.xdp_gacl.mvc.dao.IUserDao;
import tw.ethan.cnblogs.xdp_gacl.mvc.domain.User;
import tw.ethan.cnblogs.xdp_gacl.mvc.util.XmlUtils;

/**
 *
 * @author Ethan
 */
public class UserDaoImpl implements IUserDao {
    private final String nodeUser = "user";
    private final String id = "id";
    private final String email = "email";
    private final String userPwd = "userPwd";
    private final String userName = "userName";
    private final String birthday = "birthday";
    private final String dateFomat = "yyyy-MM-dd";
    
    @Override
    public User find(String userName, String userPwd) {
        try {
            Document document = XmlUtils.getDocument();
            // 使用XPath表達式來操作XML節點
            Element e = (Element) document.selectSingleNode("//" + this.nodeUser + "[@" + this.userName + "=\"" + userName + "\" and @" + this.userPwd + "=\"" + userPwd + "\"]");
            if(e==null){
                return null;
            }
            User user = new User();
            user.setId(e.attributeValue(this.id));
            user.setEmail(e.attributeValue(this.email));
            user.setUserPwd(e.attributeValue(this.userPwd));
            user.setUserName(e.attributeValue(this.userName));
            String birth = e.attributeValue(this.birthday);
            if(birth==null || birth.equals("")){
                user.setBirthday(null);
            }else{
                SimpleDateFormat sdf = new SimpleDateFormat(this.dateFomat);
                user.setBirthday(sdf.parse(birth));
            }
            
            return user;   
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void add(User user) {
        try{
            Document document = XmlUtils.getDocument();
            Element root = document.getRootElement();
            // 創建user總結，並掛到root
            Element user_node = root.addElement(this.nodeUser);
            
            user_node.addAttribute(this.id, user.getId());
            user_node.addAttribute(this.userName, user.getUserName());
            user_node.addAttribute(this.userPwd, user.getUserPwd());
            user_node.addAttribute(this.email, user.getEmail());
            SimpleDateFormat sdf = new SimpleDateFormat(this.dateFomat);
            if(user.getBirthday()==null)
                user_node.addAttribute(this.birthday, "");
            else
                user_node.addAttribute(this.birthday, sdf.format(user.getBirthday()));
            
            XmlUtils.write2Xml(document);
        }catch(Exception e){
            throw new RuntimeException(e);
        }
    }

    @Override
    public User find(String userName) {
        try{
            Document document = XmlUtils.getDocument();
            Element e = (Element) document.selectSingleNode("//" + this.nodeUser + "[@" + this.userName + "=\"" + userName + "\"]");
            if(e==null){
                return null;
            }
            User user = new User();
            user.setId(e.attributeValue(this.id));
            user.setEmail(e.attributeValue(this.email));
            user.setUserPwd(e.attributeValue(this.userPwd));
            user.setUserName(e.attributeValue(this.userName));
            String birth = e.attributeValue(this.birthday);
            if(birth==null || birth.equals("")){
                user.setBirthday(null);
            }else{
                SimpleDateFormat sdf = new SimpleDateFormat(this.dateFomat);
                user.setBirthday(sdf.parse(birth));
            }
            
            return user;
        }catch(Exception e){
            throw new RuntimeException(e);
        }
    }
    
}

/*
    在UserDAOImpl類是IUserDao接口的具體實現類，對於接口的實現類命名方式，我習慣以
“接口名（去除前綴I）+ IMPL”形式或者“接口名+ IMPL”形式來命名：IUserDao（接口）
→在UserDAOImpl（實現類）或者IUserDao（接口）→IUserDaoImpl（實現類），這也算是一
些個人的編程習慣吧，平時看到的代碼大多數都是以這兩種形式中的一種來來命名接口的具體實
現類的，反正就是要能夠一眼看出接口對應的實現類是哪一個就可以了。
*/
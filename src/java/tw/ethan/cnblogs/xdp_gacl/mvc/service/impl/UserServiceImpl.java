package tw.ethan.cnblogs.xdp_gacl.mvc.service.impl;

import tw.ethan.cnblogs.xdp_gacl.mvc.dao.IUserDao;
import tw.ethan.cnblogs.xdp_gacl.mvc.dao.impl.UserDaoImpl;
import tw.ethan.cnblogs.xdp_gacl.mvc.domain.User;
import tw.ethan.cnblogs.xdp_gacl.mvc.exception.UserExistException;
import tw.ethan.cnblogs.xdp_gacl.mvc.service.IUserService;

/**
 * 開發服務層（服務層對網絡層提供所有的業務服務）
 * @author Ethan
 */
public class UserServiceImpl implements IUserService{
    
    private IUserDao userDao = new UserDaoImpl();

    @Override
    public void registerUser(User user) throws UserExistException {
        if(userDao.find(user.getUserName()) != null){
            // checked exception
            // unchecked exception
            // 這裡拋編譯時異常的原因：是我想上一層程序處理這個異常，以給用戶一個友好提示
            throw new UserExistException("註冊的用戶名已經存在！！！");
        }
        userDao.add(user);
    }

    @Override
    public User loginUser(String userName, String userPwd) {
        return userDao.find(userName, userPwd);
    }
    
}

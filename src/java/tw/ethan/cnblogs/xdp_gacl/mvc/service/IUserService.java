package tw.ethan.cnblogs.xdp_gacl.mvc.service;

import tw.ethan.cnblogs.xdp_gacl.mvc.domain.User;
import tw.ethan.cnblogs.xdp_gacl.mvc.exception.UserExistException;

/**
 * 開發服務層（服務層對網絡層提供所有的業務服務）
 * @author Ethan
 */
public interface IUserService {
    
    /**
     * 提供註冊服務
     * @param user
     * @throws UserExistException 
     */
    void registerUser(User user) throws UserExistException;
    
    /**
     * 提供登錄服務
     * @param userName
     * @param userPwd
     * @return User
     */
    User loginUser(String userName, String userPwd);
}

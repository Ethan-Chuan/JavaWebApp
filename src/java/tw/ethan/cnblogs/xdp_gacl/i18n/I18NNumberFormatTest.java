package tw.ethan.cnblogs.xdp_gacl.i18n;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

/**
 *
 * @author Ethan
 */
public class I18NNumberFormatTest {
    
    public static void main(String[] args) throws ParseException {
        int price = 89;
        
        NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.TAIWAN);
        String result = nf.format(price);
        System.out.println(result);
        
        String s = "￥89.00";
        nf = NumberFormat.getCurrencyInstance(Locale.CHINA);
        Number n = nf.parse(s);
        System.out.println(n.doubleValue() + 1);
        
        double num = 0.5;
        nf = NumberFormat.getPercentInstance();
        System.out.println(nf.format(num));
    }
    
}

/*
NumberFormat類(數字格式化)
NumberFormat類可以將一個數值格式化為符合某個國家地區習慣的數值字符串，也可以將符合某個國家地區習慣的數值字符串解析為對應的數值
　　NumberFormat類的方法：
　　　　format 方法：將一個數值格式化為符合某個國家地區習慣的數值字符串
　　　　parse 方法：將符合某個國家地區習慣的數值字符串解析為對應的數值。
　　實例化NumberFormat類時，可以使用locale對像作為參數，也可以不使用，下面列出的是使用參數的。

(1. getNumberInstance(Locale locale)：以參數locale對象所標識的本地信息來獲得具有多種用途的NumberFormat實例對象
(2. getIntegerInstance(Locale locale)：以參數locale對象所標識的本地信息來獲得處理整數的NumberFormat實例對象
(3. getCurrencyInstance(Locale locale)：以參數locale對象所標識的本地信息來獲得處理貨幣的NumberFormat實例對象
(4. getPercentInstance(Locale locale)：以參數locale對象所標識的本地信息來獲得處理百分比數值的NumberFormat實例對象
*/
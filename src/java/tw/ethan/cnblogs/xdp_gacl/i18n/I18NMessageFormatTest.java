package tw.ethan.cnblogs.xdp_gacl.i18n;

import java.text.MessageFormat;
import java.util.Date;
import java.util.Locale;

/**
 *
 * @author Ethan
 */
public class I18NMessageFormatTest {
    
    public static void main(String[] args) {
        defaultMF();
        threeFormatElement();
    }
    
    static void defaultMF(){
        // 模擬字符串
        String pattern = "On {0}, a hurricane destroyed {1} houses and caused {2} of damage.";
        // 實例化MessageFormat對象，並裝載相應的模式字符串
        MessageFormat format = new MessageFormat(pattern, Locale.TAIWAN);
        Object arr[] = {new Date(), 99, 100000000};
        // 格式化模式字符串，參數數組中指定占位符相應的替換對象
        String result = format.format(arr);
        System.out.println(result);
    }
    
    static void threeFormatElement(){
        // 模擬字符串
        String pattern = "At {0, time, short} on {0, date}, a hurricane destroyed {1} houses and caused {2, number, currency} of damage.";
        // 實例化MessageFormat對象，並裝載相應的模式字符串
        MessageFormat format = new MessageFormat(pattern, Locale.US);
        Object arr[] = {new Date(), 99, 100000000};
        // 格式化模式字符串，參數數組中指定占位符相應的替換對象
        String result = format.format(arr);
        System.out.println(result);
    }
}

/*
MessageFormat(文本格式化)
　　如果一個字符串中包含了多個與國際化相關的數據，可以使用MessageFormat類對這些數據進行批量處理。
　　例如：At 12:30 pm on jul 3,1998, a hurricance destroyed 99 houses and caused $1000000 of damage
　　以上字符串中包含了時間、數字、貨幣等多個與國際化相關的數據，對於這種字符串，可以使用MessageFormat類對其國際化相關的數據進行批量處理。
　　MessageFormat 類如何進行批量處理呢？
　　　　1.MessageFormat類允許開發人員用佔位符替換掉字符串中的敏感數據（即國際化相關的數據）。
　　　　2.MessageFormat類在格式化輸出包含佔位符的文本時，messageFormat類可以接收一個參數數組，以替換文本中的每一個佔位符。

--------------------------------------------------------------------------------
模式字符串與占位符
模式字符串：

　　At {0} on {1},a destroyed {2} houses and caused {3} of damage

　　字符串中的{0}、{1}、{2}、{3}就是佔位符(FormatElement)

--------------------------------------------------------------------------------
格式化模式字符串
　　1、實例化MessageFormat對象，並裝載相應的模式字符串。
　　2、使用format(object obj[])格式化輸出模式字符串，參數數組中指定佔位符相應的替換對象。

--------------------------------------------------------------------------------
佔位符(FormatElement)的三種書寫方式
　　{argumentIndex}: 0-9 之間的數字，表示要格式化對像數據在參數數組中的索引號
　　{argumentIndex,formatType}: 參數的格式化類型
　　{argumentIndex,formatType,FormatStyle}: 格式化的樣式，它的值必須是與格式化類型相匹配的合法模式、或表示合法模式的字符串。
*/
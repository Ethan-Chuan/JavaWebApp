package tw.ethan.cnblogs.xdp_gacl.i18n;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Locale;

/**
 *
 * @author Ethan
 */
public class I18NDateFormatTest {
    
    public static void main(String[] args) throws ParseException{
        // 當前這一刻的時間(日期、時間)
        Date date = new Date();

        // 輸出日期部分
        DateFormat df = DateFormat.getDateInstance(DateFormat.FULL, Locale.GERMAN);
        String result = df.format(date);
        System.out.println(result);
        
        // 輸出時間部分
        df = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL, Locale.TAIWAN);
        result = df.format(date);
        System.out.println(result);
        
        // 輸出日期和時間
        df = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.LONG, Locale.CHINA);
        result = df.format(date);
        System.out.println(result);
        
        // 把字串反向解析成一個date對象
        String s = "2018年7月23日 星期一 下午01時01分54秒";
        df = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.LONG, Locale.TAIWAN);
        Date d = df.parse(s);
        System.out.println(d);
    }
}
/*
Locale類
Locale實例對象代表一個特定的地理，政治，文化區域。
一個Locale對象本身不會驗證它代表的語言和國家地區信息是否正確，只是向本地敏感的類提供國家地區信息，與國際化相關的格式化和解析任務由本地敏感的類去完成。（若JDK中的某個類在運行時需要根據Locale對象來調整其功能，這個類就稱為本地敏感類

--------------------------------------------------------------------------------
日期格式類（日期格式化）
DateFormat類可以將一個日期/時間對象格式化為表示某個國家地區的日期/時間字符串。
DateFormat類除了可按國家地區格式化輸出日期外，它還定義了一些用於描述日期/時間的顯示模式的int型的常量，包括FULL，LONG，MEDIUM，DEFAULT，SHORT，實例化DateFormat對象時，可以使用這些常量，控制日期/時間的顯示長度。

--------------------------------------------------------------------------------
實例化的DateFormat類
實例化的DateFormat類有九種方式，以下三種為帶參形式，下面列出的三種方式也可以分別不帶參，或只帶顯示樣式的參數。
getDateInstance（int style，Locale aLocale）：以指定的日期顯示模式和本地信息來獲得DateFormat實例對象，該實例對像不處理時間值部分。
getTimeInstance（int style，Locale aLocale）：以指定的時間顯示模式和本地信息來獲得DateFormat實例對象，該實例對像不處理日期值部分。
getDateTimeInstance（int dateStyle，int timeStyle，Locale aLocale）：以單獨指定的日期顯示模式，時間顯示模式和本地信息來獲得DateFormat實例對象。

--------------------------------------------------------------------------------
DateFormat對象的方法
格式：將日期對象格式化為符合某個本地環境習慣的字符串。
解析：將字符串解析為日期/時間對象
注意：解析和格式完全相反，一個是把日期時間轉化為相應地區和國家的顯示樣式，一個是把相應地區的時間日期轉化成日期對象，該方法在使用時，解析的時間或日期要符合指定的國家，地區格式，否則會拋異常。
DateFormat對象通常不是線程安全的，每個線程都應該創建自己的DateFormat實例對象
*/
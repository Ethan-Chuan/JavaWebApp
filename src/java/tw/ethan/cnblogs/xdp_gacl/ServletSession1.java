package tw.ethan.cnblogs.xdp_gacl;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ethan
 */
@WebServlet(name = "ServletSession1", urlPatterns = {"/servlet-session1"})
public class ServletSession1 extends HttpServlet {
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        response.setHeader("content-type", "text/html;charset=UTF-8");
        /*
            服務器創建session出來後，會把session的id號，以cookie的形式回寫給客戶機，
            這樣，只要客戶機的瀏覽器不關，再去訪問服務器時，都會帶著session的id號去，
            服務器發現客戶機瀏覽器帶session id過來了，就會使用內存中與之對應的
            session為之服務。
        */
        
        // 使用request對象的getSession()獲取session，如果session不存在則創建一個
        javax.servlet.http.HttpSession session = request.getSession();
        
        // 將數據存儲到session中
        session.setAttribute("data", "中文session，設置屬性！");
        
        // 獲取session的Id
        String sessionId = session.getId();
        
        // 判斷session是不是新創建的
        if(session.isNew()){
            response.getWriter().print("session創建成功，session的id是：" + sessionId);
        }else{
            response.getWriter().print("服務器已經存在該session了！session的id是：" + sessionId);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.doGet(request, response);
    }

}

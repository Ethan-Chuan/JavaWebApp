package tw.ethan.cnblogs.xdp_gacl.jdbc;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;

/**
 *
 * @author Ethan
 */
public class JdbcOperaClob {

    public static void main(String[] args) {
        JdbcOperaClob obj = new JdbcOperaClob();
        obj.add();
        obj.read();
    }
    
    /**
     * @Method : add
     * @Description:向數據庫中插入大文本數據
     */
    @Test
    public void add(){
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        Reader reader = null;
        
        try {
            conn = JdbcUtils.getConnection();
            st = conn.prepareStatement("delete from testclob where id=?");
            st.setInt(1, 1);
            int rows = st.executeUpdate();
            if(rows>0){
                System.out.println("刪除成功！");
            }
            
            String sql = "insert into testclob values(1,?)";
            st = conn.prepareStatement(sql);
            //這種方式獲取的路徑，其中的空格會被使用"%20"代替，\會被"%5c"代替
            String path = JdbcOperaClob.class.getClassLoader().getResource("tw\\ethan\\cnblogs\\xdp_gacl\\jdbc\\The Little Mermaid.txt").getPath();
            //將"%20"替換回空格
            path = path.replaceAll("%20", " ");
            //將"%5c"替換回\
            path = path.replaceAll("%5c", "\\\\");
            File file = new File(path);
            reader = new FileReader(file);
            st.setCharacterStream(1, reader, (int) file.length());
            int num = st.executeUpdate();
            if(num>0){
                System.out.println("插入成功！");
            }
            //關閉流
            reader.close();
        } catch (SQLException | FileNotFoundException ex) {
            Logger.getLogger(JdbcOperaClob.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(JdbcOperaClob.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            JdbcUtils.release(conn, st, rs);
        }
    }
    
    /**
     * @Method : read
     * @Description:讀取數據庫中的大文本數據
     */
    @Test
    public void read(){
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try {
            conn = JdbcUtils.getConnection();
            String sql = "select resume from testclob where id=1";
            st = conn.prepareStatement(sql);
            rs = st.executeQuery();
            
            String contentStr = "";
            String content = "";
            if(rs.next()){
                //使用resultSet.getString("字段名")獲取大文本數據的內容
                content = rs.getString("resume");
                //使用resultSet.getCharacterStream("字段名")獲取大文本數據的內容
                Reader reader = rs.getCharacterStream("resume");
                char[] buffer = new char[1024];
                int len=0;
                File file = new File("src\\java\\tw\\ethan\\cnblogs\\xdp_gacl\\jdbc\\Read_TheLittleMermaid.txt");
                System.out.println(file.getAbsolutePath());
                FileWriter out;
                if(file.exists()){
                    out = new FileWriter(file);
                }else{
                    file.createNewFile();
                    out = new FileWriter(file, false);
                }
                
                while((len=reader.read(buffer)) > 0){
                    contentStr += new String(buffer);
                    out.write(buffer, 0, len);
                }
                out.close();
                reader.close();
            }
            
            System.out.println(content);
            System.out.println("\n\n\n\n\n----------------------------------------"
                    + "-------------------------------------------------------"
                    + "---------------------------------------------------\n\n\n\n");
            System.out.println(contentStr);
        } catch (SQLException | IOException ex) {
            Logger.getLogger(JdbcOperaClob.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            JdbcUtils.release(conn, st, rs);
        }
        
    }
}

/*
大數據也稱之為LOB(Large Objects)，LOB又分為：clob和blob，clob用於存儲大文本，blob用於存儲二進制數據，例如圖像、聲音、二進製文等。

　　在實際開發中，有時是需要用程序把大文本或二進制數據直接保存到數據庫中進行儲存的。

　　對MySQL而言只有blob，而沒有clob，mysql存儲大文本採用的是Text，Text和blob分別又分為：
　　TINYTEXT、TEXT、MEDIUMTEXT和LONGTEXT
　　TINYBLOB、BLOB、MEDIUMBLOB和LONGBLOB

--------------------------------------------------------------------------------
對於MySQL中的Text類型，可調用如下方法設置
1 PreparedStatement.setCharacterStream(index, reader, length);//注意length長度須設置，並且設置為int型

對MySQL中的Text類型，可調用如下方法獲取
1 reader = resultSet. getCharacterStream(String columnLabel);
2 string s = resultSet.getString(String columnLabel);

--------------------------------------------------------------------------------
編寫SQL測試腳本
create database jdbcstudy;
use jdbcstudy;
create table testclob
(
         id int primary key auto_increment,
         resume text
);

--------------------------------------------------------------------------------

*/
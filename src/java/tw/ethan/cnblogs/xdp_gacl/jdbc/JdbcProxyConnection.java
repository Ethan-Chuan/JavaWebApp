package tw.ethan.cnblogs.xdp_gacl.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ethan
 */
public class JdbcProxyConnection {
    
    public static void main(String[] args) {
        new JdbcProxyConnection().dbcpDataSourceTest();
    }
    
    public void dbcpDataSourceTest(){
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        
        try {
            // 獲取數據庫連接
            conn = JdbcUtilPool.getConnection();
            String sql = "select * from account";
            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while(rs.next()){
                System.out.print("id : " + rs.getInt(1));
                System.out.print(", name : " + rs.getString(2));
                System.out.println(", money : " + rs.getDouble(3));
            }
        } catch (SQLException ex) {
            Logger.getLogger(JdbcProxyConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}

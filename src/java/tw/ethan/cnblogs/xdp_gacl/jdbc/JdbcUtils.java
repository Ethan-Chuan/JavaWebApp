package tw.ethan.cnblogs.xdp_gacl.jdbc;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ethan
 */
public class JdbcUtils {
    private static String driver = null;
    private static String url = null;
    private static String username = null;
    private static String password = null;
    
    static{
        try{
            // 讀取jdbcstudy.properties文件中的數據庫連接訊息
            InputStream in = JdbcUtils.class.getClassLoader().getResourceAsStream("tw\\ethan\\cnblogs\\xdp_gacl\\jdbc\\jdbcstudy.properties");
            Properties prop = new Properties();
            prop.load(in);
            
            // 獲取數據庫連接驅動
            driver = prop.getProperty("driver");
            // 獲取數據庫連接的URL地址
            url = prop.getProperty("url");
            // 獲取數據庫連接用戶名
            username = prop.getProperty("username");
            // 獲取數據庫連接密碼
            password = prop.getProperty("password");
            
            // 加載數據庫驅動
            Class.forName(driver);
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(JdbcUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    // 獲取數據庫連接物件
    public static Connection getConnection() throws SQLException{
        return DriverManager.getConnection(url, username, password);
    }
    
    // 要釋放的資源包括
    // Connection 數據庫連接物件
    // 負責執行 SQL 命令的 Statement 物件
    // 儲存查詢結果的 ResultSet 物件
    public static void release(Connection conn, Statement st, ResultSet rs){
        if(rs!=null){
            try{
                rs.close();
            } catch (SQLException ex) {
                Logger.getLogger(JdbcUtils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if(st!=null){
            try {
                st.close();
            } catch (SQLException ex) {
                Logger.getLogger(JdbcUtils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if(conn!=null){
            try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(JdbcUtils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    // 萬能更新
    // 所有實體的 CUD 操作代碼基本相同，僅僅發送給數據庫的 SQL 語句不同而已
    // 因此可以把 CUD 操作的所有相同代碼抽取到工具類的一個 update 方法中
    // 並定義參數接收變化的 SQL 語句
    public static void update(String sql, Object params[]) throws SQLException{
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try{
            conn = getConnection();
            pstmt = conn.prepareStatement(sql);
            for(int i=0; i<params.length; i++){
                pstmt.setObject(i+1, params[i]);
            }
            pstmt.executeUpdate();
        }finally{
            release(conn, pstmt, rs);
        }
    }
    
    // 萬能查詢
    // 實體的 R 操作，除 SQL 語句不同之外，根據操作的實體不同，對 ResultSet 的映射也各不相同
    // 因此可定義一個 query 方法，除以參數形式接收變化的 SQL 語句外，可以使用「策略模式」
    // 由 query 方法的調用者決定如何把 ResultSet 中的數據映射到實體物件中
    public static Object query(String sql, Object params[], ResultSetHandler rsh) throws SQLException{
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        
        try{
            conn = getConnection();
            pstmt = conn.prepareStatement(sql);
            for(int i=0; i<params.length; i++){
                pstmt.setObject(i+1, params[i]);
            }
            rs = pstmt.executeQuery();
            // 對於查詢返回的結果集處理 使用到了「策略模式」
            // 在設計 query 方法時，query 方法事先是無法知道用戶返回的查詢結果集 如何進行處理的，即不知道結果集的處理策略
            // 那麼這個結果集的處理策略就讓用戶自己提供，query 方法內部就調用用戶提交的結果集處理策略進行處理
            // 為了能夠讓用戶提供結果集的處理策略，需要對用戶暴露出一個結果集處理介面 ResultSetHandler
            // 用戶只要實現了 ResultSetHandler 介面，那麼 query 方法內部就知道用戶要如何處理結果集了
            return rsh.handler(rs);
        }finally{
            release(conn, pstmt, rs);
        }
    }
}

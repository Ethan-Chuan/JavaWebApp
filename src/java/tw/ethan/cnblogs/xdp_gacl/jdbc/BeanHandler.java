package tw.ethan.cnblogs.xdp_gacl.jdbc;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @Description 將結果集轉換成 bean 物件的處理器
 * @author Ethan
 */
// 當框架自身提供的結果集處理器不滿足用戶的要求時
// 那麼用戶就可以自己去實現 ResultSetHandler 介面
// 編寫滿足自己業務要求的結果集處理器。
public class BeanHandler implements  ResultSetHandler{
    private Class<?> clazz;
    public BeanHandler(Class<?> clazz){
        this.clazz = clazz;
    }
    
    @Override
    public Object handler(ResultSet rs) {
        try {
            if(!rs.next()){
                return null;
            }
            Object bean = clazz.newInstance();
            // 得到結果集元數據(metadata)
            ResultSetMetaData metaData = rs.getMetaData();
            // 得到結果集中有幾列數據
            int columnCount = metaData.getColumnCount();
            for(int i=0; i<columnCount; i++){
                // 得到每個欄位的名字
                String columnName = metaData.getColumnName(i+1);
                // 返回與欄位類型關聯的 Java 類型的物件，但使用多型 Object
                Object columnObject = rs.getObject(i+1);
                // 反射出類別上欄位對應的屬性
                Field f = clazz.getDeclaredField(columnName);
                f.setAccessible(true);
                f.set(bean, columnObject);
            }
            return bean;
        } catch (SQLException | InstantiationException | IllegalAccessException | NoSuchFieldException | SecurityException ex) {
            Logger.getLogger(BeanHandler.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
}

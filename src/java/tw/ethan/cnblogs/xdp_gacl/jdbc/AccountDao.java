package tw.ethan.cnblogs.xdp_gacl.jdbc;

import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Ethan
 */
public class AccountDao {
    
    public static void main(String[] args) throws SQLException {
        AccountDao obj = new AccountDao();
        
        //obj.add(new Account("D", 1000));
        //obj.delete(4);
        //obj.update(new Account(1, "AA", 1200));
        //System.out.println(obj.find(1));
        obj.getAll().forEach(System.out::println);
    }
    
    public void add(Account account) throws SQLException{
        String sql = "INSERT INTO account(name, money) VALUES(?, ?)";
        Object[] params = {account.getName(), account.getMoney()};
        JdbcUtils.update(sql, params);
    }
    
    public void delete(int id) throws SQLException{
        String sql = "DELETE FROM account WHERE id=?";
        Object[] params = {id};
        JdbcUtils.update(sql, params);
    }
    
    public void update(Account account) throws SQLException{
        String sql = "UPDATE account SET name=?, money=? WHERE id=?";
        Object[] params = {account.getName(), account.getMoney(), account.getId()};
        JdbcUtils.update(sql, params);
    }
    
    public Account find(int id) throws SQLException{
        String sql = "SELECT * FROM account WHERE id=?";
        Object[] params = {id};
        return (Account) JdbcUtils.query(sql, params, new BeanHandler(Account.class));
    }
    
    public List<Account> getAll() throws SQLException{
        String sql = "SELECT * FROM account";
        Object[] params = {};
        return (List<Account>) JdbcUtils.query(sql, params, new BeanListHandler(Account.class));
    }

}

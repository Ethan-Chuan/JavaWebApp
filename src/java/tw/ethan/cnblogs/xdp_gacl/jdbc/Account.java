package tw.ethan.cnblogs.xdp_gacl.jdbc;

/**
 *
 * @author Ethan
 */
public class Account {
        private int id;
        private String name;
        private float money;
        
        public Account(){}
        
        public Account(String name, float money) {
            setName(name);
            setMoney(money);
        }
        
        public Account(int id, String name, float money) {
            setId(id);
            setName(name);
            setMoney(money);
        }

        private void setId(int id) {
            this.id = id;
        }
        
        public void setName(String name) {
            this.name = name;
        }

        public void setMoney(float money) {
            this.money = money;
        }

        public int getId() {
            return id;
        }
        
        public String getName() {
            return name;
        }

        public float getMoney() {
            return money;
        }

    @Override
    public String toString() {
        return "id=" + id + ", name=" + name + ", money=" + money;
    }
        
}

package tw.ethan.cnblogs.xdp_gacl.jdbc;

import java.sql.ResultSet;

/**
 *
 * @author Ethan
 */
public interface ResultSetHandler {
    
    // 結果集處理方法
    public Object handler(ResultSet rs);
    
}

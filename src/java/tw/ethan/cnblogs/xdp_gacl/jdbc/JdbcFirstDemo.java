package tw.ethan.cnblogs.xdp_gacl.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Ethan
 */
public class JdbcFirstDemo {
    
    public static void main(String[] args) {
        // 要連接的數據庫URL
        String url = "jdbc:mariadb://localhost:3306/jdbcStudy";
        // 連接的數據庫時使用的用戶名
        String username = "test";
        // 連接的數據庫時使用的密碼
        String password = "test";
        
        try {
            // 1.加載驅動
            // DriverManager.registerDriver(new com.mysql.jdbc.Driver()); 不推薦使用這種方式來加載驅動
            Class.forName("org.mariadb.jdbc.Driver"); // 推薦使用這種方式來加載驅動
        } catch (ClassNotFoundException ex) {
            System.out.println("無法加載驅動");
            System.out.println(ex);
            return;
        }
        // 2.獲取與數據庫的鏈接
        Connection conn;
        try {
            conn = DriverManager.getConnection(url, username, password);
        } catch (SQLException ex) {
            System.out.println("連接資料庫失敗:");
            System.out.println(ex);
            return;
        }
        
        // 3.獲取用於向數據庫發送sql語句的statement
        Statement st;
        try {
            st = conn.createStatement();
        } catch (SQLException ex) {
            System.out.println("取得Statement失敗");
            System.out.println(ex);
            try {
                // 6.關閉鏈接，釋放資源
                conn.close();
            } catch (SQLException ex2) {
                System.out.println("關閉資源失敗");
                System.out.println(ex2);
            }
            return;
        }
        
        String sql = "select id, name,password,email,birthday from users";
        // 4.向數據庫發送sql指令，並獲取代表結果集的ResultSet
        ResultSet rs;
        try {
            rs = st.executeQuery(sql);
        } catch (SQLException ex) {
            System.out.println("查詢失敗！");
            System.out.println(ex);
            try {
                // 6.關閉鏈接，釋放資源
                st.close();
                conn.close();
            } catch (SQLException ex2) {
                System.out.println("關閉資源失敗");
                System.out.println(ex2);
            }
            return;
        }
        
        try {
            // 5.取出結果集的數據
            while(rs.next()){
                System.out.println("id=" + rs.getObject("id"));
                System.out.println("name=" + rs.getObject("name"));
                System.out.println("password=" + rs.getObject("password"));
                System.out.println("email=" + rs.getObject("email"));
                System.out.println("birthday=" + rs.getObject("birthday"));
            }
        } catch (SQLException ex) {
            System.out.println("取出結果集失敗！");
            System.out.println(ex);
        }finally{
            try {
                // 6.關閉鏈接，釋放資源
                rs.close();
                st.close();
                conn.close();
            } catch (SQLException ex) {
                System.out.println("關閉資源失敗");
                System.out.println(ex);
            }
        }
        
    }
    
}

/*
SUN公司為了簡化、統一對數據庫的操作，定義了一套Java操作數據庫的規範（接口），
稱之為JDBC。這套接口由數據庫廠商去實現，這樣，開發人員只需要學習jdbc接口，並通
過jdbc加載具體的驅動，就可以操作數據庫。
如圖所示：
|                應用程序
|                   ↓
|                  JDBC
|                   ｜
|         ————————————
|        ↓                    ↓
|     MariaDB驅動           Oracle驅動
|        ↓                    ↓
|     MariaDB                Oracle

JDBC全稱為：Java Data Base Connectivity（java數據庫連接），它主要由接口組成。
　　組成JDBC的２個包：
　　　java.sql
　　　javax.sql
　　開發JDBC應用需要以上2個包的支持外，還需要導入相應JDBC的數據庫實現(即數據庫驅動)。

--------------------------------------------------------------------------------
Running MariaDB from the Build Directory :
(1. Go to the directory where mysqld.exe is located
(2. mysqld.exe --console

--------------------------------------------------------------------------------
【登入】
mysql -u 帳號 -p
(MariaDB預設帳號 root ，無密碼)

【建立特殊權限帳號】
grant all privileges on databasename.tablename to 'username'@'host' identified by 'password';

--------------------------------------------------------------------------------
SQL腳本如下：

create database jdbcStudy character set utf8mb4 collate utf8mb4_general_ci;

use jdbcStudy;

create table users(
    id int primary key,
    name varchar(40),
    password varchar(40),
    email varchar(60),
    birthday date
);

insert into users(id,name,password,email,birthday) values(1,'zhansan','123456','zs@sina.com','1980-12-04');
insert into users(id,name,password,email,birthday) values(2,'lisi','123456','lisi@sina.com','1981-12-04');
insert into users(id,name,password,email,birthday) values(3,'wangwu','123456','wangwu@sina.com','1979-12-04');

--------------------------------------------------------------------------------
DriverManager類講解
　　Jdbc程序中的DriverManager用於加載驅動，並創建與數據庫的鏈接，這個API的常用方法：

DriverManager.registerDriver(new Driver())
DriverManager.getConnection(url, user, password)，
　　注意：在實際開發中並不推薦採用registerDriver方法註冊驅動。原因有二：
　　　　1、查看Driver的源代碼可以看到，如果採用此種方式，會導致驅動程序註冊兩次，也就是在內存中會有兩個Driver對象。
　　　　2、程序依賴mysql的api，脫離mysql的jar包，程序將無法編譯，將來程序切換底層數據庫將會非常麻煩。

　　推薦方式：Class.forName("com.mysql.jdbc.Driver");
　　採用此種方式不會導致驅動對像在內存中重複出現，並且採用此種方式，程序僅僅只需要
一個字符串，不需要依賴具體的驅動，使程序的靈活性更高。

--------------------------------------------------------------------------------
URL用於標識數據庫的位置，通過URL地址告訴JDBC程序連接哪個數據庫，URL的寫法為：
|           jdbc : mariadb: []  //host:3306/test ? 參數名:參數值
|          協議     子協議        主機:端口  數據庫

常用數據庫URL地址的寫法：

    Oracle寫法：jdbc:oracle:thin:@localhost:1521:sid
    SqlServer寫法：jdbc:microsoft:sqlserver://localhost:1433; DatabaseName=sid
    MySql寫法：jdbc:mysql://localhost:3306/sid
    MariaDB寫法：jdbc:mariadb://localhost:3306/sid

如果連接的是本地的Mysql數據庫，並且連接使用的端口是3306，那麼的url地址可以簡寫為： jdbc:mysql:///數據庫

--------------------------------------------------------------------------------
Connection類講解
　　Jdbc程序中的Connection，它用於代表數據庫的鏈接，Collection是數據庫編程中最重要
的一個對象，客戶端與數據庫所有交互都是通過connection對象完成的，這個對象的常用方法：

createStatement()：創建向數據庫發送sql的statement對象。
prepareStatement(sql) ：創建向數據庫發送預編譯sql的PrepareSatement對象。
prepareCall(sql)：創建執行存儲過程的callableStatement對象。
setAutoCommit(boolean autoCommit)：設置事務是否自動提交。
commit() ：在鏈接上提交事務。
rollback() ：在此鏈接上回滾事務

--------------------------------------------------------------------------------
Statement類講解
　　Jdbc程序中的Statement對像用於向數據庫發送SQL語句， Statement對象常用方法：

executeQuery(String sql) ：用於向數據發送查詢語句。
executeUpdate(String sql)：用於向數據庫發送insert、update或delete語句
execute(String sql)：用於向數據庫發送任意sql語句
addBatch(String sql) ：把多條sql語句放到一個批處理中。
executeBatch()：向數據庫發送一批sql語句執行。

--------------------------------------------------------------------------------
ResultSet類講解
　　Jdbc程序中的ResultSet用於代表Sql語句的執行結果。 Resultset封裝執行結果時，採用
的類似於表格的方式。 ResultSet 對象維護了一個指向表格數據行的游標，初始的時候，游標
在第一行之前，調用ResultSet.next() 方法，可以使游標指向具體的數據行，進行調用方法
獲取該行的數據。

　　ResultSet既然用於封裝執行結果的，所以該對象提供的都是用於獲取數據的get方法：
　　獲取任意類型的數據
　　　　getObject(int index)
　　　　getObject(string columnName)
　　獲取指定類型的數據，例如：
　　　　getString(int index)
　　　　getString(String columnName)

　　ResultSet還提供了對結果集進行滾動的方法：

next()：移動到下一行
Previous()：移動到前一行
absolute(int row)：移動到指定行
beforeFirst()：移動resultSet的最前面。
afterLast() ：移動到resultSet的最後面。

--------------------------------------------------------------------------------
釋放資源
    Jdbc程序運行完後，切記要釋放程序在運行過程中，創建的那些與數據庫進行交互的對象，
這些對象通常是ResultSet, Statement和Connection對象，特別是Connection對象，它是非常
稀有的資源，用完後必須馬上釋放，如果Connection不能及時、正確的關閉，極易導致系統宕機。
Connection的使用原則是盡量晚創建，盡量早的釋放。
　　為確保資源釋放代碼能運行，資源釋放代碼也一定要放在finally語句中。
*/
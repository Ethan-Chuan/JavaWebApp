package tw.ethan.cnblogs.xdp_gacl.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;

/**
 *
 * @author Ethan
 */
public class JdbcCRUDByPreparedStatement {
    
    public static void main(String[] args){
        JdbcCRUDByPreparedStatement crud = new JdbcCRUDByPreparedStatement();
        crud.find();
        crud.update();
        crud.insert();
    }
    
    @Test
    public void insert(){
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try {
            conn = JdbcUtils.getConnection();
            st = conn.prepareStatement("delete from users where id=?");
            st.setInt(1, 4);
            int rows = st.executeUpdate();
            if(rows>0){
                System.out.println("刪除成功！");
            }
            
            
            String sql = "insert into "
                    + "users(id,name,password,email,birthday) "
                    + "values(?,?,?,?,?)";
            st = conn.prepareStatement(sql);
            //為SQL語句中的參數賦值，注意，索引是從1開始的
            /**
               * SQL語句中各個字段的類型如下：
               *  +----------+-------------+
                  | Field    | Type        |
                  +----------+-------------+
                  | id       | int(11)     |
                  | name     | varchar(40) |
                  | password | varchar(40) |
                  | email    | varchar(60) |
                  | birthday | date        |
                  +----------+-------------+
               */
            st.setInt(1, 4);
            st.setString(2, "白虎神皇");
            st.setString(3, "123");
            st.setString(4, "bhsh@sina.com");
            st.setDate(5, new java.sql.Date(new Date().getTime()));
            int num = st.executeUpdate();
            if(num>0){
                System.out.println("插入成功！");
            }
        } catch (SQLException ex) {
            Logger.getLogger(JdbcCRUDByPreparedStatement.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            JdbcUtils.release(conn, st, rs);
        }
    }
    
    @Test
    public void update(){
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try {
            conn = JdbcUtils.getConnection();
            String sql = "update users set name=?,email=? where id=?";
            st = conn.prepareStatement(sql);
            st.setString(1, "gacl");
            st.setString(2, "gacl@sina.com");
            st.setInt(3, 2);
            int num = st.executeUpdate();
            if(num>0){
                System.out.println("更新成功！");
            }
        } catch (SQLException ex) {
            Logger.getLogger(JdbcCRUDByPreparedStatement.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            JdbcUtils.release(conn, st, rs);
        }
    }
    
    @Test
    public void find(){
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try {
            conn = JdbcUtils.getConnection();
            String sql = "select * from users where id=?";
            st = conn.prepareStatement(sql);
            st.setInt(1, 4);
            rs = st.executeQuery();
            if(rs.next()){
                System.out.println(rs.getString("name"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(JdbcCRUDByPreparedStatement.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            JdbcUtils.release(conn, st, rs);
        }
    }
}

/*
1. 在MySQL的中創建一個庫，並創建用戶表和插入表的數據。
SQL腳本如下：
Create database jdbcStudy;

Use jdbcStudy;

Create a table user (
    Id int primary key,
    Name varchar(40),
    Password varchar(40),
    Email varchar(60),
    Birthday date
);
--------------------------------------------------------------------------------

2. 在src\\tw\\ethan\\cnblogs\\xdp_gacl\\jdbc目錄下創建一個jdbcstudy.properties文件
driver=org.mariadb.jdbc.Driver
url=jdbc:mariadb://localhost:3306/jdbcstudy
username=test
password=test
--------------------------------------------------------------------------------

3. Libraries -> Add library -> JUnit 4.12 - junit-4.12.jar

*/
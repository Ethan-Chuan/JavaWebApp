package tw.ethan.cnblogs.xdp_gacl.jdbc;

import java.io.InputStream;
import java.io.PrintWriter;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.LinkedList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;

/**
 *
 * @author Ethan
 */
class JdbcPool implements DataSource{

    private static LinkedList<Connection> listConnections = new LinkedList<Connection>();

    static {
        // 在靜態代碼塊中加載 db.properties 數據庫配置文件
        InputStream in = JdbcPool.class.getClassLoader().getResourceAsStream("tw\\ethan\\cnblogs\\xdp_gacl\\jdbc\\db.properties");

        Properties prop = new Properties();

        try {
            prop.load(in);
            String driver = prop.getProperty("driver");
            String url = prop.getProperty("url");
            String username = prop.getProperty("username");
            String password = prop.getProperty("password");
            // 數據庫連接池的初始化連接數大小
            int jdbcPoolInitSize = Integer.parseInt(prop.getProperty("jdbcPoolInitSize"));
            // 加載數據庫驅動
            Class.forName(driver);
            for(int i=0; i<jdbcPoolInitSize; i++){
                Connection conn = DriverManager.getConnection(url, username, password);
                System.out.println("獲取到了鏈接" + conn);
                // 將獲取到的數據庫連接加入到 listConnections 集合中, listConnections 集合此時就是一個存放了數據庫連接的連接池
                listConnections.add(conn);
            }
        } catch (Exception ex) {
            Logger.getLogger(JdbcPool.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public Connection getConnection() throws SQLException {
        // 如果數據庫連接池中的連接物件的個數大於 0
        if(listConnections.size() > 0){
            // 從 listConnection 集合中獲取一個數據庫連接
            final Connection conn = listConnections.removeFirst();
            System.out.println("listConnections 數據庫連接池大小是：" + listConnections.size());
            // 返回 Connection 物件的代理對象
            return (Connection) Proxy.newProxyInstance(JdbcPool.class.getClassLoader(), conn.getClass().getInterfaces(), new InvocationHandler() {
                @Override
                public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                    if(!method.getName().equals("close")){
                        return method.invoke(conn, args);
                    }else{
                        // 如果調用的是 Connection 物件的 close 方法，就把 conn 還給數據庫連接池
                        listConnections.add(conn);
                        System.out.println(conn + " 被還給 listConnections 數據庫連接池了！！");
                        System.out.println("listConnections 數據庫連接池大小為 " + listConnections.size());
                        return null;
                    }
                }
            });
        }else{
            throw new RuntimeException("對不起，數據庫忙碌中...");
        }
    }

    @Override
    public Connection getConnection(String username, String password) throws SQLException {
        return null;
    }

    @Override
    public PrintWriter getLogWriter() throws SQLException {
        return null;
    }

    @Override
    public void setLogWriter(PrintWriter out) throws SQLException {
        // TODO Auto-generated method stub
    }

    @Override
    public void setLoginTimeout(int seconds) throws SQLException {
        // TODO Auto-generated method stub
    }

    @Override
    public int getLoginTimeout() throws SQLException {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        return null;
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        // TODO Auto-generated method stub;
        return null;
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        // TODO Auto-generated method stub;
        return false;
    }
    
}

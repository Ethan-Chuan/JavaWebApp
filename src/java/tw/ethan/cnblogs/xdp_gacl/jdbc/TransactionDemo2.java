package tw.ethan.cnblogs.xdp_gacl.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ethan
 */
public class TransactionDemo2 {
    
    public static void main(String[] args) {
        new TransactionDemo2().testTransaction();
    }
    
    public void testTransaction(){
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Savepoint sp = null;
        
        try {
            conn = JdbcUtils.getConnection();
            conn.setAutoCommit(false); // 通知數據庫開啟事務(start transaction)
            
            String sql1 = "update account set money=money-100 where name='A'";
            pstmt = conn.prepareStatement(sql1);
            pstmt.executeUpdate();
            
            // 設置事務回滾點
            sp = conn.setSavepoint();
            
            String sql2 = "update account set money=money+100 where name='B'";
            pstmt = conn.prepareStatement(sql2);
            pstmt.executeUpdate();
            
            // 程序執行到這裡出現異常，後面的 sql3 語句執行將會中斷
            int x = 1/0;
            
            String sql3 = "update account set money=money+100 where name='C'";
            pstmt = conn.prepareStatement(sql3);
            pstmt.executeUpdate();
            
            conn.commit();
            
        } catch (Exception ex) {
            try {
                /**
                 * 我們在上面向數據庫發送了 3 條 update 語句,
                 * sql3 語句由於程序出現異常導致無法正常執行, 數據庫事務而已無法正常提交,
                 * 由於設置的事物回滾點是在 sql1 語句正常執行之後, sql2 語句正常執行之前,
                 * 那麼通知數據庫回滾事務時, 不會回滾 sql1 執行的 update 操作
                 * 只會回滾到 sql2 執行的 update 操作, 也就是說, 上面的三條 update 語句中,
                 * sql1 這條語句的修改操作起作用了, 
                 * sql2 的修改操作由於事務回滾沒有起作用
                 * sql3 由於程序異常沒有機會執行
                 */
                // 回滾到設置的事務回滾點
                conn.rollback(sp);
                // 回滾了要記得通知數據庫提交事務
                conn.commit();
            } catch (SQLException ex1) {
                Logger.getLogger(TransactionDemo2.class.getName()).log(Level.SEVERE, null, ex1);
            }
            Logger.getLogger(TransactionDemo2.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            JdbcUtils.release(conn, pstmt, rs);
        }
        
    }
}

/**
 * JDBC 中使用事務來模擬轉帳
 *  create table account(
 *      id int primary key auto_increment,
 *      name varchar(40),
 *      money float
 *  );
 *  
 *  insert into account(name, money) values('A', 1000);
 *  insert into account(name, money) values('B', 1000);
 *  insert into account(name, money) values('C', 1000);
 */

/**
事務的四大特性(ACID)
4.1、原子性（Atomicity）
　　原子性是指事務是一個不可分割的工作單位，事務中的操作要不然全部成功，要不然全部失敗。比如在同一個事務中的SQL語句，要不然全部執行成功，要不然全部執行失敗

4.2、一致性（Consistency）
　　官網上事務一致性的概念是：事務必須使數據庫從一個一致性狀態變換到另外一個一致性狀態。以轉帳為例子，A向B轉帳，假設轉帳之前這兩個用戶的錢加起來總共是2000，那麼A向B轉帳之後，不管這兩個賬戶怎麼轉，A用戶的錢和B用戶的錢加起來的總額還是2000，這個就是事務的一致性。

4.3、隔離性（Isolation）
　　事務的隔離性是多個用戶並發訪問數據庫時，數據庫為每一個用戶開啟的事務，不能被其他事務的操作數據所干擾，多個並發事務之間要相互隔離。

4.4、持久性（Durability）
　　持久性是指一個事務一旦被提交，它對數據庫中數據的改變就是永久性的，接下來即使數據庫發生故障也不應該對其有任何影響

事務的四大特性中最麻煩的是隔離性，下面重點介紹一下事務的隔離級別
問題：1.髒讀 2.不可重複讀 3.虛讀(幻讀)
隔離級別：
  1. Serializable(串行化)：可避免髒讀、不可重複讀、虛讀情況的發生
  2. Repeatable read(可重複讀)：可避免髒讀、不可重複讀情況的發生
  3. Read committed(讀已提交)：可避免髒讀情況發生
  4. Read uncommitted(讀未提交)：最低級別，以上情況均無法保證
*/
package tw.ethan.cnblogs.xdp_gacl.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;

/**
 *
 * @author Ethan
 */
public class JdbcBatchByPrepareStatement {
    
    public static void main(String[] args) {
        JdbcBatchByPrepareStatement obj = new JdbcBatchByPrepareStatement();
        //obj.dropTable();
        obj.createTable();
        obj.testJdbcBatchHandleByPrepareStatement();
    }
    
    @Test
    public void createTable(){
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        
        try {
            conn = JdbcUtils.getConnection();
            String str = "CREATE TABLE testbatch(" + 
                    "id int PRIMARY KEY," + 
                    "name varchar(20)" + 
                    ");";
            pstmt = conn.prepareStatement(str);
            pstmt.execute();
        } catch (SQLException ex) {
            Logger.getLogger(JdbcBatchByPrepareStatement.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            JdbcUtils.release(conn, pstmt, rs);
        }
    }
    
    @Test
    public void testJdbcBatchHandleByPrepareStatement(){
        long startTime = System.currentTimeMillis();
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        
        try {
            conn = JdbcUtils.getConnection();
            String str = "INSERT INTO testbatch(id, name) values(?, ?)";
            pstmt = conn.prepareStatement(str);
            for(int i=1; i<1008; i++){
                pstmt.setInt(1, i);
                pstmt.setString(2, "aa" + i);
                pstmt.addBatch();
                if(i%1000 == 0){
                    pstmt.executeBatch();
                    pstmt.clearBatch();
                }
            }
            pstmt.executeBatch();
        } catch (SQLException ex) {
            Logger.getLogger(JdbcBatchByPrepareStatement.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            JdbcUtils.release(conn, pstmt, rs);
        }
        long endtime = System.currentTimeMillis();
        System.out.println("程序花費時間：" + (endtime - startTime) / 1000 + " 秒！！");
    }
    
    public void dropTable(){
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        
        try {
            conn = JdbcUtils.getConnection();
            String str = "DROP TABLE testbatch";
            pstmt = conn.prepareStatement(str);
            pstmt.execute();
        } catch (SQLException ex) {
            Logger.getLogger(JdbcBatchByPrepareStatement.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            JdbcUtils.release(conn, pstmt, rs);
        }
    }
}

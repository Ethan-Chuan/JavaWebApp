package tw.ethan.cnblogs.xdp_gacl.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ethan
 */
public class JdbcUtilPool {
    // 數據庫連接池
    private static final JdbcPool POOL = new JdbcPool();
    // 從數據庫連接池中獲取數據庫連接物件
    public static Connection getConnection() throws SQLException{
        return POOL.getConnection();
    }
    // 釋放的資源包括 Connection 數據庫連接物件，負責執行 SQL 命令的 Statement 物件，儲存查詢結果的 ResultSet 物件
    public static void release(Connection conn, Statement stmt, ResultSet rs){
        if(rs != null){
            try {
                // 關閉存儲查詢結果的 ResultSet 物件
                rs.close();
            } catch (SQLException ex) {
                Logger.getLogger(JdbcUtilPool.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if(stmt != null){
            try {
                // 關閉負責執行 SQL 命令的 Statement 物件
                stmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(JdbcUtilPool.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if(conn != null){
            try {
                // 關閉 Connection 數據庫連接物件
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(JdbcUtilPool.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}

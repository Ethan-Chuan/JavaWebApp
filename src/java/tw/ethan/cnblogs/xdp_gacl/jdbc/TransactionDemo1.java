package tw.ethan.cnblogs.xdp_gacl.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;

/**
 *
 * @author Ethan
 */
public class TransactionDemo1 {
    
    public static void main(String[] args) {
        TransactionDemo1 obj = new TransactionDemo1();
        obj.testTransaction1();
    }
    
    // 模擬轉帳成功時的業務場景
    @Test
    public void testTransaction1(){
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        
        try {
            conn = JdbcUtils.getConnection();
            conn.setAutoCommit(false); // 通知數據庫開啟事務(start transaction)
            String sql1 = "update account set money=money-100 where name='A'";
            pstmt = conn.prepareStatement(sql1);
            pstmt.executeUpdate();
            String sql2 = "update account set money=money+100 where name='B'";
            pstmt = conn.prepareStatement(sql2);
            pstmt.executeUpdate();
            conn.commit();//上面的兩條 SQL 執行 Update 語句成功之後就通知數據庫提交事務(commit)
            System.out.println("成功！！");
        } catch (SQLException ex) {
            Logger.getLogger(TransactionDemo1.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            JdbcUtils.release(conn, pstmt, rs);
        }
    }
    
    // 模擬轉帳過程中出現異常導致有一部分 SQL 執行失敗後讓數據庫"自動"滾回事務
    @Test
    public void testTransaction2(){
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        
        try {
            conn = JdbcUtils.getConnection();
            conn.setAutoCommit(false); // 通知數據庫開啟事務(start transaction)
            String sql1 = "update account set money=money-100 where name='A'";
            pstmt = conn.prepareStatement(sql1);
            pstmt.executeUpdate();
            
            // 用這句話代碼模擬執行完 SQL1 之後程序出現了異常而導致後面的 SQL 無法正常執行，
            // 事務也無法正常提交，此時數據庫會自動執行滾回操作
            int x = 1/0;
            
            String sql2 = "update account set money=money+100 where name='B'";
            pstmt = conn.prepareStatement(sql2);
            pstmt.executeUpdate();
            conn.commit();//上面的兩條 SQL 執行 Update 語句成功之後就通知數據庫提交事務(commit)
            System.out.println("成功！！");
        } catch (Exception ex) {
            Logger.getLogger(TransactionDemo1.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            JdbcUtils.release(conn, pstmt, rs);
        }
    }
    
    // 模擬轉帳過程中出現異常導致有一部分 SQL 執行失敗時"手動"通知數據庫回滾事務
    @Test
    public void testTransaction3(){
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        
        try {
            conn = JdbcUtils.getConnection();
            conn.setAutoCommit(false); // 通知數據庫開啟事務(start transaction)
            String sql1 = "update account set money=money-100 where name='A'";
            pstmt = conn.prepareStatement(sql1);
            pstmt.executeUpdate();
            
            // 用這句話代碼模擬執行完 SQL1 之後程序出現了異常而導致後面的 SQL 無法正常執行，
            // 事務也無法正常提交
            int x = 1/0;
            
            String sql2 = "update account set money=money+100 where name='B'";
            pstmt = conn.prepareStatement(sql2);
            pstmt.executeUpdate();
            conn.commit();//上面的兩條 SQL 執行 Update 語句成功之後就通知數據庫提交事務(commit)
            System.out.println("成功！！");
        } catch (Exception ex) {
            try{
                // 捕獲到異常之後手動通知數據庫執行回滾事務的操作
                conn.rollback();
            }catch(SQLException e1){
                e1.printStackTrace();
            }
            Logger.getLogger(TransactionDemo1.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            JdbcUtils.release(conn, pstmt, rs);
        }
    }
}

/**
 * JDBC 中使用事務來模擬轉帳
 *  create table account(
 *      id int primary key auto_increment,
 *      name varchar(40),
 *      money float
 *  );
 *  
 *  insert into account(name, money) values('A', 1000);
 *  insert into account(name, money) values('B', 1000);
 *  insert into account(name, money) values('C', 1000);
 */

/**
事務的四大特性(ACID)
4.1、原子性（Atomicity）
　　原子性是指事務是一個不可分割的工作單位，事務中的操作要不然全部成功，要不然全部失敗。比如在同一個事務中的SQL語句，要不然全部執行成功，要不然全部執行失敗

4.2、一致性（Consistency）
　　官網上事務一致性的概念是：事務必須使數據庫從一個一致性狀態變換到另外一個一致性狀態。以轉帳為例子，A向B轉帳，假設轉帳之前這兩個用戶的錢加起來總共是2000，那麼A向B轉帳之後，不管這兩個賬戶怎麼轉，A用戶的錢和B用戶的錢加起來的總額還是2000，這個就是事務的一致性。

4.3、隔離性（Isolation）
　　事務的隔離性是多個用戶並發訪問數據庫時，數據庫為每一個用戶開啟的事務，不能被其他事務的操作數據所干擾，多個並發事務之間要相互隔離。

4.4、持久性（Durability）
　　持久性是指一個事務一旦被提交，它對數據庫中數據的改變就是永久性的，接下來即使數據庫發生故障也不應該對其有任何影響

事務的四大特性中最麻煩的是隔離性，下面重點介紹一下事務的隔離級別
問題：1.髒讀 2.不可重複讀 3.虛讀(幻讀)
隔離級別：
  1. Serializable(串行化)：可避免髒讀、不可重複讀、虛讀情況的發生
  2. Repeatable read(可重複讀)：可避免髒讀、不可重複讀情況的發生
  3. Read committed(讀已提交)：可避免髒讀情況發生
  4. Read uncommitted(讀未提交)：最低級別，以上情況均無法保證
*/
package tw.ethan.cnblogs.xdp_gacl.jdbc;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;

/**
 *
 * @author Ethan
 */
public class JdbcOperaBlob {

    public static void main(String[] args) {
        JdbcOperaBlob obj = new JdbcOperaBlob();
        obj.add();
        obj.read();
    }
    
    /**
     * @Method : add
     * @Description:向數據庫中插入二進制數據
     */
    @Test
    public void add(){
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try {
            conn = JdbcUtils.getConnection();
            st = conn.prepareStatement("delete from testblob where id=?");
            st.setInt(1, 1);
            int rows = st.executeUpdate();
            if(rows>0){
                System.out.println("刪除成功！");
            }
            
            String sql = "insert into testblob values(1,?)";
            st = conn.prepareStatement(sql);
            //這種方式獲取的路徑，其中的空格會被使用"%20"代替，\會被"%5c"代替
            String path = JdbcOperaClob.class.getClassLoader().getResource("tw\\ethan\\cnblogs\\xdp_gacl\\jdbc\\li_merma.jpg").getPath();
            //將"%20"替換回空格
            path = path.replaceAll("%20", " ");
            //將"%5c"替換回\
            path = path.replaceAll("%5c", "\\\\");
            File file = new File(path);
            // 生成的流
            FileInputStream fis = new FileInputStream(file);
            st.setBinaryStream(1, fis, (int) file.length());
            int num = st.executeUpdate();
            if(num>0){
                System.out.println("插入成功！");
            }
            fis.close();
        } catch (SQLException | FileNotFoundException ex) {
            Logger.getLogger(JdbcOperaBlob.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(JdbcOperaBlob.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            JdbcUtils.release(conn, st, rs);
        }
    }
    
    /**
     * @Method : read
     * @Description:讀取數據庫中的二進制數據
     */
    @Test
    public void read(){
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try {
            conn = JdbcUtils.getConnection();
            String sql = "select image from testblob where id=?";
            st = conn.prepareStatement(sql);
            st.setInt(1, 1);
            rs = st.executeQuery();
            if(rs.next()){
                //InputStream in = rs.getBlob("image").getBinaryStream();//這種方法也可以
                InputStream in = rs.getBinaryStream("image");
                int len = 0;
                byte buffer[] = new byte[1024];
                
                File file = new File("src\\java\\tw\\ethan\\cnblogs\\xdp_gacl\\jdbc\\Read_li_merma.jpg");
                FileOutputStream out;
                if(file.exists()){
                    out = new FileOutputStream(file);
                }else{
                    file.createNewFile();
                    out = new FileOutputStream(file, false);
                }
                while((len=in.read(buffer)) > 0){
                    out.write(buffer, 0, len);
                }
                in.close();
                out.close();
            }
        } catch (SQLException | IOException ex) {
            Logger.getLogger(JdbcOperaBlob.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            JdbcUtils.release(conn, st, rs);
        }
    }
}

/*
大數據也稱之為LOB(Large Objects)，LOB又分為：clob和blob，clob用於存儲大文本，blob用於存儲二進制數據，例如圖像、聲音、二進製文等。

　　在實際開發中，有時是需要用程序把大文本或二進制數據直接保存到數據庫中進行儲存的。

　　對MySQL而言只有blob，而沒有clob，mysql存儲大文本採用的是Text，Text和blob分別又分為：
　　TINYTEXT、TEXT、MEDIUMTEXT和LONGTEXT
　　TINYBLOB、BLOB、MEDIUMBLOB和LONGBLOB

--------------------------------------------------------------------------------
對於MySQL中的BLOB類型，可調用如下方法設置：
1 PreparedStatement. setBinaryStream(i, inputStream, length);
　　
對MySQL中的BLOB類型，可調用如下方法獲取：
1 InputStream in = resultSet.getBinaryStream(String columnLabel);
2 InputStream in = resultSet.getBlob(String columnLabel).getBinaryStream();

--------------------------------------------------------------------------------
編寫SQL測試腳本
create table testblob
(
     id int primary key auto_increment,
     image longblob
)

--------------------------------------------------------------------------------

*/
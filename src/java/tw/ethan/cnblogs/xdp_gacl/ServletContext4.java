package tw.ethan.cnblogs.xdp_gacl;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ethan
 */
@WebServlet(name = "ServletContext4", urlPatterns = {"/servlet-context4"})
public class ServletContext4 extends HttpServlet {
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String data = "<h1><font color=\"red\">abcdefghijkl</font></h1>";
        response.getOutputStream().write(data.getBytes());
        
        // 獲取 ServletContext 對象
        javax.servlet.ServletContext context = this.getServletConfig().getServletContext();
        
        // 獲取請求轉發對象(RequestDispatcher)
        javax.servlet.RequestDispatcher rd = context.getRequestDispatcher("/servlet-context5");
        
        // 調用 forward 方法實現請求轉發
        rd.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.doGet(request, response);
    }
    
}

/*
    訪問是ServletContext4，瀏覽器顯示ServletContext5的內容，這是使用
ServletContext實現請求轉發
*/
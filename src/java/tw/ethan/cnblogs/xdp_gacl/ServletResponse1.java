package tw.ethan.cnblogs.xdp_gacl;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ethan
 */
@WebServlet(name = "ServletResponse1", urlPatterns = {"/servlet-response1"})
public class ServletResponse1 extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // 使用 OutputStream 流 輸出中文
        outputChineseByOutputStream(response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.doGet(request, response);
    }
    
    private void outputChineseByOutputStream(HttpServletResponse response) throws IOException{
        /**
         * 使用的OutputStream流輸出中文注意問題：
         * 在服務器端，數據是以哪個碼表輸出的，那麼就要控制客戶端瀏覽器以相應的碼表
         * 打開，比如：outputStream.write("中國".getBytes("UTF-8"));
         * 使用的 OutputStream 流向客戶端瀏覽器輸出中文，以 UTF-8 的編碼進行輸出，
         * 此時就要控制客戶端瀏覽器以 UTF-8 的編碼打開，否則顯示的時候就會出現中文
         * 亂碼，那麼在服務器端如何控制 客戶端瀏覽器以 UTF-8 的編碼顯示數據呢？
         * 可以通過設置響應頭控制瀏覽器的行為，例如：
         * response.setHeader("content-type"，"text / html; charset = UTF-8");
         * 通過設置響應頭控制瀏覽器以UTF-8的編碼顯示數據。
         */
        String data = "中國";
        
        // 獲取 OutputStream 輸出流
        java.io.OutputStream outputStream = response.getOutputStream();
        
        // 通過設置響應頭控制瀏覽器以 UTF-8 的編碼顯示數據，如過不加這句話，
        // 那麼瀏覽器顯示的將是亂碼
        response.setHeader("content-type", "text/html;charset=UTF-8");
        
        // data.getBytes()是一個將字符轉換成字節數組的過程，這個過程中一定會去查表
        // getBytes()方法如果不帶參數，那麼就會根據操作系統的語言環境來選擇轉換碼表
        
        // 將字符轉換成字節數組，指定以UTF-8編碼進行轉換
        byte[] dataByteArr = data.getBytes("UTF-8");
        
        // 使用OutputStream流向客戶端輸出字節數組
        outputStream.write(dataByteArr);
    }
}

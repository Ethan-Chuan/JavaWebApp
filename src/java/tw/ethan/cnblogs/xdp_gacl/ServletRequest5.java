package tw.ethan.cnblogs.xdp_gacl;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ethan
 */
@WebServlet(name = "ServletRequest5", urlPatterns = {"/servlet-request5"})
public class ServletRequest5 extends HttpServlet {
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        /**
         * request.setCharacterEncoding("UTF-8");
         * 對於以get方式傳輸的數據，request即使設置了以指定的編碼接收數據也是無效的，
         * 默認的還是使用ISO8859-1這個字符編碼來接收數據
         */
        // 接收數據
        String name = request.getParameter("name");
        
        // 獲取request對象以ISO8859-1字符編碼接收到的原始數據的字節數組，然後通過
        // 字節數組以指定的編碼建構字符串，解決亂碼問題
        // tomcat7以前預設用ISO8859-1編碼，tomcat8之後預設用UTF-8
        //name = new String(name.getBytes("ISO8859-1"), "UTF-8");
        
        System.out.println("name: " + name);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Do nothing!
        response.setHeader("content-type", "text/html;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write("Do nothing!");
    }

}

/*
Tomcat7以前：
    服務器端的request對象使用的是ISO8859-1這個字符編碼來接收數據
Tomcat8以後：
    服務器端的request對象使用的是UTF-8這個字符編碼來接收數據
亂碼產生：
    服務器和客戶端溝通的編碼不一致因此才會產生中文亂碼的。
解決辦法：
    在接收到數據後，先獲取request對像以服務器預設字符編碼接收到的原始數據字節數組，
然後通過字節數組以指定的編碼構建字符串，解決亂碼問題。
*/
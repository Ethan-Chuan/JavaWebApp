package tw.ethan.cnblogs.xdp_gacl;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ethan
 */
@WebServlet(name = "HttpContentDisposition", urlPatterns = {"/content-disposition"})
public class HttpContentDisposition extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // 設置 content-disposition 響應頭，讓瀏覽器下載文件
        response.setHeader("content-disposition", "attachment;filename=xxx.jpg");
        
        // 讀取位於項目根目錄下的img文件夾裡面的 daytime-green-house-low-angle-photography-1134369.jpg
        // 圖片，並返回一個輸入流
        InputStream in = this.getServletContext().getResourceAsStream("/img/daytime-green-house-low-angle-photography-1134369.jpg");
        // 取得輸出流
        OutputStream out = response.getOutputStream();
        
        // 讀取輸入流(in)裡面的內容存儲到緩衝區(buffer) 並 輸出至瀏覽器
        byte[] buffer = new byte[1024];
        int len = 0;
        while( (len=in.read(buffer)) > 0){
            out.write(buffer, 0, len);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.doGet(request, response);
    }

}

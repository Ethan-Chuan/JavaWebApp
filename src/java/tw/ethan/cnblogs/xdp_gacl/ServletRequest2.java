package tw.ethan.cnblogs.xdp_gacl;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ethan
 */
@WebServlet(name = "ServletRequest2", urlPatterns = {"/servlet-request2"})
public class ServletRequest2 extends HttpServlet {
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // 設置字符以"UTF-8"編碼輸出至客戶端瀏覽器
        response.setCharacterEncoding("UTF-8");
        // 通過設置響應頭控制瀏覽器以"UTF-8"的編碼顯示數據
        response.setHeader("content-type", "text/html;charset=UTF-8");
        java.io.PrintWriter out = response.getWriter();
        
        // 獲得所有的請求頭
        java.util.Enumeration<String> reqHeadInfos = request.getHeaderNames();
        
        out.write("獲取到客戶端所有的請求頭信息如下：<br />");
        while(reqHeadInfos.hasMoreElements()){
            // 獲得請求頭的名字
            String headName = (String) reqHeadInfos.nextElement();
            // 根據請求頭的名字獲取對應的請求頭值
            String headValue = request.getHeader(headName);
            out.write(headName + " : " + headValue + "<br />");
        }
        out.write("<br /><hr />");
        out.write("獲取到客戶端Accept-Encoding請求頭的值：<br />");
        // 獲取Accept-Encoding請求頭對應的值
        String value = request.getHeader("Accept-Encoding");
        out.write(value);
        
        java.util.Enumeration<String> e = request.getHeaders("Accept-Encoding");
        while(e.hasMoreElements()){
            String str = (String) e.nextElement();
            System.out.println(str);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.doGet(request, response);
    }

}

/*
    getHeader(string name)方法:String
　　getHeaders(String name)方法:Enumeration
　　getHeaderNames()方法:Enumeration
*/
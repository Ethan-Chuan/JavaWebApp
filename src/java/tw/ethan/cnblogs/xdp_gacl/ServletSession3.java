package tw.ethan.cnblogs.xdp_gacl;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ethan
 */
@WebServlet(name = "ServletSession3", urlPatterns = {"/servlet-session3"})
public class ServletSession3 extends HttpServlet {
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id = request.getParameter("id");
        
        // 得到用戶想買的書
        tw.ethan.cnblogs.xdp_gacl.Book book = tw.ethan.cnblogs.xdp_gacl.DB.getAll().get(id);
        
        javax.servlet.http.HttpSession session = request.getSession();
        java.util.List<tw.ethan.cnblogs.xdp_gacl.Book> list = (java.util.List) session.getAttribute("list");
        if(list==null){
            list = new java.util.ArrayList<>();
            session.setAttribute("list", list);
        }
        list.add(book);
        
        // 對於sendRedirect方法後的url地址進行重寫
        String url = response.encodeRedirectURL(request.getContextPath() + "/servlet-session4");
        
        System.out.println(url);
        response.sendRedirect(url);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.doGet(request, response);
    }
    
}

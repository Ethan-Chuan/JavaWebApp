
package tw.ethan.cnblogs.xdp_gacl;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ethan
 */
@WebServlet(name = "ServletResponse2", urlPatterns = {"/servlet-response2"})
public class ServletResponse2 extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // 使用 PrintWriter流 輸出中文
        outputChineseByPrintWriter(response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.doGet(request, response);
    }
    
    private void outputChineseByPrintWriter(HttpServletResponse response) throws IOException{
        String data = "中國";
        
        /**
         * 通過設置響應頭控制瀏覽器以 UTF-8 的編碼顯示數據，
         * 如果不加這句話，那麼遊覽器顯示的將是亂碼
         */
        //response.setHeader("content-type", "text/html;charset=UTF-8");
        
        // 設置將字符以 "UTF-8" 編碼輸出到客戶端遊覽器
        response.setCharacterEncoding("UTF-8");
        
        /**
         * // 這句代碼必須放在 response.setCharacterEncoding("UTF-8"); 之後 
         * PrintWriter out = response.getWriter();
         * 
         * // 否則 這行代碼的設置將無效，瀏覽器顯示的時候還是亂碼
         * response.setCharacterEncoding("UTF-8");
         */
        // 獲取 PrintWriter 輸出流
        PrintWriter out = response.getWriter();
        
        /**
         * 多學一招：使用HTML語言裡面的<meta>標籤來控制瀏覽器行為，模擬通過設置
         * 響應頭控制瀏覽器行為
         * out.write("<meta http-equiv=\"content-type\" content=\"text/html;charset=UTF-8\">");
         * 等同於 response.setHeader("content-type", "text/html;charset=UTF-8");
         */
        out.write("<meta http-equiv=\"content-type\" content=\"text/html;charset=UTF-8\">");
        // 使用 PrintWriter 流向客戶端輸出字符
        out.write(data);
    }
}

/*
    在獲取的PrintWriter輸出流之前首先使用"response.setCharacterEncoding(charset)"
設置字符以什麼樣的編碼輸出到瀏覽器，如：response.setCharacterEncoding("UTF-8");
設置將字符以"UTF-8"編碼輸出到客戶端瀏覽器。
    然後再使用response.getWriter();獲取的PrintWriter輸出流，這兩個步驟不能顛倒。

    然後再使用response.setHeader("content-type", "text/html;charset=字符編碼");
設置響應頭，控制瀏覽器以指定的字符編碼編碼進行顯示。
    除此之外，還可以用
response.getWriter().write(
        "<meta http-equiv='content-type' 
               content='text/html;charset=UTF-8'/>");
來模擬響應頭的作用
*/
package tw.ethan.cnblogs.xdp_gacl;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ethan
 */
@WebServlet(name = "ServletThreadUnsafety", urlPatterns = {"/servlet-thread-unsafety"})
public class ServletThreadUnsafety extends HttpServlet {
    
    // Global variable
    int i = 1;
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // 把i定義成全局變量(global variable)，當多個線程並發訪問變量i時，就會存在
        // 線程安全問題了。如果 同時開啟兩個瀏覽器模擬並發訪問同一個Servlet，本來正
        // 常來說，第一個瀏覽器應該看到2，而第二個瀏覽器應該看到3的，結果兩個瀏覽器
        // 都看到了3，這就不正常。
        i++;
        try {
            Thread.sleep(1000*4);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
        response.getWriter().write(i + "");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.doGet(request, response);
    }

}

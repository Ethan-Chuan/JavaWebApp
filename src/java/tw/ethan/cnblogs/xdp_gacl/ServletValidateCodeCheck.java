package tw.ethan.cnblogs.xdp_gacl;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ethan
 */
@WebServlet(name = "ServletValidateCodeCheck", urlPatterns = {"/servlet-validate-code-check"})
public class ServletValidateCodeCheck extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setHeader("content-type", "text/html;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setHeader("refresh", "5;url=\"/JavaWebApp/cnblogs/xdp_gacl/servlet-draw-image.jsp\"");
        
        //接收客戶端瀏覽器提交上來的驗證碼
        String clientCheckcode = request.getParameter("validateCode");
        
        //從服務器端的session中取出驗證碼
        String serverCheckcode = (String) request.getSession().getAttribute("checkcode");
        
        //將客戶端驗證碼與服務器端驗證碼比較，如果相等，則表示驗證通過
        if(clientCheckcode.equals(serverCheckcode)){
            System.out.println("驗證碼驗證通過!");
            response.getWriter().write("驗證碼驗證成功！<br/><br/>準備導向至...");
        }else{
            System.out.println("驗證碼驗證失敗...");
            response.getWriter().write("驗證碼驗證失敗...<br/><br/>準備回到登入頁面...");
        }
        
        
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.doGet(request, response);
    }

}

package tw.ethan.cnblogs.xdp_gacl;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ethan
 */
@WebServlet(name = "HttpResHeader", urlPatterns = {"/response-header.view"})
public class HttpResHeader extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // 設置服務器的響應狀態碼
        response.setStatus(302);
        
        // 設置響應頭，服務器通過 Location 這個頭，來告訴瀏覽器跳至哪裡
        // 這就是所謂的「請求重定向」
        response.setHeader("Location", "/JavaWebApp/cnblogs/xdp_gacl/1.jsp");
        
        /* Result : 
            HTTP/1.1 302
            Location: /JavaWebApp/cnblogs/xdp_gacl/1.jsp
            Content-Length: 0
            Date: Wed, 06 Jun 2018 07:06:45 GMT
            Server: Apache-Coyote/1.1
        */
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.doGet(request, response);
    }

}

/*
在HTTP1.0協議中，客戶端與web服務器建立連接後，只能獲得一個web資源。
在HTTP1.1協議，允許客戶端與web服務器建立連接後，在一個連接上獲取多個web資源。
--------------------------------------------------------------------------------
HTTP請求中的常用消息頭
　　accept:瀏覽器通過這個頭告訴服務器，它所支持的數據類型
　　Accept-Charset: 瀏覽器通過這個頭告訴服務器，它支持哪種字符集
　　Accept-Encoding：瀏覽器通過這個頭告訴服務器，支持的壓縮格式
　　Accept-Language：瀏覽器通過這個頭告訴服務器，它的語言環境
　　Host：瀏覽器通過這個頭告訴服務器，想訪問哪台主機
　　If-Modified-Since: 瀏覽器通過這個頭告訴服務器，緩存數據的時間
　　Referer：瀏覽器通過這個頭告訴服務器，客戶機是哪個頁面來的 防盜鏈
　　Connection：瀏覽器通過這個頭告訴服務器，請求完後是斷開鏈接還是何持鏈接
--------------------------------------------------------------------------------
狀態行格式： HTTP版本號　狀態碼　原因敘述<CRLF>
    舉例：HTTP/1.1 200 OK
    狀態碼用於表示服務器對請求的處理結果，它是一個三位的十進制數。響應狀態碼分為5類，如下所示：
狀態碼              含意   
100 ~ 199   表達成功接收請求，要求客戶端繼續提交下一次請求才能完成整個處理過程
200 ~ 299   表示成功接收請求並已完成整個處理過程，常用200
300 ~ 399   為完成請求，客戶需進一步細化請求。例如，請求的資源已經移動一個新地址，常用302、307和304
400 ~ 499   客戶端的請求有錯誤，常用404
500 ~ 599   服務器端出現錯誤，常用500
--------------------------------------------------------------------------------
HTTP響應中的常用響應頭(消息頭)
　　Location: 服務器通過這個頭，來告訴瀏覽器跳到哪裡
　　Server：服務器通過這個頭，告訴瀏覽器服務器的型號
　　Content-Encoding：服務器通過這個頭，告訴瀏覽器，數據的壓縮格式
　　Content-Length: 服務器通過這個頭，告訴瀏覽器回送數據的長度
　　Content-Language: 服務器通過這個頭，告訴瀏覽器語言環境
　　Content-Type：服務器通過這個頭，告訴瀏覽器回送數據的類型
　　Refresh：服務器通過這個頭，告訴瀏覽器定時刷新
　　Content-Disposition: 服務器通過這個頭，告訴瀏覽器以下載方式打數據
　　Transfer-Encoding：服務器通過這個頭，告訴瀏覽器數據是以分塊方式回送的
　　Expires: -1 控制瀏覽器不要緩存
　　Cache-Control: no-cache
　　Pragma: no-cache
*/
package tw.ethan.cnblogs.xdp_gacl;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ethan
 */
@WebServlet(name = "ServletResponse3", urlPatterns = {"/servlet-response3"})
public class ServletResponse3 extends HttpServlet {
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        boolean isOutputStream = !true;
        if(isOutputStream)
            outputOneByOutputStream(response);
        else
            outputOneByPrintWriter(response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.doGet(request, response);
    }
    
    private void outputOneByOutputStream(HttpServletResponse response) throws IOException{
        response.setHeader("content-type", "text/html;charset=UTF-8");
        java.io.OutputStream outputStream = response.getOutputStream();
        outputStream.write("使用OutputStream流輸出數字1： ".getBytes("UTF-8"));
        
        // 運行的結果和我們想像的不一樣，數字 1 沒有輸出出來
        //outputStream.write(1);
        
        /**
         * 1+""這一步是將數字1和一個空字符串相加，這樣處理之後，數字1就變成了
         * 字符串1了，然後再將字符串1轉換成字節數組使用OutputStream進行輸出
         */
        outputStream.write((1+"").getBytes());
    }
    
    private void outputOneByPrintWriter(HttpServletResponse response) throws IOException{
        response.setHeader("content-type", "text/html;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        java.io.PrintWriter out = response.getWriter();
        out.write("使用PrintWriter流輸出數字1： ");
        
        // 運行的結果和我們想像的不一樣，數字 1 沒有輸出出來
        //out.write(1);
        
        /**
         * 如果使用PrintWriter流輸出數字，那麼也要先將數字轉換成字符串後再輸出
         */
        out.write(1+"");
    }
}

/*
    在開發過程中，如果希望服務器輸出什麼瀏覽器就能看到什麼，
那麼在服務器端都要以字符串的形式進行輸出。
*/
package tw.ethan.cnblogs.xdp_gacl.webtag;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.SkipPageException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 * 防盜鏈標籤RefererTag
 * @author Ethan
 */
public class RefererTag extends SimpleTagSupport {
    /**
     * 網站域名
     */
    private String site;
    
    /**
     * 要跳轉的頁面
     */
    private String page;

    /**
     * site屬性對應的set方法
     * @param site 
     */
    public void setSite(String site) {
        this.site = site;
    }

    /**
     * page屬性對應的set方法
     * @param page 
     */
    public void setPage(String page) {
        this.page = page;
    }

    @Override
    public void doTag() throws JspException, IOException {
        // 獲取jsp頁面的 PageContext 對象
        PageContext pageContext = (PageContext) this.getJspContext();
        // 通過 PageContext 對象來獲取 HttpServletRequest 對象
        HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
        // 獲取請求的來路(Referer)
        String referer = request.getHeader("referer");
        // 如果來路是null或者來路不是來自我們自己的site，那麼就將請求重定向到page頁面
        if(referer == null || !referer.startsWith(site)){
            // 獲取HttpServletResponse對象
            HttpServletResponse response = (HttpServletResponse)pageContext.getResponse();
            String webRoot = request.getContextPath();
            
            if(page.startsWith(webRoot)){
                // 重定向到page頁面
                response.sendRedirect(page);
            }else{
                // 重定向到page頁面
                response.sendRedirect(webRoot + page);
            }
            // 重定向後，控制保護的頁面不要執行
            throw new SkipPageException();
        }
    }
    
}

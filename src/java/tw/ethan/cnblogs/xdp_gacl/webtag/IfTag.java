package tw.ethan.cnblogs.xdp_gacl.webtag;

import java.io.IOException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 * 開發 if 標籤
 * @author Ethan
 */
public class IfTag extends SimpleTagSupport {
    /**
     * if 標籤的 test 屬性
     */
    private boolean test;

    /**
     * test 屬性的set方法
     * @param test 
     */
    public void setTest(boolean test) {
        this.test = test;
    }

    /**
     * 實現 if 標籤
     * @throws JspException
     * @throws IOException 
     */
    @Override
    public void doTag() throws JspException, IOException {
        if(test){
            this.getJspBody().invoke(null);
        }
    }
    
}

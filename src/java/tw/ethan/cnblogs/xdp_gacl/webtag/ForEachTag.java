package tw.ethan.cnblogs.xdp_gacl.webtag;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 * For Each 疊代標籤
 * @author Ethan
 */
public class ForEachTag extends SimpleTagSupport {
    private Object items;
    private String var;
    private Collection collection;

    public void setItems(Object items) {
        if(items instanceof Collection){
            collection = (Collection) items; // List
        }else if(items instanceof Map){
            Map map = (Map) items;
            collection = map.entrySet(); // Map
        }else if(items.getClass().isArray()){
            collection = new ArrayList();
            // 獲取數組的長度
            int len = Array.getLength(items);
            for(int i=0;i<len;i++){
                // 獲取數組元素
                Object object = Array.get(items, i);
                collection.add(object);
            }
        }
        this.items = items;
    }

    public void setVar(String var) {
        this.var = var;
    }

    @Override
    public void doTag() throws JspException, IOException {
        PageContext pageContext = (PageContext) this.getJspContext();
        // 疊代collection集合
        Iterator it = collection.iterator();
        while(it.hasNext()){
            // 得到一個疊代出來的對象
            Object object = (Object) it.next();
            // 將疊代出來的對象存放到pageContext對象中
            pageContext.setAttribute(var, object);
            // 輸出標籤體中的內容
            this.getJspBody().invoke(null);
        }
    }
    
}

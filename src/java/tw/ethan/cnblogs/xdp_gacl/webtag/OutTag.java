package tw.ethan.cnblogs.xdp_gacl.webtag;

import java.io.IOException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 * 開發輸出標籤
 * @author Ethan
 */
public class OutTag extends SimpleTagSupport {
    /**
     * 要輸出的內容
     */
    private String content;
    
    /**
     * 是否將內容中的html進行轉義後輸出
     */
    private boolean escapeHtml;

    public void setContent(String content) {
        this.content = content;
    }

    public void setEscapeHtml(boolean escapeHtml) {
        this.escapeHtml = escapeHtml;
    }
    
    /**
     * 
     * @param message
     * @return 轉義html標籤 
     */
    private String filter(String message){
        if(message == null){
            return null;
        }
        char content[] = new char[message.length()];
        message.getChars(0, message.length(), content, 0);
        StringBuilder result = new StringBuilder(content.length + 50);
        for(int i=0;i<content.length;i++){
            switch(content[i]){
                case '<':
                    result.append("&lt;");
                    break;
                case '>':
                    result.append("&gt;");
                    break;
                case '&':
                    result.append("&amp;");
                    break;
                case '"':
                    result.append("&quot;");
                    break;
                default:
                    result.append(content[i]);
            }
        }
        return result.toString();
    }

    @Override
    public void doTag() throws JspException, IOException {
        if(escapeHtml == true){
            // 轉義內容中的html代碼
            content = filter(content);
            // 輸出轉義後的content
            this.getJspContext().getOut().write(content);
        }else{
            this.getJspContext().getOut().write(content);
        }
    }
    
}

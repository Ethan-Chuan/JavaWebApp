package tw.ethan.cnblogs.xdp_gacl.webtag;

import java.io.IOException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 * when 標籤和 otherwise 標籤的父標籤
 * @author Ethan
 */
public class ChooseTag extends SimpleTagSupport {
    /**
     * 定義一個boolean類型的屬性，該屬性用於標識該標籤下的某一個子標籤是否已經執行過了
     * 如果該標籤下的某一個子標籤已經執行過了，就將該屬性設置為true
     */
    private boolean isExcute;

    public boolean isExcute() {
        return isExcute;
    }

    public void setExcute(boolean isExcute) {
        this.isExcute = isExcute;
    }

    @Override
    public void doTag() throws JspException, IOException {
        // 輸出標籤體中的內容
        this.getJspBody().invoke(null);
    }
    
}

/*
    <wtt:when>標籤和<wtt:otherwise>標籤對應著兩個不同的標籤處理器類，我們希望做到
的效果是，如果<wtt:when>標籤執行了，那麼就<wtt:otherwise>標籤就不要再執行，那麼這
裡面就涉及到一個問題：<wtt:when>標籤執行的時候該如何通知<wtt:otherwise>標籤不要執
行了呢？這個問題就涉及到了兩個標籤處理器類如何做到相互通訊的問題，如果<wtt:when>標
籤執行了，就要通過某種方式告訴<wtt:otherwise>標籤不要執行，那麼該如何做到這樣的效
果呢？讓<wtt:when>標籤處理器類和<wtt:otherwise>標籤處理器類共享同一個變量就可以做
到了，那麼又該怎麼做才能夠讓兩個標籤處理器類共享同一個變量呢，標準的做法是這樣的：
讓兩個標籤擁有同一個父標籤<wtt:choose>。
*/
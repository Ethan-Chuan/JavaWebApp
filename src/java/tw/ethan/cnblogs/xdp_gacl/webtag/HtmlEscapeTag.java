package tw.ethan.cnblogs.xdp_gacl.webtag;

import java.io.IOException;
import java.io.StringWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 * html 轉義標籤
 * @author Ethan
 */
public class HtmlEscapeTag extends SimpleTagSupport {
    
    private String filter(String message){
        if(message == null){
            return null;
        }
        char content[] = new char[message.length()];
        message.getChars(0, message.length(), content, 0);
        StringBuilder result = new StringBuilder(content.length + 50);
        for(int i=0;i<content.length;i++){
            switch(content[i]){
                case '<':
                    result.append("&lt;");
                    break;
                case '>':
                    result.append("&gt;");
                    break;
                case '&':
                    result.append("&amp;");
                    break;
                case '"':
                    result.append("&quot;");
                    break;
                default:
                    result.append(content[i]);
            }
        }
        return result.toString();
    }

    @Override
    public void doTag() throws JspException, IOException {
        StringWriter sw = new StringWriter();
        // 將標籤體中的內容先輸出到StringWriter流
        this.getJspBody().invoke(sw);
        // 得到標籤體中的內容
        String content = sw.getBuffer().toString();
        // 轉義標籤體中的html代碼
        content = filter(content);
        // 輸出轉義後的content
        this.getJspContext().getOut().write(content);
    }
    
}

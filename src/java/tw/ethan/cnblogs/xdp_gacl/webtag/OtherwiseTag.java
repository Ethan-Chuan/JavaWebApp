package tw.ethan.cnblogs.xdp_gacl.webtag;

import java.io.IOException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 * otherwise 標籤
 * @author Ethan
 */
public class OtherwiseTag extends SimpleTagSupport {

    @Override
    public void doTag() throws JspException, IOException {
        // 獲取標籤的父標籤
        ChooseTag parentTag = (ChooseTag) this.getParent();
        // 如果父標籤下的when標籤沒有執行過
        if(parentTag.isExcute() == false){
            // 輸出標籤體中的內容
            this.getJspBody().invoke(null);
            // 設置父標籤的isExecute屬性為true，告訴父標籤，我(otherwise標籤)已經執行過了
            parentTag.setExcute(true);
        }
    }
    
}

/*
    <wtt:when>標籤和<wtt:otherwise>標籤對應著兩個不同的標籤處理器類，我們希望做到
的效果是，如果<wtt:when>標籤執行了，那麼就<wtt:otherwise>標籤就不要再執行，那麼這
裡面就涉及到一個問題：<wtt:when>標籤執行的時候該如何通知<wtt:otherwise>標籤不要執
行了呢？這個問題就涉及到了兩個標籤處理器類如何做到相互通訊的問題，如果<wtt:when>標
籤執行了，就要通過某種方式告訴<wtt:otherwise>標籤不要執行，那麼該如何做到這樣的效
果呢？讓<wtt:when>標籤處理器類和<wtt:otherwise>標籤處理器類共享同一個變量就可以做
到了，那麼又該怎麼做才能夠讓兩個標籤處理器類共享同一個變量呢，標準的做法是這樣的：
讓兩個標籤擁有同一個父標籤<wtt:choose>。
*/
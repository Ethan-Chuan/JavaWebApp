package tw.ethan.cnblogs.xdp_gacl;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ethan
 */
@WebServlet(name = "ElDemo3", urlPatterns = {"/cnblogs/xdp_gacl/el-demo3"})
public class ElDemo3 extends HttpServlet {
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * 處理用戶註冊的方法
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // 客戶端是以UTF-8編碼提交表單數據的，所以需要設置服務器端以UTF-8的編碼進行接收，否則對於中文數據就會產生亂碼
        request.setCharacterEncoding("UTF-8");
        
        
        // 1. 接收參數
        String userName = request.getParameter("username");
        
        // 2. 直接跳轉回/el-demo3.jsp頁面，沒有使用request.setAttribute("userName", userName) 將userName存儲到request對象中
        // 但是在el-demo3.jsp頁面中可以使用${param.username}獲取到request對象中的username參數的值
        request.getRequestDispatcher("/cnblogs/xdp_gacl/el-demo3.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.doGet(request, response);
    }

}

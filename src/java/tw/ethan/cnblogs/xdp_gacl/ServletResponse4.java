
package tw.ethan.cnblogs.xdp_gacl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ethan
 */
@WebServlet(name = "ServletResponse4", urlPatterns = {"/servlet-response4"})
public class ServletResponse4 extends HttpServlet {
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        downloadChineseFileByOutputStream(response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.doGet(request, response);
    }
    
    
    private void downloadChineseFileByOutputStream(HttpServletResponse response) 
            throws UnsupportedEncodingException, FileNotFoundException, IOException{
        // 獲取要下載的文件的絕對路徑
        String realPath = this.getServletContext().getRealPath("/img/牛肉-模糊-麵包.jpg");
        
        // 獲取要下載的文件名
        String fileName = realPath.substring(realPath.lastIndexOf("\\")+1);
        
        // 設置content-disposition響應頭控制瀏覽器以下載的形式打開文件
        // 中文文件名要使用URLEncoder.encode方法進行編碼，否則會出現文件名亂碼
        response.setHeader(
                "content-disposition", 
                "attachment;filename="+URLEncoder.encode(fileName, "UTF-8"));
        
        // 獲取文件輸入流
        java.io.InputStream in = new java.io.FileInputStream(realPath);
        
        int len = 0;
        byte[] buffer = new byte[1024];
        java.io.OutputStream out = response.getOutputStream();
        while((len=in.read(buffer)) > 0)
            // 將緩衝區的數據輸出到客戶端瀏覽器
            out.write(buffer, 0, len);
        
        in.close();
        out.close();
    }
}

/*
    編寫文件下載功能時推薦使用OutputStream流，避免使用PrintWriter流，因為
OutputStream流是字節流，可以處理任意類型的數據，而PrintWriter流是字符流，
只能處理字符數據，如果用字符流處理字節數據，會導致數據丟失。
*/
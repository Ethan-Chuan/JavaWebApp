package tw.ethan.cnblogs.xdp_gacl;

import java.io.IOException;
import java.text.MessageFormat;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 用類裝載器讀取資源文件
 * 通過類裝載器讀取資源文件的注意事項：不適合裝載大文件，否則會導致 jvm 內存溢出
 * @author Ethan
 */
@WebServlet(urlPatterns = {"/servlet-context-classLoader"})
public class ServletContextClassLoader extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // response.setContentType("text/html;charset=UTF-8");目的是控制瀏覽器用 UTF-8 進行解碼
        // 這樣就不會出現中文亂碼了
        response.setHeader("content-type", "text/html;charset=UTF-8");
        
        test1(response);
        response.getWriter().println("<hr/>");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.doGet(request, response);
    }
    
    
    private void test1(HttpServletResponse response) throws IOException{
        // 讀取類路徑下的資源
        // 讀取到裝載當前類的類裝載器 (適用小檔案)
        ClassLoader loader = ServletContextClassLoader.class.getClassLoader();
        
        // 用類裝載器讀取 WEB-INF 目錄下的 db1.properties 配置文件
        // 起始路徑 /WEB-INF/classes
        java.io.InputStream in = loader.getResourceAsStream("../db1.properties");
        
        java.util.Properties prop = new java.util.Properties();
        prop.load(in);
        
        String driver = prop.getProperty("driver");
        String url = prop.getProperty("url");
        String username = prop.getProperty("username");
        String password = prop.getProperty("password");
        
        response.getWriter().println("用類裝載器讀取src目錄下的db1.properties配置文件");
        response.getWriter().println(MessageFormat.format(
                "driver={0}, url={1}, username={2}, password={3}", 
                driver, url, username, password));
    }
    
}

/**
 * 獲取實際路徑
 * String path = this.getServletContext().getRealPath();
 * 
 * 獲取文件名
 * path.substring( path.lastIndexOf("\\") + 1 );
 * 
 * 如果檔案較大，建議使用 ServletContext 獲取輸入流
 * InputStream in = this.getServletContext().getResourceAsStream();
 * 
 * 檔案下載，請參考 HttpContentDisposition.java
 */
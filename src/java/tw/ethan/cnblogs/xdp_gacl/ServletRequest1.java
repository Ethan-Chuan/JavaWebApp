package tw.ethan.cnblogs.xdp_gacl;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ethan
 */
@WebServlet(name = "ServletRequest1", urlPatterns = {"/servlet-request1"})
public class ServletRequest1 extends HttpServlet {
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // 獲得 客戶端 資訊
        
        // 得到請求的URL地址
        String requestUrl = request.getRequestURL().toString();
        
        // 得到請求的資源
        String requestUri = request.getRequestURI();
        
        // 得到請求的URL地址中附帶的參數
        String queryString = request.getQueryString();
        
        // 得到來訪者的IP地址
        String remoteAddr = request.getRemoteAddr();
        
        // 得到來訪者的主機名稱
        String remoteHost = request.getRemoteHost();
        
        // 得到來訪者的IP來源端口
        int remotePort = request.getRemotePort();
        
        // 得到來訪者的使用者名稱
        String remoteUser = request.getRemoteUser();
        
        // 得到請求URL地址時使用的方法
        String method = request.getMethod();
        
        // 指定在servlet路徑之後但在請求URL中的查詢字符串之前的額外路徑信息
        String pathInfo = request.getPathInfo();
        
        // 獲取WEB服務器的IP地址
        String localAddr = request.getLocalAddr();
        
        // 獲取WEB服務器的主機名
        String localName = request.getLocalName();
        
        // 設置字符以"UTF-8"編碼輸出到客戶端瀏覽器
        response.setCharacterEncoding("UTF-8");
        
        // 通過設置響應頭控制瀏覽器以UTF-8的編碼顯示數據，如果不加這句話，那麼瀏覽器顯示的將是亂碼
        response.setHeader("content-type", "text/html;charset=UTF-8");
        
        // 獲取字元輸出物件
        java.io.PrintWriter out = response.getWriter();
        
        out.write("獲取到的客戶端信息如下：");
        out.write("<hr />");
        out.write("請求的URL地址： " + requestUrl);
        out.write("<br />");
        out.write("請求的資源： " + requestUri);
        out.write("<br />");
        out.write("請求的URL地址中附帶的參數： " + queryString );
        out.write("<br />");
        out.write("來訪者的IP地址： " + remoteAddr);
        out.write("<br />");
        out.write("來訪者的主機名： " + remoteHost);
        out.write("<br />");
        out.write("使用的端口號： " + remotePort);
        out.write("<br />");
        out.write("來訪的使用者： " + remoteUser);
        out.write("<br />");
        out.write("請求使用的方式： " + method);
        out.write("<br />");
        out.write("額外路徑信息： " + pathInfo);
        out.write("<br />");
        out.write("服務器的地址： " + localAddr);
        out.write("<br />");
        out.write("服務器的主機名： " + localName);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.doGet(request, response);
    }
    
}

/*
    getRequestURL方法返回客戶端發出請求時的完整URL。
　　getRequestURI方法返回請求行中的資源名部分。
　　getQueryString 方法返回請求行中的參數部分。
　　getPathInfo方法返回請求URL中的額外路徑信息。額外路徑信息是請求URL中的位於Servlet的路徑之後和查詢參數之前的內容，它以“/”開頭。
　　getRemoteAddr方法返回發出請求的客戶機的IP地址。
　　getRemoteHost方法返回發出請求的客戶機的完整主機名。
　　getRemotePort方法返回客戶機所使用的網絡端口號。
　　getLocalAddr方法返回WEB服務器的IP地址。
　　getLocalName方法返回WEB服務器的主機名。
--------------------------------------------------------------------------------
使用 ：
    http://127.0.0.1:8080/JavaWebApp/servlet-request1?parA=xdp&parB=gacl
結果：
    獲取到的客戶端信息如下：
    請求的URL地址： http://127.0.0.1:8080/JavaWebApp/servlet-request1
    請求的資源： /JavaWebApp/servlet-request1
    請求的URL地址中附帶的參數： parA=xdp&parB=gacl
    來訪者的IP地址： 127.0.0.1
    來訪者的主機名： 127.0.0.1
    使用的端口號： 3530
    來訪的使用者： null
    請求使用的方式： GET
    額外路徑信息： null
    服務器的地址： 127.0.0.1
    服務器的主機名： www.myjavaweb.tw
--------------------------------------------------------------------------------
使用：
    http://localhost:8080/JavaWebApp/servlet-request1?parA=xdp&parB=gacl
結果：
    獲取到的客戶端信息如下：
    請求的URL地址： http://localhost:8080/JavaWebApp/servlet-request1
    請求的資源： /JavaWebApp/servlet-request1
    請求的URL地址中附帶的參數： parA=xdp&parB=gacl
    來訪者的IP地址： 0:0:0:0:0:0:0:1
    來訪者的主機名： 0:0:0:0:0:0:0:1
    使用的端口號： 3562
    來訪的使用者： null
    請求使用的方式： GET
    額外路徑信息： null
    服務器的地址： 0:0:0:0:0:0:0:1
    服務器的主機名： 0:0:0:0:0:0:0:1
*/
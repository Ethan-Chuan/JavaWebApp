package tw.ethan.cnblogs.xdp_gacl;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ethan
 */
@WebServlet(name = "ServletPreventDuplicateSubmissionOfForms1", urlPatterns = {"/servlet-prevent-duplicate-submission-of-forms1"})
public class ServletPreventDuplicateSubmissionOfForms1 extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        
        // 判斷用戶是否重複提交
        boolean b = isRepeatSubmit(request);
        if(b){
            System.out.println("請不要重複提交");
            return;
        }
        
        // 移除session中的token
        request.getSession().removeAttribute("token");
        
        System.out.println("處理用戶提交請求！！ User name : " + request.getParameter("username"));
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.doGet(request, response);
    }
    
    private boolean isRepeatSubmit(HttpServletRequest request){
        // 取出存儲在表單中隱藏域的token
        String client_token = request.getParameter("token");
        // 1. 如果用戶提交的表單數據中沒有token，則用戶是重複提交了表單
        if(client_token==null){
            return true;
        }
        
        // 取出存儲在Session中的token
        String server_token = (String) request.getSession().getAttribute("token");
        // 2.如果當前用戶的Session中不存在Token(令牌)，則用戶是重複提交了表單
        if(server_token==null){
            return true;
        }
        
        // 3.存儲在Session中的Token(令牌)與表單提交的Token(令牌)不同，則用戶是重複提交了表單
        if(!client_token.equals(server_token)){
            return true;
        }
        
        return false;
    }
}

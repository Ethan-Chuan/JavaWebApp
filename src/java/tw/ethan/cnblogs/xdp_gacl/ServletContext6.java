package tw.ethan.cnblogs.xdp_gacl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.MessageFormat;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ethan
 */
@WebServlet(name = "ServletContext6", urlPatterns = {"/servlet-context6"})
public class ServletContext6 extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        /*
            response.setContentType("text/html;charset=UTF-8");目的是控制
        瀏覽器用UTF-8進行解碼；這樣就不會出現中文亂碼了
        */
        response.setHeader("content-type", "text/html;charset=UTF-8");
        
        String dbPath1 = "/WEB-INF/db1.properties";
        String dbPath2 = "/db2.properties";
        String dbPath3 = "/WEB-INF/db/config/db3.properties";
        String dbPath4 = "/WEB-INF/classes/tw/ethan/cnblogs/xdp_gacl/db4.properties";
        
        readPropCfgFile(response, dbPath1);
        response.getWriter().write("<hr/>");
        readPropCfgFile(response, dbPath2);
        response.getWriter().write("<hr/>");
        readPropCfgFile(response, dbPath3);
        response.getWriter().write("<hr/>");
        readPropCfgFile(response, dbPath4);
        response.getWriter().write("<hr/>");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.doGet(request, response);
    }
    
    private void readPropCfgFile(HttpServletResponse response, String path) throws IOException{
        java.io.InputStream in = this.getServletConfig().getServletContext().getResourceAsStream(path);
        java.util.Properties prop = new java.util.Properties();
        prop.load(in);
        display(response, path, prop);
    }
    
    private void readPropCfgFileSpecial(HttpServletResponse response, String path) throws FileNotFoundException, IOException{
        // 通過ServletContext獲取web資源的絕對路徑
        String realPath = this.getServletConfig().getServletContext().getRealPath(path);
        
        java.io.InputStream in = new java.io.FileInputStream(realPath);
        java.util.Properties prop = new java.util.Properties();
        prop.load(in);
        display(response, path, prop);
    }
    
    private void display(HttpServletResponse response, String path, java.util.Properties prop) throws IOException{
        String driver = prop.getProperty("driver");
        String url = prop.getProperty("url");
        String username = prop.getProperty("username");
        String password = prop.getProperty("password");
        
        response.getWriter().write("讀取 " + path + " 配置文件<br/><br/>");
        response.getWriter().write(
                MessageFormat.format(
                        "&nbsp;driver={0}<br/>&nbsp;url={1}<br/>&nbsp;username={2}<br/>&nbsp;password={3}<br/><br/>", 
                        driver, url, username, password));
    }
}

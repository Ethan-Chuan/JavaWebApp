package tw.ethan.cnblogs.xdp_gacl;

/**
 *
 * @author Ethan
 */
public class BeanPerson {
    private String age;
    private String home;
    private String name;
    private String sex;

    public String getAge() {
        return age;
    }

    public String getHome() {
        return home;
    }

    public String getName() {
        return name;
    }

    public String getSex() {
        return sex;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
    
}

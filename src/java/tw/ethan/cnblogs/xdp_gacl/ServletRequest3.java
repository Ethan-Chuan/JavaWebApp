package tw.ethan.cnblogs.xdp_gacl;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ethan
 * 獲取客戶端通過Form表單提交上來的參數
 */
@WebServlet(name = "ServletRequest3", urlPatterns = {"/servlet-request3"})
public class ServletRequest3 extends HttpServlet {
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Nothing to do.
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        boolean isMap = !true;
        
        if(isMap)
            // Use getParameterMap method.
            useGetParameterMap(request, response);
        else
            // Use getParamter and getParameterValues method.
            useGetParameterAndGetParameterValues( request,  response);
    }
    
    private void useGetParameterAndGetParameterValues(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException{
        // 客戶端是以UTF-8編碼提交表單數據的，所以需要設置服務器端以UTF-8的編碼進行接收，否則對於中文數據就會產生亂碼
        request.setCharacterEncoding("UTF-8");
        
        /**
         * 編&nbsp;&nbsp;號(文本框)：
         * <input type="text" name="userid" value="NO." size="2" maxlength="2">
         */
        // 獲取填寫的編號，userid是文本框的名字
        String userid = request.getParameter("userid");
        
        /**
         * 用戶名(文本框)：<input type="text" name="username" value="請輸入用戶名">
         */
        // 獲取填寫的用戶名
        String username = request.getParameter("username");
        
        /**
         * 密&nbsp;&nbsp;碼(密碼框)：<input type="password" name="userpass" value="請輸入密碼">
         */
        // 獲取填寫的密碼
        String userpass = request.getParameter("userpass");
        
        // 獲得選中的性別
        String sex = request.getParameter("sex");
        
        // 獲得選中的部門
        String dept = request.getParameter("dept");
        
        // 獲得選中的興趣，因為可以選擇多個值，所以獲取到的值是一個字符數組，因此需要使用getParameterValues方法來獲取
        String[] insts = request.getParameterValues("inst");
        
        // 獲得填寫的說明信息
        String note = request.getParameter("note");
        
        // 獲得隱藏域的內容
        String hiddenField = request.getParameter("hiddenField");
        
        
        String instStr="";
        // 獲取數組數據的技巧，可以避免insts數組為null時引發的空指針異常錯誤!
        for(int i=0; insts!=null && i<insts.length; i++)
            if(i==insts.length-1)
                instStr += insts[i];
            else
                instStr += insts[i]+",";
        
        String htmlStr = "<table>" +
                "<tr><td>填寫的編號：</td><td>{0}<td/></tr>" +
                "<tr><td>填寫的用戶名：</td><td>{1}</td></tr>" + 
                "<tr><td>填寫的密碼：</td><td>{2}</td></tr>" + 
                "<tr><td>填寫的性別：</td><td>{3}</td></tr>" + 
                "<tr><td>填寫的部門：</td><td>{4}</td></tr>" + 
                "<tr><td>填寫的興趣：</td><td>{5}</td></tr>" + 
                "<tr><td>填寫的說明：</td><td>{6}</td></tr>" + 
                "<tr><td>隱藏域的內容：</td><td>{7}</td></tr>" +
                "</table>";
        htmlStr = MessageFormat.format(htmlStr, userid, username, userpass, sex, dept, instStr, note, hiddenField);
        
        
        // 設置服務器端以UTF-8編碼輸出數據到客戶端
        response.setCharacterEncoding("UTF-8");
        // 設置客戶端瀏覽器以UTF-8編碼解析數據
        response.setContentType("text/html;charset=UTF-8");
        // 輸出htmlStr裡面的內容到客戶端瀏覽器顯示
        response.getWriter().write(htmlStr);
    }
    
    private void useGetParameterMap(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException{
        // 客戶端是以UTF-8編碼提交表單數據的，所以需要設置服務器端以UTF-8的編碼進行接收，否則對於中文數據就會產生亂碼
        request.setCharacterEncoding("UTF-8");
        
        // request對象封裝的參數是以Map的形式儲存的
        java.util.Map<String, String[]> paramMap = request.getParameterMap();
        for(Map.Entry<String , String[]> entry : paramMap.entrySet()){
            String paramName = entry.getKey();
            String paramValue = "";
            String[] paramValueArr = entry.getValue();
            for(int i=0; paramValueArr!=null && i<paramValueArr.length; i++)
                if(i==paramValueArr.length-1)
                    paramValue += paramValueArr[i];
                else
                    paramValue += paramValueArr[i] + ",";
            System.out.println(MessageFormat.format("{0} = {1}", paramName, paramValue));
        }
    }
}

/*
    getParameter(String)方法(常用)
    getParameterValues(String name)方法(常用)
    getParameterNames()方法(不常用)
    getParameterMap()方法(編寫框架時常用)
*/
package tw.ethan.cnblogs.xdp_gacl;

import java.io.IOException;
import java.io.StringWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 * SimpleTagSupport類實現了SimpleTag接口
 * TagSample3類繼承SimpleTagSupport
 * @author Ethan
 */
public class TagSimple3 extends SimpleTagSupport {

    // 在doTag方法調用jspFrament.invoke方法時，讓執行結果寫一個自定義的緩衝中即可，
    // 然後開發人員可以取出緩衝的數據修改輸出。
    @Override
    public void doTag() throws JspException, IOException {
        // 得到代表jsp標籤體的JspFragment
        JspFragment jspFragment = this.getJspBody();
        StringWriter sw = new StringWriter();
        //將標籤體的內容寫入到sw流中
        jspFragment.invoke(sw);
        // 獲取sw流緩衝區的內容
        String content = sw.getBuffer().toString();
        content = content.toUpperCase();
        PageContext pageContext = (PageContext) this.getJspContext();
        // 將修改後的content輸出至瀏覽器中
        pageContext.getOut().write(content);
    }
    
}

/*
            JspTag接口
               ↑
               ∣
        —  —  —  —  —
       ｜               ｜
    Tag接口        SimpleTag接口
       ↑               ↑
       ｜         SimpleTagSupport類
       ｜
       ｜
 IterationTag接口 ←  — TagSupport類
       ↑                    ↑
       ｜                    ｜
       ｜                    ｜
   BodyTag接口 ←  — BodyTagSupport類

--------------------------------------------------------------------------------
實現SimpleTag接口的標籤通常稱為簡單標籤。簡單標籤共定義了5個方法：

(1. setJspContext方法
　　用於把JSP頁面的pageContext對像傳遞給標籤處理器對象

(2. setParent方法
　　用於把父標籤處理器對像傳遞給當前標籤處理器對象

(3. getParent方法
    用於獲得當前標籤的父標籤處理器對象

(4. setJspBody方法
　　用於把代表標籤體的JspFragment對像傳遞給標籤處理器對象

(5. doTag方法
　　用於完成所有的標籤邏輯，包括輸出、迭代、修改標籤體內容等。在doTag方法中可以拋出
javax.servlet.jsp.SkipPageException異常，用於通知WEB容器不再執行JSP頁面中位於結束
標記後面的內容，這等效於在傳統標籤的doEndTag方法中返回Tag.SKIP_PAGE常量的情況。

--------------------------------------------------------------------------------
SimpleTag接口方法的執行順序：
　　當web容器開始執行標籤時，會調用如下方法完成標籤的初始化：

(1. WEB容器調用標籤處理器對象的setJspContext方法，將代表JSP頁面的pageContext對像傳遞給標籤處理器對象。
(2. WEB容器調用標籤處理器對象的setParent方法，將父標籤處理器對像傳遞給這個標籤處理器對象。注意，只有在標籤存在父標籤的情況下，WEB容器才會調用這個方法。
(3. 如果調用標籤時設置了屬性，容器將調用每個屬性對應的setter方法把屬性值傳遞給標籤處理器對象。如果標籤的屬性值是EL表達式或腳本表達式，則WEB容器首先計算表達式的值，然後把值傳遞給標籤處理器對象。
(4. 如果簡單標籤有標籤體，WEB容器將調用setJspBody方法把代表標籤體的JspFragment對像傳遞進來。
(5. 執行標籤時WEB容器調用標籤處理器的doTag()方法，開發人員在方法體內通過操作JspFragment對象，就可以實現是否執行、迭代、修改標籤體的目的。

--------------------------------------------------------------------------------
開發簡單標籤實現頁面邏輯：
　　SUN公司針對SimpleTag接口提供了一個默認的實現類SimpleTagSupport，
SimpleTagSupport類中實現了SimpleTag接口的所有方法，因此我們可以編寫一個類繼承
SimpleTagSupport類，然後根據業務需要再重寫doTag方法。
*/
package tw.ethan.cnblogs.xdp_gacl;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ethan
 */
@WebServlet(urlPatterns = {"/servlet-session4"})
public class ServletSession4 extends HttpServlet {
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setHeader("content-type", "text/html;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        java.io.PrintWriter out = response.getWriter();
        
        javax.servlet.http.HttpSession session = request.getSession();
        
        java.util.List<tw.ethan.cnblogs.xdp_gacl.Book> list = (java.util.List) session.getAttribute("list");
        if(list==null || list.isEmpty()){
            out.write("對不起，您還沒有購買任何商品！！");
            return;
        }
        
        // 顯示用戶買過的商品
        out.write("您買過如下商品：<br />");
        for(tw.ethan.cnblogs.xdp_gacl.Book book : list){
            out.write(book.getName() + "<br />");
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.doGet(request, response);
    }
    
}
